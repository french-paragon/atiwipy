#include <pybind11/pybind11.h>
#include <pybind11/numpy.h>
#include <pybind11/stl.h>
#include <pybind11/eigen.h>

#include <vector>
#include <string>
#include <iterator>

#include <MultidimArrays/MultidimArrays.h>

#include "3d/parallax.h"
#include "3d/transforms.h"
#include "3d/semiglobalmatching.h"
#include "3d/savepointcloud.h"
#include "3d/geometry.h"
#include "3d/unfold.h"
#include "segmentation/foregroundbackgroundsegmentation.h"
#include "shading/l0segmentation.h"
#include "shading/meanshiftclustering.h"
#include "utils/type_utils.h"
#include "math/splineinterpolationfunc.h"
#include "colors/ociocolortransformer.h"

#include "LibStevi/geometry/alignement.h"

#define xstr(a) str(a)
#define str(a) #a

namespace py = pybind11;

class PySplineInterpolationFunc1D : public SplineInterpolationFunc1D
{
public:
	PySplineInterpolationFunc1D(py::array_t<float, py::array::c_style | py::array::forcecast> array);
};

PySplineInterpolationFunc1D::PySplineInterpolationFunc1D(py::array_t<float, py::array::c_style | py::array::forcecast> array) :
	SplineInterpolationFunc1D (array.data(), static_cast<int>(array.size()-1), true)
{

}

typedef Multidim::Array<float, 2> A_f_2d;
typedef Multidim::Array<float, 3> A_f_3d;
typedef Multidim::Array<float, 4> A_f_4d;
typedef Multidim::Array<disp_t, 2> A_d_2d;
typedef Multidim::Array<disp_t, 3> A_d_3d;

auto imageReprojectorInit (double f1,
						   pybind11::array_t<double> pp1,
						   double f2,
						   pybind11::array_t<double> pp2,
						   pybind11::array_t<double> Rcam1_to_cam2,
						   pybind11::array_t<double> tcam1_to_cam2) {

	Eigen::Matrix<double, 3, 3> Rc1_to_c2;

	if (Rcam1_to_cam2.request().ndim != 2) {
		throw std::invalid_argument("Invalid Rcam1_to_cam2");
	}

	if (Rcam1_to_cam2.request().shape[0] != 3 or Rcam1_to_cam2.request().shape[1] != 3) {
		throw std::invalid_argument("Invalid Rcam1_to_cam2");
	}

	for (int i = 0; i < 3; i++) {
		for (int j = 0; j < 3; j++) {
			Rc1_to_c2(i,j) = Rcam1_to_cam2.at(i,j);
		}
	}

	Eigen::Matrix<double, 3, 1> tc1_to_c2;

	if (tcam1_to_cam2.request().ndim != 1 and tcam1_to_cam2.request().ndim != 2) {
		throw std::invalid_argument("Invalid tcam1_to_cam2");
	}

	if (tcam1_to_cam2.request().shape[0] != 3) {
		throw std::invalid_argument("Invalid tcam1_to_cam2");
	}

	if (tcam1_to_cam2.request().ndim == 2) {
		if (tcam1_to_cam2.request().shape[1] != 1) {
			throw std::invalid_argument("Invalid tcam1_to_cam2");
		}
	}

	for (int i = 0; i < 3; i++) {
		if (tcam1_to_cam2.request().ndim == 1) {
			tc1_to_c2(i) = tcam1_to_cam2.at(i);
		}
		if (tcam1_to_cam2.request().ndim == 2) {
			tc1_to_c2(i) = tcam1_to_cam2.at(i,0);
		}
	}

	Eigen::Matrix<double, 2, 1> pp_c1;

	if (pp1.request().ndim != 1 and pp1.request().ndim != 2) {
		throw std::invalid_argument("Invalid pp1");
	}

	if (pp1.request().shape[0] != 2) {
		throw std::invalid_argument("Invalid pp1");
	}

	if (pp1.request().ndim == 2) {
		if (pp1.request().shape[1] != 1) {
			throw std::invalid_argument("Invalid pp1");
		}
	}

	for (int i = 0; i < 2; i++) {
		if (pp1.request().ndim == 1) {
			pp_c1(i) = pp1.at(i);
		}
		if (pp1.request().ndim == 2) {
			pp_c1(i) = pp1.at(i,0);
		}
	}


	Eigen::Matrix<double, 2, 1> pp_c2;

	if (pp2.request().ndim != 1 and pp2.request().ndim != 2) {
		throw std::invalid_argument("Invalid pp2");
	}

	if (pp2.request().shape[0] != 2) {
		throw std::invalid_argument("Invalid pp2");
	}

	if (pp2.request().ndim == 2) {
		if (pp2.request().shape[1] != 1) {
			throw std::invalid_argument("Invalid pp2");
		}
	}

	for (int i = 0; i < 2; i++) {
		if (pp2.request().ndim == 1) {
			pp_c2(i) = pp2.at(i);
		}
		if (pp2.request().ndim == 2) {
			pp_c2(i) = pp2.at(i,0);
		}
	}

	StereoVision::Geometry::AffineTransform<double> cam1_to_cam2(Rc1_to_c2, tc1_to_c2);

	return StereoVision::Geometry::ImageToImageReprojector<double>(f1, pp_c1, f2, pp_c2, cam1_to_cam2);
};

PYBIND11_MODULE(LIB_NAME, m) {
	m.doc() = "pybind11 " xstr(LIB_NAME) " module.";

	py::class_<A_f_2d>(m, "FloatArray2D", py::buffer_protocol())
			.def_buffer([] (A_f_2d &a) {
		return py::buffer_info(&a.at(0),
							   sizeof (float),
							   py::format_descriptor<float>::format(),
							   2,
							   {a.shape()[0], a.shape()[1]},
							   {sizeof (float)*a.strides()[0],
								sizeof (float)*a.strides()[1]});
	});

	py::class_<A_f_3d>(m, "FloatArray3D", py::buffer_protocol())
			.def_buffer([] (A_f_3d &a) {
		return py::buffer_info(&a.at(0),
							   sizeof (float),
							   py::format_descriptor<float>::format(),
							   3,
							   {a.shape()[0], a.shape()[1], a.shape()[2]},
							   {sizeof (float)*a.strides()[0],
								sizeof (float)*a.strides()[1],
								sizeof (float)*a.strides()[2]});
	});

	py::class_<A_f_4d>(m, "FloatArray4D", py::buffer_protocol())
			.def_buffer([] (A_f_4d &a) {
		return py::buffer_info(&a.at(0),
							   sizeof (float),
							   py::format_descriptor<float>::format(),
							   4,
							   {a.shape()[0], a.shape()[1], a.shape()[2], a.shape()[3]},
							   {sizeof (float)*a.strides()[0],
								sizeof (float)*a.strides()[1],
								sizeof (float)*a.strides()[2],
								sizeof (float)*a.strides()[3]});
	});

	py::class_<A_d_2d>(m, "DisparityArray2D", py::buffer_protocol())
			.def_buffer([] (A_d_2d &a) {
		return py::buffer_info(&a.at(0),
							   sizeof (disp_t),
							   py::format_descriptor<disp_t>::format(),
							   2,
							   {a.shape()[0], a.shape()[1]},
							   {sizeof (disp_t)*a.strides()[0],
								sizeof (disp_t)*a.strides()[1]});
	});

	py::class_<A_d_3d>(m, "DisparityArray3D", py::buffer_protocol())
			.def_buffer([] (A_d_3d &a) {
		return py::buffer_info(&a.at(0),
							   sizeof (disp_t),
							   py::format_descriptor<disp_t>::format(),
							   3,
							   {a.shape()[0], a.shape()[1], a.shape()[2]},
							   {sizeof (disp_t)*a.strides()[0],
								sizeof (disp_t)*a.strides()[1],
								sizeof (disp_t)*a.strides()[2]});
	});

	py::module m3d = m.def_submodule("im3d", "A set of functions for 3d computer vision.");
	py::module mSegmentation = m.def_submodule("segmentation", "A set of functions for segmentation.");
	py::module mShading = m.def_submodule("shading", "A set of functions for shading-based computer vision.");
	py::module mMath = m.def_submodule("math", "A set of math helper functions.");
	py::module mColor = m.def_submodule("color", "A set of color helper functions.");

	m3d.def("computeDisparity", &computeDisparity,
			py::arg("img_l"),
			py::arg("img_r"),
			py::arg("width") = 40,
			py::arg("offset") = 0,
			py::arg("v_radius") = 3,
			py::arg("h_radius") = 3,
			py::arg("method") = "ncc",
			py::arg("direction") = "r2l");

	m3d.def("computeDisparity", &computeDisparityWithCompressor,
			py::arg("img_l"),
			py::arg("img_r"),
			py::arg("compressor"),
			py::arg("width") = 40,
			py::arg("offset") = 0,
			py::arg("method") = "ncc",
			py::arg("direction") = "r2l");

	m3d.def("computeHierachicalDisparity", &computeDisparityHierarchical,
			py::arg("img_l"),
			py::arg("img_r"),
			py::arg("width"),
			py::arg("radiuses"),
			py::arg("truncated_cost_volume_radius") = 2,
			py::arg("method") = "ncc",
			py::arg("direction") = "r2l");

	m3d.def("computeHierachicalDisparity", &computeDisparityHierarchicalConstantRadiuses,
			py::arg("img_l"),
			py::arg("img_r"),
			py::arg("depth") = 1,
			py::arg("width") = 40,
			py::arg("v_radius") = 3,
			py::arg("h_radius") = 3,
			py::arg("truncated_cost_volume_radius") = 2,
			py::arg("method") = "ncc",
			py::arg("direction") = "r2l");

	m3d.def("computeRefinedDisparity", &computeRefinedDisparity,
			py::arg("img_l"),
			py::arg("img_r"),
			py::arg("width") = 40,
			py::arg("v_radius") = 3,
			py::arg("h_radius") = 3,
			py::arg("method") = "ncc",
			py::arg("interpolation") = "image",
			py::arg("preNormalize") = false,
			py::arg("direction") = "r2l");

	m3d.def("computeRefinedDisparity", &computeRefinedDisparityWithCompressor,
			py::arg("img_l"),
			py::arg("img_r"),
			py::arg("compressor"),
			py::arg("width") = 40,
			py::arg("method") = "ncc",
			py::arg("interpolation") = "image",
			py::arg("preNormalize") = false,
			py::arg("direction") = "r2l");

	m3d.def("computeRefinedDisparity2d", &computeRefinedDisparity2d,
			py::arg("img_l"),
			py::arg("img_r"),
			py::arg("searchRegion"),
			py::arg("v_radius") = 3,
			py::arg("h_radius") = 3,
			py::arg("method") = "ncc",
			py::arg("interpolation") = "image",
			py::arg("contiguity") = "queen",
			py::arg("preNormalize") = false);

	m3d.def("computeRefinedDisparity2d", &computeRefinedDisparity2dWithCompressor,
			py::arg("img_l"),
			py::arg("img_r"),
			py::arg("compressor"),
			py::arg("searchRegion"),
			py::arg("method") = "ncc",
			py::arg("interpolation") = "image",
			py::arg("contiguity") = "queen",
			py::arg("preNormalize") = false);

	m3d.def("fullCostVolume", &computeFullCostVolume,
			py::arg("img_l"),
			py::arg("img_r"),
			py::arg("width") = 40,
			py::arg("offset") = 0,
			py::arg("v_radius") = 3,
			py::arg("h_radius") = 3,
			py::arg("method") = "ncc",
			py::arg("direction") = "r2l");

	m3d.def("fullCostVolume", &computeFullCostVolumeWithCompressor,
			py::arg("img_l"),
			py::arg("img_r"),
			py::arg("compressor"),
			py::arg("width") = 40,
			py::arg("offset") = 0,
			py::arg("method") = "ncc",
			py::arg("direction") = "r2l");

	m3d.def("fullCostVolume2d", &computeFullCostVolume2d,
			py::arg("img_l"),
			py::arg("img_r"),
			py::arg("searchRegion"),
			py::arg("v_radius") = 3,
			py::arg("h_radius") = 3,
			py::arg("method") = "ncc");

	m3d.def("fullCostVolume2d", &computeFullCostVolume2dWithCompressor,
			py::arg("img_l"),
			py::arg("img_r"),
			py::arg("compressor"),
			py::arg("searchRegion"),
			py::arg("method") = "ncc");

	m3d.def("getGuidedAggregatedCostVolume", getGuidedAggregatedCostVolume,
			py::arg("costVolume"),
			py::arg("guide"),
			py::arg("v_radius") = 3,
			py::arg("h_radius") = 3,
			py::arg("sigma") = 2.0);

	m3d.def("extractDisparity", &extractRawDisparity,
			py::arg("cost_volume"),
			py::arg("cv_type") = "score");

	m3d.def("dynamicProgrammingExtractDisparity", &extractDisparityDP,
			py::arg("cost_volume"),
			py::arg("cv_type") = "score");

	m3d.def("extractDisparity2d", &extractRawDisparity2d,
			py::arg("cost_volume"),
			py::arg("cv_type") = "score");

	m3d.def("offsetDisparity2d", &offset2dDisparity,
			py::arg("raw_disp"),
			py::arg("searchRegion"));

	m3d.def("unfold", &unfoldImage,
			py::arg("image"),
			py::arg("v_radius") = 3,
			py::arg("h_radius") = 3);

	m3d.def("costFunctionType", [] (std::string const& costFunc) -> std::string {
		std::string lowercaseCostFunc = costFunc;
		std::transform(lowercaseCostFunc.begin(), lowercaseCostFunc.end(), lowercaseCostFunc.begin(),
			[](unsigned char c){ return std::tolower(c); });

		if (lowercaseCostFunc == "ncc" or
				lowercaseCostFunc == "zncc" or
				lowercaseCostFunc == "cc" or
				lowercaseCostFunc == "zcc") {
			return "score";
		} else if (lowercaseCostFunc == "ssd" or
				   lowercaseCostFunc == "zssd" or
				   lowercaseCostFunc == "sad" or
				   lowercaseCostFunc == "zsad" or
				   lowercaseCostFunc == "meds" or
				   lowercaseCostFunc == "zmeds") {
			return "cost";
		} else {
			return "Unsuported matching function";
		}

	});

	m3d.def("truncatedCostVolume", &getTruncatedCostVolume,
			py::arg("cost_volume"),
			py::arg("raw_disp"),
			py::arg("h_radius") = 3,
			py::arg("v_radius") = 3,
			py::arg("cv_radius") = 1,
			py::arg("direction") = "r2l",
			py::arg("truncation_type") = "same");

	m3d.def("truncatedCostVolume2d", &getTruncatedCostVolume2d,
			py::arg("cost_volume"),
			py::arg("raw_disp"),
			py::arg("cv_radius_h") = 1,
			py::arg("cv_radius_v") = 1);

	m3d.def("truncatedCostVolumeInBoundDomain", &getInBound,
			py::arg("raw_disp"),
			py::arg("correlationWidth"),
			py::arg("h_radius") = 3,
			py::arg("v_radius") = 3,
			py::arg("cv_radius") = 1,
			py::arg("direction") = "r2l",
			py::arg("truncation_type") = "same");

	m3d.def("getCostRefinedDisparity", &getCostRefinedDisparity,
			py::arg("truncated_cost_volume"),
			py::arg("raw_disp"),
			py::arg("interpolation") = "parabola");

	m3d.def("getImageRefinedDisparity", &getImageRefinedDisparity,
			py::arg("img_l"),
			py::arg("img_r"),
			py::arg("raw_disp"),
			py::arg("h_radius") = 3,
			py::arg("v_radius") = 3,
			py::arg("method") = "ncc",
			py::arg("direction") = "r2l");

	m3d.def("getCostRefinedDisparity2d", &getCostRefinedDisparity2d,
			py::arg("truncated_cost_volume"),
			py::arg("raw_disp"),
			py::arg("interpolation") = "parabola",
			py::arg("assumeIsotropy") = true,
			py::arg("use2dFunctionMatching") = false);

	m3d.def("semisGlobalMatchingCostVolume", &semisGlobalMatchingCostVolume,
			py::arg("cv_base"),
			py::arg("cv_type"),
			py::arg("n_directions") = 8,
			py::arg("margins") = pybind11::array_t<int>(),
			py::arg("P1") = 0.01,
			py::arg("P2") = 0.2);

	m3d.def("saveXYZRGBpointcloud", &saveXYZRGBpointcloud,
			py::arg("filename"),
			py::arg("geometry"),
			py::arg("color"),
			py::arg("correctGamma") = true);

	m3d.def("censusTransform", &censusTransform,
			py::arg("img"),
			py::arg("h_radius") = 3,
			py::arg("v_radius") = 3);

	m3d.def("getProportionCovered", &getProportionCovered,
			py::arg("disp_l"),
			py::arg("disp_r")
			);

	m3d.def("getCoverMask", &getCoverMask,
			py::arg("disp_l"),
			py::arg("disp_r")
			);

	m3d.def("estimateShapePreservingTransformBetweenPoints", &estimateShapePreservingTransformBetweenPoints,
			py::arg("ptsSource"),
			py::arg("ptsTarget"),
			py::arg("n_steps") = 50,
			py::arg("incrLimit") = 1e-8,
			py::arg("damping") = 5e-1,
			py::arg("dampingScale") = std::nanf(""));

	py::class_< StereoVision::Geometry::ImageToImageReprojector<double>>(m3d, "ImageToImageReprojector")
			.def(py::init(&imageReprojectorInit),
				 py::arg("f1"),
				 py::arg("pp1"),
				 py::arg("f2"),
				 py::arg("pp2"),
				 py::arg("Rcam1_to_cam2"),
				 py::arg("tcam1_to_cam2"))
			.def("reprojectPoint", [] (StereoVision::Geometry::ImageToImageReprojector<double> &self, Eigen::Vector2d imPos, double depth) {return self.reprojected(imPos, depth);});

	mShading.def("l0approximation", &l0approximation,
				 py::arg("image"),
				 py::arg("lambda"),
				 py::arg("channelDim") = -1,
				 py::arg("maxiter") = 100);

	mSegmentation.def("partialGlobalSegmentation", &partialGlobalSegmentation,
					  py::arg("cost"),
					  py::arg("current_mask"),
					  py::arg("guide"),
					  py::arg("optimizablePixelsRadius"),
					  py::arg("switch_max_cost"),
					  py::arg("switch_min_cost"));

	mShading.def("meanShiftImageClustering", &meanShiftImageClustering,
				 py::arg("image"),
				 py::arg("radius"),
				 py::arg("incrLimit") = -1,
				 py::arg("maxiter") = 100);

	py::class_<PySplineInterpolationFunc1D>(mMath, "SplineInterpolationFunc1D")
			.def(py::init<py::array_t<float, py::array::c_style | py::array::forcecast>>())
			.def("aCoeff", [](PySplineInterpolationFunc1D &self, py::array_t<int> arg) {
				auto v = [&self](int arg) { return self.aCoeffSafe(arg); };
				return py::vectorize(v)(arg);
			})
			.def("bCoeff", [](PySplineInterpolationFunc1D &self, py::array_t<int> arg) {
				auto v = [&self](int arg) { return self.bCoeffSafe(arg); };
				return py::vectorize(v)(arg);
			})
			.def("cCoeff", [](PySplineInterpolationFunc1D &self, py::array_t<int> arg) {
				auto v = [&self](int arg) { return self.cCoeffSafe(arg); };
				return py::vectorize(v)(arg);
			})
			.def("eval", [](PySplineInterpolationFunc1D &self, py::array_t<float> arg) {
				auto v = [&self](float arg) { return self(arg); };
				return py::vectorize(v)(arg);
			});

	py::class_<OcioColorTransformer>(mColor, "OcioColorTransformer")
			.def(py::init<const std::string &, const std::string &, const std::string &, const std::string &>())
			.def("applyTransform", &OcioColorTransformer::applyTransform);
}
