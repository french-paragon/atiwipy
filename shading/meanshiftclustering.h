#ifndef MEANSHIFTCLUSTERING_H
#define MEANSHIFTCLUSTERING_H

#include "utils/arrays_utils.h"
#include "utils/type_utils.h"

#include <LibStevi/imageProcessing/meanShiftClustering.h>


pybind11::array meanShiftImageClustering(pybind11::array const& inputImage,
										 float radius,
										 float incrTol = -1,
										 int maxiter= 100);

#endif // MEANSHIFTCLUSTERING_H
