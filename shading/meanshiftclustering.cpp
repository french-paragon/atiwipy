#include "meanshiftclustering.h"

template<class T_IM, int nDim, typename ComputeType = float>
pybind11::array_t<ComputeType> meanShiftImageClusteringImpl(Multidim::Array<T_IM, nDim> const& im,
												   ComputeType radius,
												   float incrTol = -1,
												   int maxiter= 100) {

	typedef std::function<ComputeType(std::vector<ComputeType> const& v1,std::vector<ComputeType> const& v2)> KernelT;

	KernelT kernel = StereoVision::ImageProcessing::RadiusKernel<ComputeType>(radius);
	std::optional<float> incrLim = (incrTol > 0) ? std::optional<float>(incrTol) : std::nullopt;

	return Multidim2NumpyArray(StereoVision::ImageProcessing::meanShiftClustering(im, kernel, -1, incrLim, maxiter));

}

template<class T_IM, typename ComputeType = float>
pybind11::array_t<ComputeType> meanShiftImageClusteringImpl(pybind11::array const& im,
												   ComputeType radius,
												   float incrTol = -1,
												   int maxiter= 100) {

	if (im.ndim() == 2) {
		return meanShiftImageClusteringImpl<T_IM, 2, ComputeType>(Numpy2MultidimArray<T_IM, 2>(im), radius, incrTol, maxiter);
	} else if (im.ndim() == 3) {
		return meanShiftImageClusteringImpl<T_IM, 3, ComputeType>(Numpy2MultidimArray<T_IM, 3>(im), radius, incrTol, maxiter);
	} else {
		throw std::invalid_argument("Input image should be either two or three dimensional");
	}

	return pybind11::array_t<ComputeType>();
}


pybind11::array meanShiftImageClustering(pybind11::array const& inputImage,
										 float radius,
										 float incrTol,
										 int maxiter) {

	ArraysFormat im_format = formatFromName(inputImage.request().format);

	switch (im_format) {
	case T_FLOAT:
		return meanShiftImageClusteringImpl<float, float>(inputImage, radius, incrTol, maxiter);
	case T_UINT8:
		return meanShiftImageClusteringImpl<uint8_t, float>(inputImage, radius, incrTol, maxiter);
	default:
		throw std::invalid_argument("l0approximation can use images of coded with either float or uint8 !");
	}

	return pybind11::array();
}
