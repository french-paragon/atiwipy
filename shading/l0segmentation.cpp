#include "./l0segmentation.h"

template<class T_IM, int nDim, typename ComputeType = float>
pybind11::array_t<ComputeType> l0approximationImpl(Multidim::Array<T_IM, nDim> const& im,
												   ComputeType lambda,
												   int channelDim = -1,
												   int maxiter= 100) {

	return Multidim2NumpyArray(StereoVision::Optimization::regionFusionL0Approximation(im, lambda, channelDim, maxiter));

}

template<class T_IM, typename ComputeType = float>
pybind11::array_t<ComputeType> l0approximationImpl(pybind11::array const& im,
												   ComputeType lambda,
												   int channelDim = -1,
												   int maxiter= 100) {

	if (im.ndim() == 2) {
		return l0approximationImpl<T_IM, 2, ComputeType>(Numpy2MultidimArray<T_IM, 2>(im), lambda, channelDim, maxiter);
	} else if (im.ndim() == 3) {
		return l0approximationImpl<T_IM, 3, ComputeType>(Numpy2MultidimArray<T_IM, 3>(im), lambda, channelDim, maxiter);
	} else {
		throw std::invalid_argument("Input image should be either two or three dimensional");
	}

	return pybind11::array_t<ComputeType>();
}

pybind11::array l0approximation(pybind11::array const& inputImage,
								float lambda,
								int channelDim,
								int maxiter) {

	ArraysFormat im_format = formatFromName(inputImage.request().format);

	switch (im_format) {
	case T_FLOAT:
		return l0approximationImpl<float, float>(inputImage, lambda, channelDim, maxiter);
	case T_UINT8:
		return l0approximationImpl<uint8_t, float>(inputImage, lambda, channelDim, maxiter);
	default:
		throw std::invalid_argument("l0approximation can use images of coded with either float or uint8 !");
	}

	return pybind11::array();

}
