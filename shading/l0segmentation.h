#ifndef L0SEGMENTATION_H
#define L0SEGMENTATION_H

#include "utils/arrays_utils.h"
#include "utils/type_utils.h"

#include <LibStevi/optimization/l0optimization.h>


pybind11::array l0approximation(pybind11::array const& inputImage,
								float lambda,
								int channelDim = -1,
								int maxiter= 100);

#endif // L0SEGMENTATION_H
