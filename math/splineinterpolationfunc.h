#ifndef SPLINEINTERPOLATIONFUNC_H
#define SPLINEINTERPOLATIONFUNC_H

#include <vector>
#include <memory>

/*!
 * \brief The SplineInterpolationFunc1D class is a very simple spline class to interpolate along images lines.
 *
 * It has degree 2 (so that derivative is linear, usefull for optimization problems) and assume second derivative equal 0 on the first interval.
 */
class SplineInterpolationFunc1D
{
public:

	SplineInterpolationFunc1D(float const* vals, int n_intervals, bool keepDat = false);
	SplineInterpolationFunc1D(std::vector<float> const& vals);
	~SplineInterpolationFunc1D();

	float operator() (float x) const;

	//! \brief constant coeff
	float aCoeff(int i) const;
	//! \brief linear coeff
	float bCoeff(int i) const;
	//! \brief quadratic coeff
	float cCoeff(int i) const;

	//! \brief constant coeff with adress check
	float aCoeffSafe(int i) const;
	//! \brief linear coeff with adress check
	float bCoeffSafe(int i) const;
	//! \brief quadratic coeff with adress check
	float cCoeffSafe(int i) const;

protected:

	void initVal();

	const float * _vals;
	int _n;

	float* _b;
	float* _c;

	std::vector<float> _storedVals;

};

#endif // SPLINEINTERPOLATIONFUNC_H
