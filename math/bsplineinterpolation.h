#ifndef BSPLINEINTERPOLATION_H
#define BSPLINEINTERPOLATION_H

#include <Eigen/SparseCore>
#include <Eigen/IterativeLinearSolvers>
#include <MultidimArrays/MultidimArrays.h>

template<int DEG, int DIM, typename T, typename... Ts>
T evaluateBSplineVal(Ts... coords) {

	std::array<T, DIM> c = {coords...};
	return evaluateBSplineVal<DEG, DIM, T>(c);
}

template<int DEG, int DIM, typename T>
T evaluateBSplineVal(std::array<T, DIM> const& coords) {

	if (DEG <= 0) {
		for (int i = 0; i < DIM; i++) {
			if (coords[i] < 0 or coords[i] > 1) {
				return 0;
			}
		}
		return 1;
	}

	if (DIM == 1) {
		return (coords[0])/DEG * evaluateBSplineVal<DEG-1, 1, T>(coords[0]) +
				(DEG + 1 - coords[0])/DEG * evaluateBSplineVal<DEG-1, 1, T>(coords[0] - 1);
	}

	T prod = 1;

	for (int i = 0; i < DIM; i++) {
		prod *= (coords[i])/DEG * evaluateBSplineVal<DEG-1, 1, T>(coords[i]) +
				(DEG + 1 - coords[i])/DEG * evaluateBSplineVal<DEG-1, 1, T>(coords[i] - 1);
	}

	return prod;

}

/*!
 * The GridBSplineFunc class represent a zero centered BSpline function
 * assuming the nodes form a regular grid in the given dimension.
 *
 * By default the computation is made with float but this can be overriden.
 */
template<int DEG, int DIM = 1, typename T = float>
class GridBSplineFunc {

	static_assert(std::is_same<T, float>::value or std::is_same<T, double>::value,
			"Only float or double should be selected as underlying type");

	static_assert(DEG >= 0,
			"Degree of a spline should be greather than or equal to zero.");

	static_assert(DIM > 0,
			"Dimension of a spline should be greather than zero.");
public:

	using ShapeBlock = typename Multidim::Array<T, DIM>::ShapeBlock;
	using IndexBlock = typename Multidim::Array<T, DIM>::IndexBlock;
	using CoordBlock = std::array<T, DIM>;

	//! \brief the delta betwee the centered and uncentered splines
	static const T UncenteredDelta = (static_cast<T>(DEG) + static_cast<T>(1))/static_cast<T>(2);
	static const int NodeMargin = DEG/2;
	static const int NodeGridWidth = 2*NodeMargin + 1;

	template<typename... Ts>
	T operator()(Ts... Args) {
		static_assert(sizeof...(Args) == DIM,
				"Wrong number of coordinates provided");

		return *this({Args...});
	}

	T operator()(CoordBlock const& coords) {
		CoordBlock c = coords;
		c += UncenteredDelta;
		return evaluateBSplineVal<DEG, DIM, T>(c);
	}

	static constexpr Multidim::Array<T, DIM> getValuesAtNodes() {
		Multidim::Array<T, DIM> r(getNodeValCacheShape());

		IndexBlock b;
		b.fill(0);

		ShapeBlock s = r.shape();


		for (int i = 0; i < r.flatLenght(); i++) {

			CoordBlock c;
			c += b;

			r.at(b) = evaluateBSplineVal<DEG, DIM, T>(c);

			b.moveToNextIndex(s);
		}

		return r;
	}

	static Multidim::Array<T, DIM> changeToSplineBase(Multidim::Array<T, DIM> const& array, int maxIter = -1, T tol = -1) {

		using Vector = Eigen::Matrix<T,Eigen::Dynamic,1>;
		using Matrix = Eigen::SparseMatrix<T>;

		if (DEG == 0 or DEG == 1) {
			return array;
		}

		int n = array.flatLenght();

		Matrix A(n, n);
		Vector y;
		y.resize(n);

		IndexBlock b;
		b.fill(0);

		IndexBlock s = array.shape();

		Multidim::Array<T, DIM> nodes = getValuesAtNodes();
		IndexBlock n_s = array.shape();

		Multidim::Array<T, DIM> outArray(array.shape());

		for (int i = 0; i < n; i++) {

			IndexBlock x;
			x.fill(0);

			int id_in = outArray.flatIndex<Multidim::AccessCheck::Nocheck>(b);
			y[id_in] = array.value<Multidim::AccessCheck::Nocheck>(b);

			for (int i = 0; i < nodes.flatLenght(); i++) {

				IndexBlock c;

				c = b + x - NodeMargin;
				if (!c.isInLimit(s)) {
					continue;
				}

				int id_out = outArray.flatIndex<Multidim::AccessCheck::Nocheck>(c);

				A(id_in, id_out) = nodes.value(x);


				x.moveToNextIndex(n_s);
			}

			b.moveToNextIndex(s);
		}

		Eigen::Map<Vector> m(&outArray.at(0), n, 1);

		Eigen::ConjugateGradient<Matrix, Eigen::Lower|Eigen::Upper> cg;
		cg.compute(A);

		if (maxIter > 0) {
			cg.setMaxIterations(maxIter);
		}

		if (tol > 0) {
			cg.setTolerance(tol);
		}

		m = cg.solveWithGuess(y, y);

		return outArray;
	}

protected:
	static constexpr typename Multidim::Array<T, DIM>::ShapeBlock getNodeValCacheShape() {
		typename Multidim::Array<T, DIM>::ShapeBlock b;
		for (int i = 0; i < DIM; i++) {
			b[i] = NodeGridWidth;
		}

		return b;
	}
	Multidim::Array<T, DIM> _nodeValCache;

};

template<int DEG, int DIM, typename T, typename... Ts>
T evaluateCenteredBSplineVal(Ts... coords) {

	std::array<T, DIM> c = {coords...};
	return evaluateCenteredBSplineVal<DEG, DIM, T>(c);
}

template<int DEG, int DIM, typename T>
T evaluateCenteredBSplineVal(std::array<T, DIM> const& coords) {

	static const GridBSplineFunc<DEG, DIM, T> spline;

	return spline(coords);

}

#endif // BSPLINEINTERPOLATION_H
