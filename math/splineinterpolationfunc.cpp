#include "splineinterpolationfunc.h"

#include <cmath>
#include <stdexcept>

SplineInterpolationFunc1D::SplineInterpolationFunc1D(const float *vals, int n, bool keepDat) :
	_vals(vals),
	_n(n),
	_b(nullptr),
	_c(nullptr)
{
	_b = new float [_n];
	_c = new float [_n];

	if (keepDat) {

		_storedVals.reserve(static_cast<size_t>(n+1));

		for (int i = 0; i <= n; i++) {
			_storedVals.push_back(vals[i]);
		}

		_vals = _storedVals.data();

	}

	initVal();

}

SplineInterpolationFunc1D::SplineInterpolationFunc1D(std::vector<float> const& vals) :
	SplineInterpolationFunc1D(vals.data(), static_cast<int>(vals.size()-1))
{
	_storedVals = vals;
	_vals = _storedVals.data();
}

SplineInterpolationFunc1D::~SplineInterpolationFunc1D() {

	delete [] _b;
	delete [] _c;

}

float SplineInterpolationFunc1D::operator() (float x) const{

	if (x < 0 || x > _n) {
		return 0;
	}

	if (std::fabs(x - _n) < float(1e-6)) {
		return _vals[_n];
	}

	int i = static_cast<int>(floor(static_cast<double>(x)));
	float dx = x - i;

	return _vals[i] + _b[i]*dx + _c[i]*dx*dx;

}

float SplineInterpolationFunc1D::aCoeff(int i) const {
	return _vals[i];
}

float SplineInterpolationFunc1D::bCoeff(int i) const {
	return _b[i];
}

float SplineInterpolationFunc1D::cCoeff(int i) const {
	return _c[i];
}


float SplineInterpolationFunc1D::aCoeffSafe(int i) const {

	if (i < 0 || i >= _n) {
		throw std::out_of_range("Tried to read out of memory coefficient");
	}

	return _vals[i];
}

float SplineInterpolationFunc1D::bCoeffSafe(int i) const {

	if (i < 0 || i >= _n) {
		throw std::out_of_range("Tried to read out of memory coefficient");
	}

	return _b[i];
}

float SplineInterpolationFunc1D::cCoeffSafe(int i) const {

	if (i < 0 || i >= _n) {
		throw std::out_of_range("Tried to read out of memory coefficient");
	}

	return _c[i];
}


void SplineInterpolationFunc1D::initVal() {

	_b[0] = _vals[1] - _vals[0];
	_c[0] = 0;

	for (int i = 1; i < _n; i++) {
		_b[i] = _b[i-1] + 2*_c[i-1];
		_c[i] = _vals[i+1] - _vals[i] - _b[i];
	}

}
