#ifndef TYPE_UTILS_H
#define TYPE_UTILS_H

#include <string>

enum ArraysFormat{
	T_UINT8 = 1,
	T_FLOAT = 2,
	T_INVALID = 0
};

enum DispFormat{
	T_DISP_INT8 = 1,
	T_DISP_INT32 = 2,
	T_DISP_FLOAT = 3,
	T_DISP_INVALID = 0
};

ArraysFormat formatFromName(std::string const& f_name);
DispFormat dispFormatFromName(std::string const& f_name);

#endif // TYPE_UTILS_H
