#include "type_utils.h"

#include <pybind11/pybind11.h>

namespace py = pybind11;

ArraysFormat formatFromName(const std::string &f_name) {

    if (f_name == py::format_descriptor<float>::format()) {
        return T_FLOAT;
    } else if (f_name == py::format_descriptor<uint8_t>::format()) {
        return T_UINT8;
    }

    return T_INVALID;

}

DispFormat dispFormatFromName(std::string const& f_name) {

	if (f_name == py::format_descriptor<float>::format()) {
		return T_DISP_FLOAT;
	} else if (f_name == py::format_descriptor<int8_t>::format()) {
		return T_DISP_INT8;
	} else if (f_name == py::format_descriptor<int32_t>::format()) {
		return T_DISP_INT32;
	}

	return T_DISP_INVALID;
}
