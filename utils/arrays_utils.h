#ifndef ARRAYS_UTILS_H
#define ARRAYS_UTILS_H

#include <pybind11/pybind11.h>
#include <pybind11/numpy.h>
#include <MultidimArrays/MultidimArrays.h>

template<typename T, Multidim::array_size_t nDim>
/*!
 * \brief Multidim2UntypedNumpyArray convert a multidim array to a numpy array. The numpy array takes ownership of the data.
 * \param array the array to convert
 * \return the corresponding numpy array
 */
pybind11::array Multidim2UntypedNumpyArray(Multidim::Array<T, nDim> & array) {

	std::vector<pybind11::size_t> shape(nDim);
	std::vector<pybind11::size_t> strides(nDim);

	for (int i = 0; i < nDim; i++) {
		shape[i] = array.shape()[i];
		strides[i] = array.strides()[i]*sizeof (T);
	}

	T* data = array.takePointer();

	pybind11::capsule cleaner(data, [](void *buffer) {
				T *data = reinterpret_cast<T *>(buffer);
				delete[] data;
	});


	return pybind11::array(shape, strides, data, cleaner);

}

template<typename T, Multidim::array_size_t nDim>
/*!
 * \brief Multidim2UntypedNumpyArray convert a multidim array to a numpy array. The numpy array takes ownership of the data.
 * \param array the array to convert
 * \return the corresponding numpy array
 */
pybind11::array Multidim2UntypedNumpyArray(Multidim::Array<T, nDim> && array) {

	std::vector<pybind11::size_t> shape(nDim);
	std::vector<pybind11::size_t> strides(nDim);

	for (int i = 0; i < nDim; i++) {
		shape[i] = array.shape()[i];
		strides[i] = array.strides()[i]*sizeof (T);
	}

	T* data = array.takePointer();

	pybind11::capsule cleaner(data, [](void *buffer) {
				T *data = reinterpret_cast<T *>(buffer);
				delete[] data;
	});


	return pybind11::array(shape, strides, data, cleaner);

}

template<typename T, Multidim::array_size_t nDim>
/*!
 * \brief Multidim2NumpyArray convert a multidim array to a numpy array. The numpy array takes ownership of the data.
 * \param array the array to convert
 * \return the corresponding numpy array
 */
pybind11::array_t<T> Multidim2NumpyArray(Multidim::Array<T, nDim> & array) {

	std::vector<pybind11::size_t> shape(nDim);
	std::vector<pybind11::size_t> strides(nDim);

	for (int i = 0; i < nDim; i++) {
		shape[i] = array.shape()[i];
		strides[i] = array.strides()[i]*sizeof (T);
	}

	T* data = array.takePointer();

	pybind11::capsule cleaner(data, [](void *buffer) {
				T *data = reinterpret_cast<T *>(buffer);
				delete[] data;
	});


	return pybind11::array_t<T>(shape, strides, data, cleaner);

}

template<typename T, Multidim::array_size_t nDim>
/*!
 * \brief Multidim2NumpyArray convert a multidim array to a numpy array. The numpy array takes ownership of the data.
 * \param array the array to convert
 * \return the corresponding numpy array
 */
pybind11::array_t<T> Multidim2NumpyArray(Multidim::Array<T, nDim> && array) {

	std::vector<pybind11::size_t> shape(nDim);
	std::vector<pybind11::size_t> strides(nDim);

	for (int i = 0; i < nDim; i++) {
		shape[i] = array.shape()[i];
		strides[i] = array.strides()[i]*sizeof (T);
	}

	T* data = array.takePointer();

	pybind11::capsule cleaner(data, [](void *buffer) {
				T *data = reinterpret_cast<T *>(buffer);
				delete[] data;
	});


	return pybind11::array_t<T>(shape, strides, data, cleaner);

}

template<typename T, Multidim::array_size_t nDim>
/*!
 * \brief Numpy2MultidimArray convert a pybind11 numpy array to a multidim array. The numpy array keeps ownership of the data.
 * \param array the array to convert
 * \return a view of the data as a multidim array
 */
Multidim::Array<T, nDim> Numpy2MultidimArray(pybind11::array & array) {

	if (array.ndim() != nDim) {
		return Multidim::Array<T, nDim>();
	}

	typename Multidim::Array<T, nDim>::ShapeBlock shape;
	typename Multidim::Array<T, nDim>::ShapeBlock strides;

	auto in_shape = array.shape();
	auto in_strides = array.strides();

	for (int i = 0; i < nDim; i++) {
		shape[i] = in_shape[i];
		strides[i] = in_strides[i]/sizeof (T);
	}

	return Multidim::Array<T, nDim>(static_cast<T*>(array.mutable_data()), shape, strides);

}

template<typename T, Multidim::array_size_t nDim>
/*!
 * \brief Numpy2MultidimArray convert a pybind11 numpy array to a multidim array. The numpy array keeps ownership of the data.
 * \param array the array to convert
 * \return a view of the data as a multidim array (with constant underlying type)
 */
const Multidim::Array<T, nDim> Numpy2MultidimArray(pybind11::array const& array) {

	if (array.ndim() != nDim) {
		return Multidim::Array<T, nDim>();
	}

	typename Multidim::Array<T, nDim>::ShapeBlock shape;
	typename Multidim::Array<T, nDim>::ShapeBlock strides;

	auto in_shape = array.shape();
	auto in_strides = array.strides();

	for (int i = 0; i < nDim; i++) {
		shape[i] = in_shape[i];
		strides[i] = in_strides[i]/sizeof (T);
	}

	return Multidim::Array<T, nDim>(static_cast<T*>(const_cast<void*>(array.data())), shape, strides);

}

#endif // ARRAYS_UTILS_H
