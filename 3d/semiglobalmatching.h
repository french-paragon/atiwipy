#ifndef SEMIGLOBALMATCHING_H
#define SEMIGLOBALMATCHING_H

#include <pybind11/pybind11.h>
#include <pybind11/numpy.h>

#include <MultidimArrays/MultidimArrays.h>

Multidim::Array<float, 3> semisGlobalMatchingCostVolume(pybind11::array cv_base,
														std::string const& cv_type,
														int nDirections = 8,
														pybind11::array_t<int> margins = pybind11::array_t<int>(),
														float P1 = 0.01,
														float P2 = 0.2);

#endif // SEMIGLOBALMATCHING_H
