#include "geometry.h"

#include "LibStevi/geometry/pointcloudalignment.h"

pybind11::array_t<float> estimateShapePreservingTransformBetweenPoints(pybind11::array_t<float> ptsSource,
																	   pybind11::array_t<float> ptsTarget,
																	   int n_steps,
																	   float incrLimit,
																	   float damping,
																	   float dampingScale) {


	auto shapeSource = ptsSource.request().shape;
	auto shapeTarget = ptsTarget.request().shape;

	if (shapeSource != shapeTarget) {
		throw std::invalid_argument("function expect arrays of the same shape !");
	}

	if (shapeSource[0] != 3) {
		throw std::invalid_argument("function expect arrays where first dimension has size 3 !");
	}

	if (shapeSource.size() != 2) {
		throw std::invalid_argument("function expect arrays with 2 dimensions !");
	}

	int nPts = shapeSource[1];

	Eigen::VectorXf obs;
	obs.resize(3*nPts);

	Eigen::Matrix3Xf pts;
	pts.resize(3, nPts);

	std::vector<int> idxs(3*nPts);
	std::vector<StereoVision::Geometry::Axis> coords(3*nPts);

	for (int i = 0; i < nPts; i++) {

		obs[3*i] = ptsTarget.unchecked()(0,i);
		obs[3*i+1] = ptsTarget.unchecked()(1,i);
		obs[3*i+2] = ptsTarget.unchecked()(2,i);

		idxs[3*i] = i;
		idxs[3*i+1] = i;
		idxs[3*i+2] = i;

		coords[3*i] = StereoVision::Geometry::Axis::X;
		coords[3*i+1] = StereoVision::Geometry::Axis::Y;
		coords[3*i+2] = StereoVision::Geometry::Axis::Z;

		pts(0,i) = ptsSource.unchecked()(0,i);
		pts(1,i) = ptsSource.unchecked()(1,i);
		pts(2,i) = ptsSource.unchecked()(2,i);

	}

	StereoVision::Geometry::IterativeTermination status;
	StereoVision::Geometry::ShapePreservingTransform t = StereoVision::Geometry::estimateShapePreservingMap(obs,
																											pts,
																											idxs,
																											coords,
																											&status,
																											n_steps,
																											incrLimit,
																											damping,
																											(!std::isnan(dampingScale)) ? dampingScale : damping);

	if (status == StereoVision::Geometry::IterativeTermination::Error) {
		return pybind11::array_t<float>();
	}

	StereoVision::Geometry::AffineTransform at = t.toAffineTransform();
	pybind11::array_t<float> r({3,4});

	for (int i = 0; i < 3; i++) {
		for (int j = 0; j < 3; j++) {
			r.mutable_unchecked()(i,j) = at.R(i,j);
		}
		r.mutable_unchecked()(i,3) = at.t[i];
	}

	return r;

}
