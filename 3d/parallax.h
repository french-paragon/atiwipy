#ifndef PARALLAX_H
#define PARALLAX_H

#include <pybind11/pytypes.h>
#include <pybind11/numpy.h>

#include <MultidimArrays/MultidimArrays.h>

#include <vector>
#include <map>

enum costVolStrategy{
	SlicedCostVolume = 0,
	FullCostVolume = 1
};

enum dispExtractionStartegy{
	cvCost = 0,
	cvScore = 1
};

typedef int32_t disp_t;

template<class T_CV>
pybind11::array_t<T_CV> truncatedCostVolume(pybind11::array_t<T_CV> costVolume,
									  pybind11::array_t<disp_t> disp,
									  uint8_t cost_vol_radius) {

	std::vector<long> cv_shape = costVolume.request().shape;

	std::vector<long> tcv_shape = {cv_shape[0], cv_shape[1], cost_vol_radius*2+1};
	std::vector<long> tcv_strides = {cv_shape[1]*(cost_vol_radius*2+1)*static_cast<long>(sizeof(T_CV)),
									(cost_vol_radius*2+1)*static_cast<long>(sizeof(T_CV)),
									1*static_cast<long>(sizeof(T_CV))};

	pybind11::array_t<T_CV> tcv = pybind11::array_t<T_CV>(tcv_shape, tcv_strides);

	auto cv_data = costVolume.unchecked();
	auto disp_data = disp.unchecked();
	auto tcv_data = tcv.mutable_unchecked();

	#pragma omp parallel for
	for (int i = 0; i < cv_shape[0]; i++) {
		for (int j = 0; j < cv_shape[1]; j++) {
			for (int32_t d = 0; d <= 2*cost_vol_radius; d++) {
				int32_t p = disp_data(i,j)+d-cost_vol_radius;

				if (p < 0 or p >= cv_shape[2]) {
					tcv_data(i,j,d) = 0;
				} else {
					tcv_data(i,j,d) = cv_data(i,j,p);
				}
			}
		}
	}

	return tcv;

}


template<class T_IB>
pybind11::array_t<T_IB> extractInBoundDomain(pybind11::array_t<disp_t> disp,
									   uint32_t width,
									   uint8_t cost_vol_radius) {


	std::vector<long> im_shape = disp.request().shape;

	std::vector<long> ib_shape = {im_shape[0], im_shape[1], cost_vol_radius*2+1};
	std::vector<long> ib_strides = {im_shape[1]*(cost_vol_radius*2+1)*static_cast<long>(sizeof(T_IB)),
									(cost_vol_radius*2+1)*static_cast<long>(sizeof(T_IB)),
									1*static_cast<long>(sizeof(T_IB))};

	pybind11::array_t<T_IB> ib = pybind11::array_t<T_IB>(ib_shape, ib_strides);

	auto disp_data = disp.unchecked();
	auto ib_data = disp.mutable_unchecked();

	#pragma omp parallel for
	for (int i = 0; i < im_shape[0]; i++) {
		for (int j = 0; j < im_shape[1]; j++) {
			for (uint32_t d = -cost_vol_radius; d <= cost_vol_radius; d++) {
				uint32_t p = disp_data(i,j)+d;

				if (p < 0 or p >= width) {
					ib_data(i,j,d+cost_vol_radius) = 0;
				} else {
					ib_data(i,j,d+cost_vol_radius) = 1;
				}
			}
		}
	}

	return ib;

}

pybind11::array_t<disp_t> computeDisparity(pybind11::array img_l,
											pybind11::array img_r,
											uint32_t width,
										   int32_t offset,
											uint8_t v_radius,
											uint8_t h_radius,
										   std::string const& method = "zncc",
										   std::string const& direction = "r2l");

pybind11::tuple computeDisparityHierarchicalConstantRadiuses(pybind11::array img_l,
															 pybind11::array img_r,
															 uint8_t depth,
															 uint32_t width,
															 uint8_t v_radius,
															 uint8_t h_radius,
															 disp_t truncated_cost_volume_radius,
															 std::string const& method = "zncc",
															 const std::string &direction = "r2l");

pybind11::tuple computeDisparityHierarchical(pybind11::array img_l,
											 pybind11::array img_r,
											 uint32_t width,
											 pybind11::array_t<uint8_t> radiuses,
											 disp_t truncated_cost_volume_radius,
											 std::string const& method = "zncc",
											 const std::string &direction = "r2l");

pybind11::array_t<disp_t> computeDisparityWithCompressor(pybind11::array img_l,
														  pybind11::array img_r,
														  pybind11::array_t<int> compressor,
														  uint32_t width,
														  int32_t offset,
														  std::string const& method = "zncc",
														 const std::string &direction = "r2l");

pybind11::array_t<float> computeFullCostVolume(pybind11::array img_l,
											   pybind11::array img_r,
											   uint32_t width,
											   int32_t offset,
											   uint8_t v_radius,
											   uint8_t h_radius,
											   std::string const& method = "zncc",
											   const std::string &direction = "r2l");

pybind11::array_t<float> computeFullCostVolumeWithCompressor(pybind11::array img_l,
															 pybind11::array img_r,
															 pybind11::array_t<int> compressor,
															 uint32_t width,
															 int32_t offset,
															 std::string const& method = "zncc",
															 const std::string &direction = "r2l");

pybind11::array_t<float> computeFullCostVolume2d(pybind11::array img_l,
												  pybind11::array img_r,
												  std::array<int, 4> searchSpace,
												  uint8_t v_radius,
												  uint8_t h_radius,
												  std::string const& method = "zncc");

pybind11::array_t<float> computeFullCostVolume2dWithCompressor(pybind11::array img_l,
																pybind11::array img_r,
																pybind11::array_t<int> compressor,
																std::array<int, 4> searchSpace,
																std::string const& method = "zncc");

pybind11::array_t<disp_t> extractRawDisparity(pybind11::array_t<float> costVolume,
											   std::string const& cv_type = "score");

pybind11::array_t<disp_t> extractDisparityDP(pybind11::array_t<float> costVolume,
											  std::string const& cv_type = "score");

pybind11::array_t<disp_t> extractRawDisparity2d(pybind11::array_t<float> costVolume,
												 std::string const& cv_type = "score");

pybind11::array_t<disp_t> offset2dDisparity( pybind11::array_t<disp_t> rawDisp,
											  std::array<int, 4> searchSpace);

pybind11::array_t<float> getGuidedAggregatedCostVolume(pybind11::array_t<float> costVolume,
													   pybind11::array guide,
													   uint8_t h_radius,
													   uint8_t v_radius,
													   float sigma);

pybind11::array_t<float> getTruncatedCostVolume(pybind11::array_t<float> costVolume,
												 pybind11::array_t<disp_t> rawDisp,
												 uint8_t h_radius,
												 uint8_t v_radius,
												 uint8_t cost_vol_radius = 1,
												 std::string const& dir = "r2l",
												 std::string const& tcv_type = "same");

pybind11::array_t<float> getTruncatedCostVolume2d(pybind11::array_t<float> costVolume,
												   pybind11::array_t<disp_t> rawDisp,
												   uint8_t cost_vol_radius_h = 1,
												   uint8_t cost_vol_radius_v = 1);

pybind11::array_t<float> getInBound(pybind11::array_t<disp_t> rawDisp,
									 uint32_t width,
									 uint8_t h_radius,
									 uint8_t v_radius,
									 uint8_t cost_vol_radius = 1,
									 std::string const& dir = "r2l",
									 std::string const& tcv_type = "same");

pybind11::array_t<float> getCostRefinedDisparity(pybind11::array_t<float> truncatedCostVolume,
												 pybind11::array_t<disp_t> rawDisp,
												 std::string const& interpolation = "parabola");

pybind11::array_t<float> getImageRefinedDisparity(pybind11::array img_l,
												  pybind11::array img_r,
												  pybind11::array_t<disp_t> rawDisp,
												  uint8_t v_radius,
												  uint8_t h_radius,
												  std::string const& costFunc = "ncc",
												  const std::string &direction = "r2l");

pybind11::array_t<float> getCostRefinedDisparity2d(pybind11::array_t<float> truncatedCostVolume,
													pybind11::array_t<disp_t> rawDisp,
													std::string const& interpolation = "parabola",
													bool assumeIsotropy = true,
													bool use2dFunctionMatching = false);

pybind11::array_t<float> computeRefinedDisparity(pybind11::array img_l,
												 pybind11::array img_r,
												 uint32_t width,
												 uint8_t v_radius,
												 uint8_t h_radius,
												 std::string const& method = "ncc",
												 std::string const& interpolate = "image",
												 bool preNormalize = false,
												 const std::string &direction = "r2l");

pybind11::array_t<float> computeRefinedDisparityWithCompressor(pybind11::array img_l,
															   pybind11::array img_r,
															   pybind11::array_t<int> compressor_mask,
															   uint32_t width,
															   std::string const& method = "ncc",
															   std::string const& interpolate = "image",
															   bool preNormalize = false,
															   const std::string &direction = "r2l");

pybind11::array_t<float> computeRefinedDisparity2d(pybind11::array img_l,
													pybind11::array img_r,
													std::array<int,4> searchRegion,
													uint8_t v_radius,
													uint8_t h_radius,
													std::string const& method = "ncc",
													std::string const& interpolate = "image",
													std::string const& contiguity = "queen",
													bool preNormalize = false);

pybind11::array_t<float> computeRefinedDisparity2dWithCompressor(pybind11::array img_l,
																  pybind11::array img_r,
																  std::array<int,4> searchRegion,
																  pybind11::array_t<int> compressor,
																  std::string const& method = "ncc",
																  std::string const& interpolate = "image",
																  std::string const& contiguity = "queen",
																  bool preNormalize = false);

pybind11::array_t<float> computeImageRefinedDisparity(pybind11::array img_l,
													  pybind11::array img_r,
													  uint32_t width,
													  uint8_t v_radius,
													  uint8_t h_radius,
													  std::string const& method = "ncc",
													  bool preNormalize = false,
													  const std::string &direction = "r2l");

pybind11::array_t<float> computeImageRefinedDisparityWithCompressor(pybind11::array img_l,
																	 pybind11::array img_r,
																	 pybind11::array_t<int> compressor,
																	 uint32_t width,
																	 std::string const& method = "ncc",
																	 bool preNormalize = false,
																	const std::string &direction = "r2l");

pybind11::array_t<float> computeImageRefinedDisparity2d(pybind11::array img_l,
														 pybind11::array img_r,
														 std::array<int,4> searchRegion,
														 uint8_t v_radius,
														 uint8_t h_radius,
														 std::string const& method,
														 std::string const& contiguity,
														 bool preNormalize,
														 bool useSymmetricBarycentricCoordinates);

pybind11::array_t<float> computeImageRefinedDisparity2dWithCompressor(pybind11::array img_l,
																	   pybind11::array img_r,
																	   std::array<int,4> searchRegion,
																	   pybind11::array_t<int> compressor,
																	   std::string const& method,
																	   std::string const& contiguity,
																	   bool preNormalize,
																	   bool useSymmetricBarycentricCoordinates);

pybind11::array_t<float> computeBarycentricImageRefinedDisparity(pybind11::array img_l,
																  pybind11::array img_r,
																  uint32_t width,
																  uint8_t v_radius,
																  uint8_t h_radius,
																  std::string const& method = "ncc",
																  bool preNormalize = false,
																 const std::string &direction = "r2l");

pybind11::array_t<float> computeBarycentricImageRefinedDisparityWithCompressor(pybind11::array img_l,
																				pybind11::array img_r,
																				pybind11::array_t<int> compressor,
																				uint32_t width,
																				std::string const& method = "ncc",
																				bool preNormalize = false,
																			   const std::string &direction = "r2l");

pybind11::array_t<float> computeSymmetricRefinedDisparity(pybind11::array img_l,
														   pybind11::array img_r,
														   uint32_t width,
														   uint8_t v_radius,
														   uint8_t h_radius,
														   std::string const& method = "ncc",
														   bool preNormalize = false,
														  const std::string &direction = "r2l");

pybind11::array_t<float> computeSymmetricRefinedDisparityWithCompressor(pybind11::array img_l,
																		 pybind11::array img_r,
																		 pybind11::array_t<int> compressor,
																		 uint32_t width,
																		 std::string const& method = "ncc",
																		 bool preNormalize = false,
																		const std::string &direction = "r2l");

float getProportionCovered(pybind11::array disp_l,
						   pybind11::array disp_r);

pybind11::array_t<float> getCoverMask(pybind11::array disp_l,
									  pybind11::array disp_r);

#endif // PARALLAX_H
