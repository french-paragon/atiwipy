#ifndef SAVEPOINTCLOUD_H
#define SAVEPOINTCLOUD_H

#include <pybind11/pytypes.h>
#include <pybind11/numpy.h>

int saveXYZRGBpointcloud(std::string file, pybind11::array_t<float> geom, pybind11::array color, bool correctGamma = true);

#endif // SAVEPOINTCLOUD_H
