#include "parallax.h"

#include "transforms.h"
#include "utils/type_utils.h"
#include "utils/arrays_utils.h"
#include "math/splineinterpolationfunc.h"

#include "LibStevi/correlation/cross_correlations.h"
#include "LibStevi/correlation/image_based_refinement.h"
#include "LibStevi/correlation/census.h"
#include "LibStevi/correlation/cost_based_refinement.h"
#include "LibStevi/correlation/hierarchical.h"
#include "LibStevi/correlation/dynamic_programing_stereo.h"
#include "LibStevi/correlation/guided_cost_filtering.h"

#include "LibStevi/statistics/stereo_covering.h"

#include <time.h>
#include <vector>
#include <limits>
#include <bitset>

#include <pybind11/pybind11.h>
#include <pybind11/numpy.h>

namespace py = pybind11;
namespace Correlation = StereoVision::Correlation;


template <StereoVision::Correlation::matchingFunctions matchingFunc,
		  StereoVision::Correlation::dispDirection dispDir,
		  typename T_L, typename T_R>
inline Multidim::Array<float, 3> callCV(pybind11::array img_l,
										pybind11::array img_r,
										uint8_t h_radius,
										uint8_t v_radius,
										uint32_t width,
										int32_t offset) {

	std::vector<long> l_shape = img_l.request().shape;
	std::vector<long> r_shape = img_r.request().shape;

	std::vector<long> l_strides = img_l.request().strides;
	std::vector<long> r_strides = img_r.request().strides;

	if (l_shape != r_shape) {
		throw std::invalid_argument("computeCostVolume needs arrays of the same shape !");
	}

	if (l_shape.size() != 2 and l_shape.size() != 3) {
		throw std::invalid_argument("computeCostVolume needs arrays with ndims = 2 or 3 !");
	}

	if (l_shape.size() == 3) {

		Multidim::Array<T_L,3> imL(static_cast<T_L*>(img_l.mutable_data(0)),
							   {static_cast<int>(l_shape[0]),
								static_cast<int>(l_shape[1]),
								static_cast<int>(l_shape[2])},
							   {static_cast<int>(l_strides[0]/sizeof (T_L)),
								static_cast<int>(l_strides[1]/sizeof (T_L)),
								static_cast<int>(l_strides[2]/sizeof (T_L))});

		Multidim::Array<T_R,3> imR(static_cast<T_R*>(img_r.mutable_data(0)),
							   {static_cast<int>(r_shape[0]),
								static_cast<int>(r_shape[1]),
								static_cast<int>(r_shape[2])},
							   {static_cast<int>(r_strides[0]/sizeof (T_R)),
								static_cast<int>(r_strides[1]/sizeof (T_R)),
								static_cast<int>(r_strides[2]/sizeof (T_R))});

		Multidim::Array<T_L, 3> left_feature_volume = StereoVision::Correlation::unfold<T_L, T_L>(h_radius, v_radius, imL);
		Multidim::Array<T_R, 3> right_feature_volume = StereoVision::Correlation::unfold<T_R, T_R>(h_radius, v_radius, imR);

		StereoVision::Correlation::searchOffset<1> searchRange(offset, width+offset);

		return StereoVision::Correlation::featureVolume2CostVolume
				<matchingFunc, T_L, T_R, StereoVision::Correlation::searchOffset<1>, dispDir, float>
				(left_feature_volume, right_feature_volume, searchRange);

	} else {

		Multidim::Array<T_L,2> imL(static_cast<T_L*>(img_l.mutable_data(0)),
							   {static_cast<int>(l_shape[0]), static_cast<int>(l_shape[1])},
							   {static_cast<int>(l_strides[0]/sizeof (T_L)), static_cast<int>(l_strides[1]/sizeof (T_L))});

		Multidim::Array<T_R,2> imR(static_cast<T_R*>(img_r.mutable_data(0)),
							   {static_cast<int>(r_shape[0]), static_cast<int>(r_shape[1])},
							   {static_cast<int>(r_strides[0]/sizeof (T_R)), static_cast<int>(r_strides[1]/sizeof (T_R))});

		Multidim::Array<T_L, 3> left_feature_volume = StereoVision::Correlation::unfold<T_L, T_L>(h_radius, v_radius, imL);
		Multidim::Array<T_R, 3> right_feature_volume = StereoVision::Correlation::unfold<T_R, T_R>(h_radius, v_radius, imR);

		StereoVision::Correlation::searchOffset<1> searchRange(offset, width+offset);

		return StereoVision::Correlation::featureVolume2CostVolume
				<matchingFunc, T_L, T_R, StereoVision::Correlation::searchOffset<1>, dispDir, float>
				(left_feature_volume, right_feature_volume, searchRange);
	}
}

template <StereoVision::Correlation::matchingFunctions matchingFunc,
		  StereoVision::Correlation::dispDirection dispDir,
		  typename T_L, typename T_R>
inline pybind11::array_t<float> callCostVolume(pybind11::array img_l,
											   pybind11::array img_r,
											   uint8_t h_radius,
											   uint8_t v_radius,
											   uint32_t width,
											   int32_t offset) {

	return Multidim2NumpyArray(
				callCV<matchingFunc, dispDir, T_L, T_R>(img_l, img_r, h_radius, v_radius, width, offset)
			);
}

template <StereoVision::Correlation::matchingFunctions matchingFunc,
		  StereoVision::Correlation::dispDirection dispDir,
		  typename T_L, typename T_R>
inline Multidim::Array<float, 3> callCV(pybind11::array img_l,
										pybind11::array img_r,
										Multidim::Array<int,2> const& compressor,
										uint32_t width,
										int32_t offset) {

	std::vector<long> l_shape = img_l.request().shape;
	std::vector<long> r_shape = img_r.request().shape;

	std::vector<long> l_strides = img_l.request().strides;
	std::vector<long> r_strides = img_r.request().strides;

	if (l_shape != r_shape) {
		throw std::invalid_argument("computeCostVolume needs arrays of the same shape !");
	}

	if (l_shape.size() != 2 and l_shape.size() != 3) {
		throw std::invalid_argument("computeCostVolume needs arrays with ndims = 2 or 3 !");
	}

	StereoVision::Correlation::UnFoldCompressor cmpr(compressor);

	if (l_shape.size() == 3) {

		Multidim::Array<T_L,3> imL(static_cast<T_L*>(img_l.mutable_data(0)),
							   {static_cast<int>(l_shape[0]),
								static_cast<int>(l_shape[1]),
								static_cast<int>(l_shape[2])},
							   {static_cast<int>(l_strides[0]/sizeof (T_L)),
								static_cast<int>(l_strides[1]/sizeof (T_L)),
								static_cast<int>(l_strides[2]/sizeof (T_L))});

		Multidim::Array<T_R,3> imR(static_cast<T_R*>(img_r.mutable_data(0)),
							   {static_cast<int>(r_shape[0]),
								static_cast<int>(r_shape[1]),
								static_cast<int>(r_shape[2])},
							   {static_cast<int>(r_strides[0]/sizeof (T_R)),
								static_cast<int>(r_strides[1]/sizeof (T_R)),
								static_cast<int>(r_strides[2]/sizeof (T_R))});

		Multidim::Array<T_L, 3> left_feature_volume = StereoVision::Correlation::unfold<T_L, T_L>(cmpr, imL);
		Multidim::Array<T_R, 3> right_feature_volume = StereoVision::Correlation::unfold<T_R, T_R>(cmpr, imR);

		StereoVision::Correlation::searchOffset<1> searchRange(offset, width+offset);

		return StereoVision::Correlation::featureVolume2CostVolume
				<matchingFunc, T_L, T_R, StereoVision::Correlation::searchOffset<1>, dispDir, float>
				(left_feature_volume, right_feature_volume, searchRange);

	} else {

		Multidim::Array<T_L,2> imL(static_cast<T_L*>(img_l.mutable_data(0)),
							   {static_cast<int>(l_shape[0]), static_cast<int>(l_shape[1])},
							   {static_cast<int>(l_strides[0]/sizeof (T_L)), static_cast<int>(l_strides[1]/sizeof (T_L))});

		Multidim::Array<T_R,2> imR(static_cast<T_R*>(img_r.mutable_data(0)),
							   {static_cast<int>(r_shape[0]), static_cast<int>(r_shape[1])},
							   {static_cast<int>(r_strides[0]/sizeof (T_R)), static_cast<int>(r_strides[1]/sizeof (T_R))});

		Multidim::Array<T_L, 3> left_feature_volume = StereoVision::Correlation::unfold<T_L, T_L>(cmpr, imL);
		Multidim::Array<T_R, 3> right_feature_volume = StereoVision::Correlation::unfold<T_R, T_R>(cmpr, imR);

		StereoVision::Correlation::searchOffset<1> searchRange(offset, width+offset);

		return StereoVision::Correlation::featureVolume2CostVolume
				<matchingFunc, T_L, T_R, StereoVision::Correlation::searchOffset<1>, dispDir, float>
				(left_feature_volume, right_feature_volume, searchRange);
	}
}

template <StereoVision::Correlation::matchingFunctions matchingFunc,
		  StereoVision::Correlation::dispDirection dispDir,
		  typename T_L, typename T_R>
inline pybind11::array_t<float> callCostVolume(pybind11::array img_l,
												pybind11::array img_r,
												Multidim::Array<int,2> const& compressor,
												uint32_t width,
											   int32_t offset) {
	return Multidim2NumpyArray(
				callCV<matchingFunc, dispDir, T_L, T_R>(img_l, img_r, compressor, width, offset)
			);
}

template <StereoVision::Correlation::matchingFunctions matchingFunc, typename T_L, typename T_R>
inline pybind11::array_t<float> callCostVolume2d(pybind11::array img_l,
												  pybind11::array img_r,
												  uint8_t h_radius,
												  uint8_t v_radius,
												  Correlation::searchOffset<2> const& searchWindow) {

	constexpr Correlation::dispDirection dispDir = StereoVision::Correlation::dispDirection::RightToLeft;

	std::vector<long> l_shape = img_l.request().shape;
	std::vector<long> r_shape = img_r.request().shape;

	std::vector<long> l_strides = img_l.request().strides;
	std::vector<long> r_strides = img_r.request().strides;

	if (l_shape != r_shape) {
		throw std::invalid_argument("computeCostVolume needs arrays of the same shape !");
	}

	if (l_shape.size() != 2 and l_shape.size() != 3) {
		throw std::invalid_argument("computeCostVolume needs arrays with ndims = 2 or 3 !");
	}

	if (l_shape.size() == 3) {
		return Multidim2NumpyArray(
					Correlation::unfoldBased2dDisparityCostVolume<matchingFunc, T_L, T_R, 3, dispDir>(
					Multidim::Array<T_L,3>(static_cast<T_L*>(img_l.mutable_data(0)),
										   {static_cast<int>(l_shape[0]),
											static_cast<int>(l_shape[1]),
											static_cast<int>(l_shape[2])},
										   {static_cast<int>(l_strides[0]/sizeof (T_L)),
											static_cast<int>(l_strides[1]/sizeof (T_L)),
											static_cast<int>(l_strides[2]/sizeof (T_L))}),
					Multidim::Array<T_R,3>(static_cast<T_R*>(img_r.mutable_data(0)),
										   {static_cast<int>(r_shape[0]),
											static_cast<int>(r_shape[1]),
											static_cast<int>(r_shape[2])},
										   {static_cast<int>(r_strides[0]/sizeof (T_R)),
											static_cast<int>(r_strides[1]/sizeof (T_R)),
											static_cast<int>(r_strides[2]/sizeof (T_R))}),
					h_radius,
					v_radius,
					searchWindow)
				);

	} else {
		return Multidim2NumpyArray(
					Correlation::unfoldBased2dDisparityCostVolume<matchingFunc, T_L, T_R, 2, dispDir>(
					Multidim::Array<T_L,2>(static_cast<T_L*>(img_l.mutable_data(0)),
										   {static_cast<int>(l_shape[0]), static_cast<int>(l_shape[1])},
										   {static_cast<int>(l_strides[0]/sizeof (T_L)), static_cast<int>(l_strides[1]/sizeof (T_L))}),
					Multidim::Array<T_R,2>(static_cast<T_R*>(img_r.mutable_data(0)),
										   {static_cast<int>(r_shape[0]), static_cast<int>(r_shape[1])},
										   {static_cast<int>(r_strides[0]/sizeof (T_R)), static_cast<int>(r_strides[1]/sizeof (T_R))}),
					h_radius,
					v_radius,
					searchWindow)
				);
	}
}

template <StereoVision::Correlation::matchingFunctions matchingFunc, typename T_L, typename T_R>
inline pybind11::array_t<float> callCostVolume2d(pybind11::array img_l,
												  pybind11::array img_r,
												  Multidim::Array<int,2> const& compressor,
												  Correlation::searchOffset<2> const& searchWindow) {

	constexpr Correlation::dispDirection dispDir = StereoVision::Correlation::dispDirection::RightToLeft;

	std::vector<long> l_shape = img_l.request().shape;
	std::vector<long> r_shape = img_r.request().shape;

	std::vector<long> l_strides = img_l.request().strides;
	std::vector<long> r_strides = img_r.request().strides;

	if (l_shape != r_shape) {
		throw std::invalid_argument("computeCostVolume needs arrays of the same shape !");
	}

	if (l_shape.size() != 2 and l_shape.size() != 3) {
		throw std::invalid_argument("computeCostVolume needs arrays with ndims = 2 or 3 !");
	}

	StereoVision::Correlation::UnFoldCompressor cmpr(compressor);

	if (l_shape.size() == 3) {
		return Multidim2NumpyArray(
					Correlation::unfoldBased2dDisparityCostVolume<matchingFunc, T_L, T_R, 3, dispDir>(
					Multidim::Array<T_L,3>(static_cast<T_L*>(img_l.mutable_data(0)),
										   {static_cast<int>(l_shape[0]),
											static_cast<int>(l_shape[1]),
											static_cast<int>(l_shape[2])},
										   {static_cast<int>(l_strides[0]/sizeof (T_L)),
											static_cast<int>(l_strides[1]/sizeof (T_L)),
											static_cast<int>(l_strides[2]/sizeof (T_L))}),
					Multidim::Array<T_R,3>(static_cast<T_R*>(img_r.mutable_data(0)),
										   {static_cast<int>(r_shape[0]),
											static_cast<int>(r_shape[1]),
											static_cast<int>(r_shape[2])},
										   {static_cast<int>(r_strides[0]/sizeof (T_R)),
											static_cast<int>(r_strides[1]/sizeof (T_R)),
											static_cast<int>(r_strides[2]/sizeof (T_R))}),
					cmpr,
					searchWindow)
				);

	} else {
		return Multidim2NumpyArray(
					Correlation::unfoldBased2dDisparityCostVolume<matchingFunc, T_L, T_R, 2, dispDir>(
					Multidim::Array<T_L,2>(static_cast<T_L*>(img_l.mutable_data(0)),
										   {static_cast<int>(l_shape[0]), static_cast<int>(l_shape[1])},
										   {static_cast<int>(l_strides[0]/sizeof (T_L)), static_cast<int>(l_strides[1]/sizeof (T_L))}),
					Multidim::Array<T_R,2>(static_cast<T_R*>(img_r.mutable_data(0)),
										   {static_cast<int>(r_shape[0]), static_cast<int>(r_shape[1])},
										   {static_cast<int>(r_strides[0]/sizeof (T_R)), static_cast<int>(r_strides[1]/sizeof (T_R))}),
					cmpr,
					searchWindow)
				);
	}
}


template <StereoVision::Correlation::matchingFunctions matchingFunc,
		  StereoVision::Correlation::dispDirection dispDir,
		  typename T_L, typename T_R>
inline pybind11::array_t<disp_t> callDisp(pybind11::array img_l,
										   pybind11::array img_r,
										   uint8_t h_radius,
										   uint8_t v_radius,
										   uint32_t width,
										  int32_t offset)
{

	constexpr auto cvStrat = StereoVision::Correlation::MatchingFunctionTraits<matchingFunc>::extractionStrategy;
	return Multidim2NumpyArray(
				StereoVision::Correlation::extractSelectedIndex<cvStrat, float>(
					callCV<matchingFunc, dispDir, T_L, T_R>(img_l, img_r, h_radius, v_radius, width, offset))
			);
}

template <StereoVision::Correlation::matchingFunctions matchingFunc,
		  StereoVision::Correlation::dispDirection dispDir,
		  typename T_L, typename T_R>
inline pybind11::array_t<disp_t> callDisp(pybind11::array img_l,
										  pybind11::array img_r,
										  Multidim::Array<int,2> const& compressor,
										  uint32_t width,
										  int32_t offset)
{

	constexpr auto cvStrat = StereoVision::Correlation::MatchingFunctionTraits<matchingFunc>::extractionStrategy;
	return Multidim2NumpyArray(
				StereoVision::Correlation::extractSelectedIndex<cvStrat, float>(
					callCV<matchingFunc, dispDir, T_L, T_R>(img_l, img_r, compressor, width, offset))
			);
}


const int MAX_HIERARCHICAL_DISP_DEPTH = 6;

template<int depth, StereoVision::Correlation::matchingFunctions matchFunc,
		 StereoVision::Correlation::dispDirection dispDir,
		 class T_L, class T_R>
pybind11::tuple callHierarchicalDisparity(pybind11::array & img_l,
										  pybind11::array & img_r,
										  uint32_t width,
										  std::vector<uint8_t> const& v_radiuses,
										  std::vector<uint8_t> const& h_radiuses,
										  disp_t truncated_cost_volume_radius) {

	std::vector<long> l_shape = img_l.request().shape;
	std::vector<long> r_shape = img_r.request().shape;

	std::vector<long> l_strides = img_l.request().strides;
	std::vector<long> r_strides = img_r.request().strides;

	if (l_shape != r_shape) {
		throw std::invalid_argument("computeDisaprityHierarchical needs images of the same shape !");
	}

	if (l_shape.size() != 2 and l_shape.size() != 3) {
		throw std::invalid_argument("computeDisaprityHierarchical needs arrays with ndims = 2 or 3 !");
	}

	StereoVision::Correlation::OffsetedCostVolume<float> result;
	std::array<uint8_t, depth+1> v_radiuses_arr;
	std::array<uint8_t, depth+1> h_radiuses_arr;

	for (int i = 0; i <= depth; i++) {
		v_radiuses_arr[i] = v_radiuses[i];
		h_radiuses_arr[i] = h_radiuses[i];
	}

	if (l_shape.size() == 3) {

		result = StereoVision::Correlation::hiearchicalTruncatedCostVolume
				<matchFunc, depth, T_L, T_R, 3, dispDir>
				(Multidim::Array<T_L,3>(static_cast<T_L*>(img_l.mutable_data(0)),
										{static_cast<int>(l_shape[0]),
										 static_cast<int>(l_shape[1]),
										 static_cast<int>(l_shape[2])},
										{static_cast<int>(l_strides[0]/sizeof (T_L)),
										 static_cast<int>(l_strides[1]/sizeof (T_L)),
										 static_cast<int>(l_strides[2]/sizeof (T_L))}),
				 Multidim::Array<T_R,3>(static_cast<T_R*>(img_r.mutable_data(0)),
										{static_cast<int>(r_shape[0]),
										 static_cast<int>(r_shape[1]),
										 static_cast<int>(r_shape[2])},
										{static_cast<int>(r_strides[0]/sizeof (T_R)),
										 static_cast<int>(r_strides[1]/sizeof (T_R)),
										 static_cast<int>(r_strides[2]/sizeof (T_R))}),
				 v_radiuses_arr,
				 h_radiuses_arr,
				 width,
				 truncated_cost_volume_radius);

	} else {

		result = StereoVision::Correlation::hiearchicalTruncatedCostVolume
				<matchFunc, depth, T_L, T_R, 2, dispDir>
				(Multidim::Array<T_L,2>(static_cast<T_L*>(img_l.mutable_data(0)),
										{static_cast<int>(l_shape[0]),
										 static_cast<int>(l_shape[1])},
										{static_cast<int>(l_strides[0]/sizeof (T_L)),
										 static_cast<int>(l_strides[1]/sizeof (T_L))}),
				 Multidim::Array<T_R,2>(static_cast<T_R*>(img_r.mutable_data(0)),
										{static_cast<int>(r_shape[0]),
										 static_cast<int>(r_shape[1])},
										{static_cast<int>(r_strides[0]/sizeof (T_R)),
										 static_cast<int>(r_strides[1]/sizeof (T_R))}),
				 v_radiuses_arr,
				 h_radiuses_arr,
				 width,
				 truncated_cost_volume_radius);
	}

	return pybind11::make_tuple(
				Multidim2NumpyArray(result.disp_estimate),
				Multidim2NumpyArray(result.truncated_cost_volume)
			);
}

template<StereoVision::Correlation::matchingFunctions matchFunc,
		 StereoVision::Correlation::dispDirection dispDir,
		 class T_L, class T_R>
pybind11::tuple setDepthCallHierarchicalDisparity(pybind11::array & img_l,
												  pybind11::array & img_r,
												  uint32_t width,
												  std::vector<uint8_t> const& v_radiuses,
												  std::vector<uint8_t> const& h_radiuses,
												  disp_t truncated_cost_volume_radius) {

	int depth = v_radiuses.size()-1;

	switch (depth) {
	case 1:
		return callHierarchicalDisparity<1, matchFunc, dispDir, T_L, T_R>
				(img_l, img_r, width, v_radiuses, h_radiuses, truncated_cost_volume_radius);
	case 2:
		return callHierarchicalDisparity<2, matchFunc, dispDir, T_L, T_R>
				(img_l, img_r, width, v_radiuses, h_radiuses, truncated_cost_volume_radius);
	case 3:
		return callHierarchicalDisparity<3, matchFunc, dispDir, T_L, T_R>
				(img_l, img_r, width, v_radiuses, h_radiuses, truncated_cost_volume_radius);
	case 4:
		return callHierarchicalDisparity<4, matchFunc, dispDir, T_L, T_R>
				(img_l, img_r, width, v_radiuses, h_radiuses, truncated_cost_volume_radius);
	case 5:
		return callHierarchicalDisparity<5, matchFunc, dispDir, T_L, T_R>
				(img_l, img_r, width, v_radiuses, h_radiuses, truncated_cost_volume_radius);
	case 6:
		return callHierarchicalDisparity<6, matchFunc, dispDir, T_L, T_R>
				(img_l, img_r, width, v_radiuses, h_radiuses, truncated_cost_volume_radius);
	}

	return py::make_tuple();
}

template<StereoVision::Correlation::matchingFunctions matchFunc,
		 StereoVision::Correlation::dispDirection dispDir>
pybind11::tuple callTypedHierarchicalDisparity(pybind11::array & img_l,
											   ArraysFormat l_format,
											   pybind11::array & img_r,
											   ArraysFormat r_format,
											   uint32_t width,
											   std::vector<uint8_t> const& v_radiuses,
											   std::vector<uint8_t> const& h_radiuses,
											   disp_t truncated_cost_volume_radius) {

	if (l_format == T_FLOAT) {

		if (r_format == T_FLOAT) {

			return setDepthCallHierarchicalDisparity<matchFunc, dispDir, float, float>
					(img_l, img_r, width, v_radiuses, h_radiuses, truncated_cost_volume_radius);

		} else if (r_format == T_UINT8) {

			return setDepthCallHierarchicalDisparity<matchFunc, dispDir, float, uint8_t>
					(img_l, img_r, width, v_radiuses, h_radiuses, truncated_cost_volume_radius);

		}

	} else if (l_format == T_UINT8) {

		if (r_format == T_FLOAT) {

			return setDepthCallHierarchicalDisparity<matchFunc, dispDir, uint8_t, float>
					(img_l, img_r, width, v_radiuses, h_radiuses, truncated_cost_volume_radius);

		} else if (r_format == T_UINT8) {

			return setDepthCallHierarchicalDisparity<matchFunc, dispDir, uint8_t, uint8_t>
					(img_l, img_r, width, v_radiuses, h_radiuses, truncated_cost_volume_radius);

		}

	}

	return py::make_tuple();

}

template <StereoVision::Correlation::matchingFunctions matchingFunc, typename T_L, typename T_R>
inline Multidim::Array<disp_t, 3> callDisp2d(pybind11::array img_l,
										   pybind11::array img_r,
										   uint8_t h_radius,
										   uint8_t v_radius,
										   Correlation::searchOffset<2> const& searchWindow)
{

	constexpr auto cvStrat = Correlation::MatchingFunctionTraits<matchingFunc>::extractionStrategy;
	return Correlation::selected2dIndexToDisp(Correlation::extractSelected2dIndex<cvStrat, float>(
												  callCostVolume2d<matchingFunc, T_L, T_R>(img_l, img_r, h_radius, v_radius, searchWindow)),
											  searchWindow);
}

template <StereoVision::Correlation::matchingFunctions matchingFunc, typename T_L, typename T_R>
inline Multidim::Array<disp_t, 3> callDisp2d(pybind11::array img_l,
													pybind11::array img_r,
													Multidim::Array<int,2> const& compressor,
													Correlation::searchOffset<2> const& searchWindow)
{

	constexpr auto cvStrat = Correlation::MatchingFunctionTraits<matchingFunc>::extractionStrategy;
	return Correlation::selected2dIndexToDisp(Correlation::extractSelected2dIndex<cvStrat, float>(
												  callCostVolume2d<matchingFunc, T_L, T_R>(img_l, img_r, compressor, searchWindow)),
											  searchWindow);
}


template <typename T_L, typename T_R, StereoVision::Correlation::dispDirection dispDir = StereoVision::Correlation::dispDirection::RightToLeft>
inline pybind11::array_t<disp_t> callCensusDisp(pybind11::array img_l,
													pybind11::array img_r,
													uint8_t h_radius,
													uint8_t v_radius,
													uint32_t width,
													bool allowOutLimit)
{

	constexpr auto FCost = StereoVision::Correlation::dispExtractionStartegy::Cost;
	constexpr StereoVision::Correlation::matchingFunctions Census = StereoVision::Correlation::matchingFunctions::CENSUS;
	using cv_t = StereoVision::Correlation::hamming_cv_t;

	std::vector<long> l_shape = img_l.request().shape;
	std::vector<long> r_shape = img_r.request().shape;

	std::vector<long> l_strides = img_l.request().strides;
	std::vector<long> r_strides = img_r.request().strides;

	if (l_shape != r_shape) {
		throw std::invalid_argument("computeCostVolume needs arrays of the same shape !");
	}

	if (l_shape.size() != 2 and l_shape.size() != 3) {
		throw std::invalid_argument("computeCostVolume needs arrays with ndims = 2 or 3 !");
	}

	if (allowOutLimit) {

		if (l_shape.size() == 3) {
			return Multidim2NumpyArray(
						StereoVision::Correlation::extractSelectedIndex<FCost, cv_t>(
						StereoVision::Correlation::unfoldBasedCostVolume<Census, T_L, T_R, 3, dispDir, cv_t>(
						Multidim::Array<T_L,3>(static_cast<T_L*>(img_l.mutable_data(0)),
											   {static_cast<int>(l_shape[0]),
												static_cast<int>(l_shape[1]),
												static_cast<int>(l_shape[2])},
											   {static_cast<int>(l_strides[0]/sizeof (T_L)),
												static_cast<int>(l_strides[1]/sizeof (T_L)),
												static_cast<int>(l_strides[2]/sizeof (T_L))}),
						Multidim::Array<T_R,3>(static_cast<T_R*>(img_r.mutable_data(0)),
											   {static_cast<int>(r_shape[0]),
												static_cast<int>(r_shape[1]),
												static_cast<int>(r_shape[2])},
											   {static_cast<int>(r_strides[0]/sizeof (T_R)),
												static_cast<int>(r_strides[1]/sizeof (T_R)),
												static_cast<int>(r_strides[2]/sizeof (T_R))}),
						h_radius,
						v_radius,
						width))
					);
		} else {
			return Multidim2NumpyArray(
						StereoVision::Correlation::extractSelectedIndex<FCost, cv_t>(
						StereoVision::Correlation::unfoldBasedCostVolume<Census, T_L, T_R, 2, dispDir, cv_t>(
						Multidim::Array<T_L,2>(static_cast<T_L*>(img_l.mutable_data(0)),
											   {static_cast<int>(l_shape[0]), static_cast<int>(l_shape[1])},
											   {static_cast<int>(l_strides[0]/sizeof (T_L)), static_cast<int>(l_strides[1]/sizeof (T_L))}),
						Multidim::Array<T_R,2>(static_cast<T_R*>(img_r.mutable_data(0)),
											   {static_cast<int>(r_shape[0]), static_cast<int>(r_shape[1])},
											   {static_cast<int>(r_strides[0]/sizeof (T_R)), static_cast<int>(r_strides[1]/sizeof (T_R))}),
						h_radius,
						v_radius,
						width))
					);
		}
	} else {

		if (l_shape.size() == 3) {
			return Multidim2NumpyArray(
						StereoVision::Correlation::extractSelectedIndex<FCost, cv_t>(
						StereoVision::Correlation::unfoldBasedCostVolume<Census, T_L, T_R, 3, dispDir, cv_t>(
						Multidim::Array<T_L,3>(static_cast<T_L*>(img_l.mutable_data(0)),
											   {static_cast<int>(l_shape[0]),
												static_cast<int>(l_shape[1]),
												static_cast<int>(l_shape[2])},
											   {static_cast<int>(l_strides[0]/sizeof (T_L)),
												static_cast<int>(l_strides[1]/sizeof (T_L)),
												static_cast<int>(l_strides[2]/sizeof (T_L))}),
						Multidim::Array<T_R,3>(static_cast<T_R*>(img_r.mutable_data(0)),
											   {static_cast<int>(r_shape[0]),
												static_cast<int>(r_shape[1]),
												static_cast<int>(r_shape[2])},
											   {static_cast<int>(r_strides[0]/sizeof (T_R)),
												static_cast<int>(r_strides[1]/sizeof (T_R)),
												static_cast<int>(r_strides[2]/sizeof (T_R))}),
						h_radius,
						v_radius,
						width))
					);
		} else {
			return Multidim2NumpyArray(
						StereoVision::Correlation::extractSelectedIndex<FCost, cv_t>(
						StereoVision::Correlation::unfoldBasedCostVolume<Census, T_L, T_R, 2, dispDir, cv_t>(
						Multidim::Array<T_L,2>(static_cast<T_L*>(img_l.mutable_data(0)),
											   {static_cast<int>(l_shape[0]), static_cast<int>(l_shape[1])},
											   {static_cast<int>(l_strides[0]/sizeof (T_L)), static_cast<int>(l_strides[1]/sizeof (T_L))}),
						Multidim::Array<T_R,2>(static_cast<T_R*>(img_r.mutable_data(0)),
											   {static_cast<int>(r_shape[0]), static_cast<int>(r_shape[1])},
											   {static_cast<int>(r_strides[0]/sizeof (T_R)), static_cast<int>(r_strides[1]/sizeof (T_R))}),
						h_radius,
						v_radius,
						width))
					);
		}
	}
}


template <StereoVision::Correlation::matchingFunctions matchingFunc,
		  StereoVision::Correlation::dispDirection dispDir,
		  typename T_L, typename T_R>
inline pybind11::array_t<float> callRefinedBarycentricDisp(pybind11::array img_l,
															pybind11::array img_r,
															uint8_t h_radius,
															uint8_t v_radius,
															uint32_t width,
															bool preNormalize)
{

	std::vector<long> l_shape = img_l.request().shape;
	std::vector<long> r_shape = img_r.request().shape;

	std::vector<long> l_strides = img_l.request().strides;
	std::vector<long> r_strides = img_r.request().strides;

	if (l_shape != r_shape) {
		throw std::invalid_argument("computeCostVolume needs arrays of the same shape !");
	}

	if (l_shape.size() != 2 and l_shape.size() != 3) {
		throw std::invalid_argument("computeCostVolume needs arrays with ndims = 2 or 3 !");
	}

	if (l_shape.size() == 3) {
			return Multidim2NumpyArray(
						StereoVision::Correlation::refinedBarycentricDisp<matchingFunc, T_L, T_R, 3, dispDir>(
						Multidim::Array<T_L,3>(static_cast<T_L*>(img_l.mutable_data(0)),
											   {static_cast<int>(l_shape[0]),
												static_cast<int>(l_shape[1]),
												static_cast<int>(l_shape[2])},
											   {static_cast<int>(l_strides[0]/sizeof (T_L)),
												static_cast<int>(l_strides[1]/sizeof (T_L)),
												static_cast<int>(l_strides[2]/sizeof (T_L))}),
						Multidim::Array<T_R,3>(static_cast<T_R*>(img_r.mutable_data(0)),
											   {static_cast<int>(r_shape[0]),
												static_cast<int>(r_shape[1]),
												static_cast<int>(r_shape[2])},
											   {static_cast<int>(r_strides[0]/sizeof (T_R)),
												static_cast<int>(r_strides[1]/sizeof (T_R)),
												static_cast<int>(r_strides[2]/sizeof (T_R))}),
						h_radius,
						v_radius,
						width,
						preNormalize)
					);

	} else {
			return Multidim2NumpyArray(
						StereoVision::Correlation::refinedBarycentricDisp<matchingFunc, T_L, T_R, 2, dispDir>(
						Multidim::Array<T_L,2>(static_cast<T_L*>(img_l.mutable_data(0)),
											   {static_cast<int>(l_shape[0]), static_cast<int>(l_shape[1])},
											   {static_cast<int>(l_strides[0]/sizeof (T_L)), static_cast<int>(l_strides[1]/sizeof (T_L))}),
						Multidim::Array<T_R,2>(static_cast<T_R*>(img_r.mutable_data(0)),
											   {static_cast<int>(r_shape[0]), static_cast<int>(r_shape[1])},
											   {static_cast<int>(r_strides[0]/sizeof (T_R)), static_cast<int>(r_strides[1]/sizeof (T_R))}),
						h_radius,
						v_radius,
						width,
						preNormalize)
					);
	}
}

template <StereoVision::Correlation::matchingFunctions matchingFunc,
		  StereoVision::Correlation::dispDirection dispDir,
		  typename T_L, typename T_R>
inline pybind11::array_t<float> callRefinedBarycentricDisp(pybind11::array img_l,
															pybind11::array img_r,
															Multidim::Array<int, 2> const& compressor,
															uint32_t width,
															bool preNormalize)
{

	std::vector<long> l_shape = img_l.request().shape;
	std::vector<long> r_shape = img_r.request().shape;

	std::vector<long> l_strides = img_l.request().strides;
	std::vector<long> r_strides = img_r.request().strides;

	if (l_shape != r_shape) {
		throw std::invalid_argument("computeCostVolume needs arrays of the same shape !");
	}

	if (l_shape.size() != 2 and l_shape.size() != 3) {
		throw std::invalid_argument("computeCostVolume needs arrays with ndims = 2 or 3 !");
	}

	StereoVision::Correlation::UnFoldCompressor cmpr(compressor);

	if (l_shape.size() == 3) {
			return Multidim2NumpyArray(
						StereoVision::Correlation::refinedBarycentricDisp<matchingFunc, T_L, T_R, 3, dispDir>(
						Multidim::Array<T_L,3>(static_cast<T_L*>(img_l.mutable_data(0)),
											   {static_cast<int>(l_shape[0]),
												static_cast<int>(l_shape[1]),
												static_cast<int>(l_shape[2])},
											   {static_cast<int>(l_strides[0]/sizeof (T_L)),
												static_cast<int>(l_strides[1]/sizeof (T_L)),
												static_cast<int>(l_strides[2]/sizeof (T_L))}),
						Multidim::Array<T_R,3>(static_cast<T_R*>(img_r.mutable_data(0)),
											   {static_cast<int>(r_shape[0]),
												static_cast<int>(r_shape[1]),
												static_cast<int>(r_shape[2])},
											   {static_cast<int>(r_strides[0]/sizeof (T_R)),
												static_cast<int>(r_strides[1]/sizeof (T_R)),
												static_cast<int>(r_strides[2]/sizeof (T_R))}),
						cmpr,
						width,
						preNormalize)
					);

	} else {
			return Multidim2NumpyArray(
						StereoVision::Correlation::refinedBarycentricDisp<matchingFunc, T_L, T_R, 2, dispDir>(
						Multidim::Array<T_L,2>(static_cast<T_L*>(img_l.mutable_data(0)),
											   {static_cast<int>(l_shape[0]), static_cast<int>(l_shape[1])},
											   {static_cast<int>(l_strides[0]/sizeof (T_L)), static_cast<int>(l_strides[1]/sizeof (T_L))}),
						Multidim::Array<T_R,2>(static_cast<T_R*>(img_r.mutable_data(0)),
											   {static_cast<int>(r_shape[0]), static_cast<int>(r_shape[1])},
											   {static_cast<int>(r_strides[0]/sizeof (T_R)), static_cast<int>(r_strides[1]/sizeof (T_R))}),
						cmpr,
						width,
						preNormalize)
					);
	}
}

template <StereoVision::Correlation::matchingFunctions matchingFunc,
		  typename T_L,
		  typename T_R,
		  StereoVision::Contiguity::bidimensionalContiguity contiguity = StereoVision::Contiguity::Queen>
inline pybind11::array_t<float> callRefinedBarycentricDisp2d(pybind11::array img_l,
															  pybind11::array img_r,
															  uint8_t h_radius,
															  uint8_t v_radius,
															  Correlation::searchOffset<2> const& searchWindow,
															  bool preNormalize,
															  bool useSymmetricBarycentricCoordinates)
{

	constexpr StereoVision::Correlation::dispDirection dispDir = StereoVision::Correlation::dispDirection::RightToLeft;

	std::vector<long> l_shape = img_l.request().shape;
	std::vector<long> r_shape = img_r.request().shape;

	std::vector<long> l_strides = img_l.request().strides;
	std::vector<long> r_strides = img_r.request().strides;

	if (l_shape != r_shape) {
		throw std::invalid_argument("computeCostVolume needs arrays of the same shape !");
	}

	if (l_shape.size() != 2 and l_shape.size() != 3) {
		throw std::invalid_argument("computeCostVolume needs arrays with ndims = 2 or 3 !");
	}

	if (l_shape.size() == 3) {

		Multidim::Array<T_L,3> imgL(static_cast<T_L*>(img_l.mutable_data(0)),
									{static_cast<int>(l_shape[0]),
									 static_cast<int>(l_shape[1]),
									 static_cast<int>(l_shape[2])},
									{static_cast<int>(l_strides[0]/sizeof (T_L)),
									 static_cast<int>(l_strides[1]/sizeof (T_L)),
									 static_cast<int>(l_strides[2]/sizeof (T_L))});

		Multidim::Array<T_R,3> imgR(static_cast<T_R*>(img_r.mutable_data(0)),
									{static_cast<int>(r_shape[0]),
									 static_cast<int>(r_shape[1]),
									 static_cast<int>(r_shape[2])},
									{static_cast<int>(r_strides[0]/sizeof (T_R)),
									 static_cast<int>(r_strides[1]/sizeof (T_R)),
									 static_cast<int>(r_strides[2]/sizeof (T_R))});

		if (useSymmetricBarycentricCoordinates) {
			return Multidim2NumpyArray(
						Correlation::refinedBarycentricSymmetric2dDisp<matchingFunc, T_L, T_R, 3, contiguity, dispDir>(
						imgL,
						imgR,
						h_radius,
						v_radius,
						searchWindow,
						preNormalize)
					);
		} else {
			return Multidim2NumpyArray(
						Correlation::refinedBarycentric2dDisp<matchingFunc, T_L, T_R, 3, contiguity, dispDir>(
						imgL,
						imgR,
						h_radius,
						v_radius,
						searchWindow,
						preNormalize)
					);
		}

	} else {
		Multidim::Array<T_L,2> imgL(static_cast<T_L*>(img_l.mutable_data(0)),
									{static_cast<int>(l_shape[0]),
									 static_cast<int>(l_shape[1])},
									{static_cast<int>(l_strides[0]/sizeof (T_L)),
									 static_cast<int>(l_strides[1]/sizeof (T_L))});
		Multidim::Array<T_R,2> imgR(static_cast<T_R*>(img_r.mutable_data(0)),
									{static_cast<int>(r_shape[0]),
									 static_cast<int>(r_shape[1])},
									{static_cast<int>(r_strides[0]/sizeof (T_R)),
									 static_cast<int>(r_strides[1]/sizeof (T_R))});

		if (useSymmetricBarycentricCoordinates) {
			return Multidim2NumpyArray(
						Correlation::refinedBarycentricSymmetric2dDisp<matchingFunc, T_L, T_R, 2, contiguity, dispDir>(
						imgL,
						imgR,
						h_radius,
						v_radius,
						searchWindow,
						preNormalize)
					);
		} else {
			return Multidim2NumpyArray(
						Correlation::refinedBarycentric2dDisp<matchingFunc, T_L, T_R, 2, contiguity, dispDir>(
						imgL,
						imgR,
						h_radius,
						v_radius,
						searchWindow,
						preNormalize)
					);
		}
	}
}

template <StereoVision::Correlation::matchingFunctions matchingFunc,
		  typename T_L,
		  typename T_R,
		  StereoVision::Contiguity::bidimensionalContiguity contiguity = StereoVision::Contiguity::Queen>
inline pybind11::array_t<float> callRefinedBarycentricDisp2d(pybind11::array img_l,
															  pybind11::array img_r,
															  Multidim::Array<int, 2> const& compressor,
															  Correlation::searchOffset<2> const& searchWindow,
															  bool preNormalize,
															  bool useSymmetricBarycentricCoordinates)
{

	constexpr StereoVision::Correlation::dispDirection dispDir = StereoVision::Correlation::dispDirection::RightToLeft;

	std::vector<long> l_shape = img_l.request().shape;
	std::vector<long> r_shape = img_r.request().shape;

	std::vector<long> l_strides = img_l.request().strides;
	std::vector<long> r_strides = img_r.request().strides;

	if (l_shape != r_shape) {
		throw std::invalid_argument("computeCostVolume needs arrays of the same shape !");
	}

	if (l_shape.size() != 2 and l_shape.size() != 3) {
		throw std::invalid_argument("computeCostVolume needs arrays with ndims = 2 or 3 !");
	}

	Correlation::UnFoldCompressor cmpr(compressor);

	if (l_shape.size() == 3) {

		Multidim::Array<T_L,3> imgL(static_cast<T_L*>(img_l.mutable_data(0)),
									{static_cast<int>(l_shape[0]),
									 static_cast<int>(l_shape[1]),
									 static_cast<int>(l_shape[2])},
									{static_cast<int>(l_strides[0]/sizeof (T_L)),
									 static_cast<int>(l_strides[1]/sizeof (T_L)),
									 static_cast<int>(l_strides[2]/sizeof (T_L))});

		Multidim::Array<T_R,3> imgR(static_cast<T_R*>(img_r.mutable_data(0)),
									{static_cast<int>(r_shape[0]),
									 static_cast<int>(r_shape[1]),
									 static_cast<int>(r_shape[2])},
									{static_cast<int>(r_strides[0]/sizeof (T_R)),
									 static_cast<int>(r_strides[1]/sizeof (T_R)),
									 static_cast<int>(r_strides[2]/sizeof (T_R))});

		if (useSymmetricBarycentricCoordinates) {
			return Multidim2NumpyArray(
						Correlation::refinedBarycentricSymmetric2dDisp<matchingFunc, T_L, T_R, 3, contiguity, dispDir>(
						imgL,
						imgR,
						cmpr,
						searchWindow,
						preNormalize)
					);
		} else {
			return Multidim2NumpyArray(
						Correlation::refinedBarycentric2dDisp<matchingFunc, T_L, T_R, 3, contiguity, dispDir>(
						imgL,
						imgR,
						cmpr,
						searchWindow,
						preNormalize)
					);
		}

	} else {
		Multidim::Array<T_L,2> imgL(static_cast<T_L*>(img_l.mutable_data(0)),
									{static_cast<int>(l_shape[0]),
									 static_cast<int>(l_shape[1])},
									{static_cast<int>(l_strides[0]/sizeof (T_L)),
									 static_cast<int>(l_strides[1]/sizeof (T_L))});
		Multidim::Array<T_R,2> imgR(static_cast<T_R*>(img_r.mutable_data(0)),
									{static_cast<int>(r_shape[0]),
									 static_cast<int>(r_shape[1])},
									{static_cast<int>(r_strides[0]/sizeof (T_R)),
									 static_cast<int>(r_strides[1]/sizeof (T_R))});

		if (useSymmetricBarycentricCoordinates) {
			return Multidim2NumpyArray(
						Correlation::refinedBarycentricSymmetric2dDisp<matchingFunc, T_L, T_R, 2, contiguity, dispDir>(
						imgL,
						imgR,
						cmpr,
						searchWindow,
						preNormalize)
					);
		} else {
			return Multidim2NumpyArray(
						Correlation::refinedBarycentric2dDisp<matchingFunc, T_L, T_R, 2, contiguity, dispDir>(
						imgL,
						imgR,
						cmpr,
						searchWindow,
						preNormalize)
					);
		}
	}
}

template <StereoVision::Correlation::matchingFunctions matchingFunc,
		  Correlation::dispDirection dispDir,
		  typename T_L, typename T_R>
inline pybind11::array_t<float> callRefinedBarycentricSymmetricDisp(pybind11::array img_l,
																	 pybind11::array img_r,
																	 uint8_t h_radius,
																	 uint8_t v_radius,
																	 uint32_t width,
																	 bool preNormalize)
{

	std::vector<long> l_shape = img_l.request().shape;
	std::vector<long> r_shape = img_r.request().shape;

	std::vector<long> l_strides = img_l.request().strides;
	std::vector<long> r_strides = img_r.request().strides;

	if (l_shape != r_shape) {
		throw std::invalid_argument("computeCostVolume needs arrays of the same shape !");
	}

	if (l_shape.size() != 2 and l_shape.size() != 3) {
		throw std::invalid_argument("computeCostVolume needs arrays with ndims = 2 or 3 !");
	}

	if (l_shape.size() == 3) {
		return Multidim2NumpyArray(
					StereoVision::Correlation::refinedBarycentricSymmetricDisp<matchingFunc, T_L, T_R, 3, dispDir, 1>(
					Multidim::Array<T_L,3>(static_cast<T_L*>(img_l.mutable_data(0)),
											{static_cast<int>(l_shape[0]),
											static_cast<int>(l_shape[1]),
											static_cast<int>(l_shape[2])},
											{static_cast<int>(l_strides[0]/sizeof (T_L)),
											static_cast<int>(l_strides[1]/sizeof (T_L)),
											static_cast<int>(l_strides[2]/sizeof (T_L))}),
					Multidim::Array<T_R,3>(static_cast<T_R*>(img_r.mutable_data(0)),
											{static_cast<int>(r_shape[0]),
											static_cast<int>(r_shape[1]),
											static_cast<int>(r_shape[2])},
											{static_cast<int>(r_strides[0]/sizeof (T_R)),
											static_cast<int>(r_strides[1]/sizeof (T_R)),
											static_cast<int>(r_strides[2]/sizeof (T_R))}),
					h_radius,
					v_radius,
					width,
					preNormalize)
				);
	} else {
		return Multidim2NumpyArray(
					StereoVision::Correlation::refinedBarycentricSymmetricDisp<matchingFunc, T_L, T_R, 2, dispDir, 1>(
					Multidim::Array<T_L,2>(static_cast<T_L*>(img_l.mutable_data(0)),
											{static_cast<int>(l_shape[0]), static_cast<int>(l_shape[1])},
											{static_cast<int>(l_strides[0]/sizeof (T_L)), static_cast<int>(l_strides[1]/sizeof (T_L))}),
					Multidim::Array<T_R,2>(static_cast<T_R*>(img_r.mutable_data(0)),
											{static_cast<int>(r_shape[0]), static_cast<int>(r_shape[1])},
											{static_cast<int>(r_strides[0]/sizeof (T_R)), static_cast<int>(r_strides[1]/sizeof (T_R))}),
					h_radius,
					v_radius,
					width,
					preNormalize)
				);
	}
}

template <StereoVision::Correlation::matchingFunctions matchingFunc,
		  Correlation::dispDirection dispDir,
		  typename T_L, typename T_R>
inline pybind11::array_t<float> callRefinedBarycentricSymmetricDisp(pybind11::array img_l,
																	 pybind11::array img_r,
																	 Multidim::Array<int, 2> const& compressor,
																	 uint32_t width,
																	 bool preNormalize)
{

	std::vector<long> l_shape = img_l.request().shape;
	std::vector<long> r_shape = img_r.request().shape;

	std::vector<long> l_strides = img_l.request().strides;
	std::vector<long> r_strides = img_r.request().strides;

	if (l_shape != r_shape) {
		throw std::invalid_argument("computeCostVolume needs arrays of the same shape !");
	}

	if (l_shape.size() != 2 and l_shape.size() != 3) {
		throw std::invalid_argument("computeCostVolume needs arrays with ndims = 2 or 3 !");
	}

	StereoVision::Correlation::UnFoldCompressor cmpr(compressor);

	if (l_shape.size() == 3) {
			return Multidim2NumpyArray(
						StereoVision::Correlation::refinedBarycentricSymmetricDisp<matchingFunc, T_L, T_R, 3, dispDir,1>(
						Multidim::Array<T_L,3>(static_cast<T_L*>(img_l.mutable_data(0)),
											   {static_cast<int>(l_shape[0]),
												static_cast<int>(l_shape[1]),
												static_cast<int>(l_shape[2])},
											   {static_cast<int>(l_strides[0]/sizeof (T_L)),
												static_cast<int>(l_strides[1]/sizeof (T_L)),
												static_cast<int>(l_strides[2]/sizeof (T_L))}),
						Multidim::Array<T_R,3>(static_cast<T_R*>(img_r.mutable_data(0)),
											   {static_cast<int>(r_shape[0]),
												static_cast<int>(r_shape[1]),
												static_cast<int>(r_shape[2])},
											   {static_cast<int>(r_strides[0]/sizeof (T_R)),
												static_cast<int>(r_strides[1]/sizeof (T_R)),
												static_cast<int>(r_strides[2]/sizeof (T_R))}),
						cmpr,
						width,
						preNormalize)
					);

	} else {
			return Multidim2NumpyArray(
						StereoVision::Correlation::refinedBarycentricSymmetricDisp<matchingFunc, T_L, T_R, 2, dispDir,1>(
						Multidim::Array<T_L,2>(static_cast<T_L*>(img_l.mutable_data(0)),
											   {static_cast<int>(l_shape[0]), static_cast<int>(l_shape[1])},
											   {static_cast<int>(l_strides[0]/sizeof (T_L)), static_cast<int>(l_strides[1]/sizeof (T_L))}),
						Multidim::Array<T_R,2>(static_cast<T_R*>(img_r.mutable_data(0)),
											   {static_cast<int>(r_shape[0]), static_cast<int>(r_shape[1])},
											   {static_cast<int>(r_strides[0]/sizeof (T_R)), static_cast<int>(r_strides[1]/sizeof (T_R))}),
						cmpr,
						width,
						preNormalize)
					);
	}
}

template <StereoVision::Correlation::matchingFunctions matchingFunc,
		  Correlation::dispDirection dispDir,
		  typename T_L, typename T_R>
inline pybind11::array_t<float> callRefinedCostSymmetricDisp(pybind11::array img_l,
															  pybind11::array img_r,
															  uint8_t h_radius,
															  uint8_t v_radius,
															  uint32_t width,
															  bool preNormalize)
{

	std::vector<long> l_shape = img_l.request().shape;
	std::vector<long> r_shape = img_r.request().shape;

	std::vector<long> l_strides = img_l.request().strides;
	std::vector<long> r_strides = img_r.request().strides;

	if (l_shape != r_shape) {
		throw std::invalid_argument("computeCostVolume needs arrays of the same shape !");
	}

	if (l_shape.size() != 2 and l_shape.size() != 3) {
		throw std::invalid_argument("computeCostVolume needs arrays with ndims = 2 or 3 !");
	}

	if (l_shape.size() == 3) {
		return Multidim2NumpyArray(
					StereoVision::Correlation::refinedCostSymmetricDisp<matchingFunc, T_L, T_R, 3, dispDir>(
					Multidim::Array<T_L,3>(static_cast<T_L*>(img_l.mutable_data(0)),
											   {static_cast<int>(l_shape[0]),
												static_cast<int>(l_shape[1]),
												static_cast<int>(l_shape[2])},
											   {static_cast<int>(l_strides[0]/sizeof (T_L)),
												static_cast<int>(l_strides[1]/sizeof (T_L)),
												static_cast<int>(l_strides[2]/sizeof (T_L))}),
					Multidim::Array<T_R,3>(static_cast<T_R*>(img_r.mutable_data(0)),
											   {static_cast<int>(r_shape[0]),
												static_cast<int>(r_shape[1]),
												static_cast<int>(r_shape[2])},
											   {static_cast<int>(r_strides[0]/sizeof (T_R)),
												static_cast<int>(r_strides[1]/sizeof (T_R)),
												static_cast<int>(r_strides[2]/sizeof (T_R))}),
					h_radius,
					v_radius,
					width,
					preNormalize)
				);
	} else {
		return Multidim2NumpyArray(
					StereoVision::Correlation::refinedCostSymmetricDisp<matchingFunc, T_L, T_R, 2, dispDir>(
					Multidim::Array<T_L,2>(static_cast<T_L*>(img_l.mutable_data(0)),
											   {static_cast<int>(l_shape[0]), static_cast<int>(l_shape[1])},
											   {static_cast<int>(l_strides[0]/sizeof (T_L)), static_cast<int>(l_strides[1]/sizeof (T_L))}),
					Multidim::Array<T_R,2>(static_cast<T_R*>(img_r.mutable_data(0)),
											   {static_cast<int>(r_shape[0]), static_cast<int>(r_shape[1])},
											   {static_cast<int>(r_strides[0]/sizeof (T_R)), static_cast<int>(r_strides[1]/sizeof (T_R))}),
					h_radius,
					v_radius,
					width,
					preNormalize)
				);
	}
}

template <StereoVision::Correlation::matchingFunctions matchingFunc,
		  Correlation::dispDirection dispDir,
		  typename T_L, typename T_R>
inline pybind11::array_t<float> callRefinedCostSymmetricDisp(pybind11::array img_l,
															  pybind11::array img_r,
															  Multidim::Array<int, 2> const& compressor,
															  uint32_t width,
															  bool preNormalize)
{

	std::vector<long> l_shape = img_l.request().shape;
	std::vector<long> r_shape = img_r.request().shape;

	std::vector<long> l_strides = img_l.request().strides;
	std::vector<long> r_strides = img_r.request().strides;

	if (l_shape != r_shape) {
		throw std::invalid_argument("computeCostVolume needs arrays of the same shape !");
	}

	if (l_shape.size() != 2 and l_shape.size() != 3) {
		throw std::invalid_argument("computeCostVolume needs arrays with ndims = 2 or 3 !");
	}

	StereoVision::Correlation::UnFoldCompressor cmpr(compressor);

	if (l_shape.size() == 3) {
		return Multidim2NumpyArray(
					StereoVision::Correlation::refinedCostSymmetricDisp<matchingFunc, T_L, T_R, 3, dispDir>(
					Multidim::Array<T_L,3>(static_cast<T_L*>(img_l.mutable_data(0)),
											   {static_cast<int>(l_shape[0]),
												static_cast<int>(l_shape[1]),
												static_cast<int>(l_shape[2])},
											   {static_cast<int>(l_strides[0]/sizeof (T_L)),
												static_cast<int>(l_strides[1]/sizeof (T_L)),
												static_cast<int>(l_strides[2]/sizeof (T_L))}),
					Multidim::Array<T_R,3>(static_cast<T_R*>(img_r.mutable_data(0)),
											   {static_cast<int>(r_shape[0]),
												static_cast<int>(r_shape[1]),
												static_cast<int>(r_shape[2])},
											   {static_cast<int>(r_strides[0]/sizeof (T_R)),
												static_cast<int>(r_strides[1]/sizeof (T_R)),
												static_cast<int>(r_strides[2]/sizeof (T_R))}),
					cmpr,
					width,
					preNormalize)
				);
	} else {
		return Multidim2NumpyArray(
					StereoVision::Correlation::refinedCostSymmetricDisp<matchingFunc, T_L, T_R, 2, dispDir>(
					Multidim::Array<T_L,2>(static_cast<T_L*>(img_l.mutable_data(0)),
											   {static_cast<int>(l_shape[0]), static_cast<int>(l_shape[1])},
											   {static_cast<int>(l_strides[0]/sizeof (T_L)), static_cast<int>(l_strides[1]/sizeof (T_L))}),
					Multidim::Array<T_R,2>(static_cast<T_R*>(img_r.mutable_data(0)),
											   {static_cast<int>(r_shape[0]), static_cast<int>(r_shape[1])},
											   {static_cast<int>(r_strides[0]/sizeof (T_R)), static_cast<int>(r_strides[1]/sizeof (T_R))}),
					cmpr,
					width,
					preNormalize)
				);
	}
}

template <StereoVision::Correlation::matchingFunctions matchingFunc,
		  StereoVision::Correlation::dispDirection dispDir>
pybind11::array_t<disp_t> callTypedDisparity(pybind11::array img_l,
											  ArraysFormat l_format,
											  pybind11::array img_r,
											  ArraysFormat r_format,
											  uint32_t width,
											 int32_t offset,
											  uint8_t v_radius,
											  uint8_t h_radius) {

	if (l_format == T_FLOAT) {

		if (r_format == T_FLOAT) {

			return callDisp<matchingFunc, dispDir, float, float>(img_l, img_r, h_radius, v_radius, width, offset);

		} else if (r_format == T_UINT8) {

			return callDisp<matchingFunc, dispDir, float, uint8_t>(img_l, img_r, h_radius, v_radius, width, offset);

		}

	} else if (l_format == T_UINT8) {

		if (r_format == T_FLOAT) {

			return callDisp<matchingFunc, dispDir, uint8_t, float>(img_l, img_r, h_radius, v_radius, width, offset);

		} else if (r_format == T_UINT8) {

			return callDisp<matchingFunc, dispDir, uint8_t, uint8_t>(img_l, img_r, h_radius, v_radius, width, offset);

		}

	}

	return pybind11::array_t<disp_t>();
}

template <StereoVision::Correlation::dispDirection dispDir>
pybind11::array_t<disp_t> callDirectedDisparity(pybind11::array img_l,
												ArraysFormat l_format,
												pybind11::array img_r,
												ArraysFormat r_format,
												std::string const& method,
												uint32_t width,
												int32_t offset,
												uint8_t v_radius,
												uint8_t h_radius) {


	if (method == "ncc") {
		return callTypedDisparity<StereoVision::Correlation::matchingFunctions::NCC,
				dispDir>(
					img_l,
					l_format,
					img_r,
					r_format,
					width,
					offset,
					v_radius,
					h_radius);

	} else if (method == "zncc") {
		return callTypedDisparity<StereoVision::Correlation::matchingFunctions::ZNCC,
				dispDir>(
					img_l,
					l_format,
					img_r,
					r_format,
					width,
					offset,
					v_radius,
					h_radius);
	} else if (method == "sad") {

		return callTypedDisparity<StereoVision::Correlation::matchingFunctions::SAD,
				dispDir>(
					img_l,
					l_format,
					img_r,
					r_format,
					width,
					offset,
					v_radius,
					h_radius);

	} else if (method == "zsad") {

		return callTypedDisparity<StereoVision::Correlation::matchingFunctions::ZSAD,
				dispDir>(
					img_l,
					l_format,
					img_r,
					r_format,
					width,
					offset,
					v_radius,
					h_radius);

	} else if (method == "ssd") {

		return callTypedDisparity<StereoVision::Correlation::matchingFunctions::SSD,
				dispDir>(
					img_l,
					l_format,
					img_r,
					r_format,
					width,
					offset,
					v_radius,
					h_radius);

	} else if (method == "zssd") {

		return callTypedDisparity<StereoVision::Correlation::matchingFunctions::ZSSD,
				dispDir>(
					img_l,
					l_format,
					img_r,
					r_format,
					width,
					offset,
					v_radius,
					h_radius);

	} else if (method == "meds") {

		return callTypedDisparity<StereoVision::Correlation::matchingFunctions::MEDAD,
				dispDir>(
					img_l,
					l_format,
					img_r,
					r_format,
					width,
					offset,
					v_radius,
					h_radius);

	} else if (method == "zmeds") {

		return callTypedDisparity<StereoVision::Correlation::matchingFunctions::ZMEDAD,
				dispDir>(
					img_l,
					l_format,
					img_r,
					r_format,
					width,
					offset,
					v_radius,
					h_radius);

	} else if (method == "census") {

		if (l_format == T_FLOAT) {

			if (r_format == T_FLOAT) {

				return callCensusDisp<float, float, dispDir>(img_l, img_r, h_radius, v_radius, width, true);

			} else if (r_format == T_UINT8) {

				return callCensusDisp<float, uint8_t, dispDir>(img_l, img_r, h_radius, v_radius, width, true);

			}

		} else if (l_format == T_UINT8) {

			if (r_format == T_FLOAT) {

				return callCensusDisp<uint8_t, float, dispDir>(img_l, img_r, h_radius, v_radius, width, true);

			} else if (r_format == T_UINT8) {

				return callCensusDisp<uint8_t, uint8_t, dispDir>(img_l, img_r, h_radius, v_radius, width, true);

			}

		}

	}

	return pybind11::array_t<disp_t>();
}

pybind11::array_t<disp_t> computeDisparity(pybind11::array img_l,
										   pybind11::array img_r,
										   uint32_t width,
										   int32_t offset,
										   uint8_t v_radius,
										   uint8_t h_radius,
										   std::string const& method,
										   std::string const& direction) {

	if (width > 1000) {
		std::stringstream ss;
		ss << "Maximal allowed width is " << 1000 << " !";
		throw std::invalid_argument(ss.str());
	}

	auto toLower = [](unsigned char c){ return std::tolower(c); };

	std::string lowercaseMethod = method;
	std::transform(lowercaseMethod.begin(), lowercaseMethod.end(), lowercaseMethod.begin(), toLower);

	std::string lowercaseDirection = direction;
	std::transform(lowercaseDirection.begin(), lowercaseDirection.end(), lowercaseDirection.begin(), toLower);

	if (lowercaseMethod != "ncc" and
		lowercaseMethod != "zncc" and
		lowercaseMethod != "sad" and
		lowercaseMethod != "zsad" and
		lowercaseMethod != "ssd" and
		lowercaseMethod != "zssd" and
		lowercaseMethod != "meds" and
		lowercaseMethod != "zmeds" and
		lowercaseMethod != "census") {

		std::stringstream ss;
		ss << "Unsupported method " << method << " !";
		throw std::invalid_argument(ss.str());

	}

	if (lowercaseDirection != "l2r" and
		lowercaseDirection != "r2l") {

		std::stringstream ss;
		ss << "Unsupported direction " << direction << " !";
		throw std::invalid_argument(ss.str());

	}

	std::vector<long> l_shape = img_l.request().shape;
	std::vector<long> r_shape = img_r.request().shape;

	if (l_shape != r_shape) {
		throw std::invalid_argument("computeCostVolume needs arrays of the same shape !");
	}

	if (l_shape.size() != 2 and l_shape.size() != 3) {
		throw std::invalid_argument("computeCostVolume needs arrays with ndims = 2 or 3 !");
	}

	ArraysFormat l_format = formatFromName(img_l.request().format);
	ArraysFormat r_format = formatFromName(img_r.request().format);

	if (l_format == T_INVALID || r_format == T_INVALID) {
		throw std::invalid_argument("computeDisparity uses arrays of either float or uint8 !");
	}

	if (lowercaseDirection == "l2r") {
		return callDirectedDisparity<StereoVision::Correlation::dispDirection::LeftToRight>(
					img_l,
					l_format,
					img_r,
					r_format,
					lowercaseMethod,
					width,
					offset,
					v_radius,
					h_radius
					);
	}

	if (lowercaseDirection == "r2l") {
		return callDirectedDisparity<StereoVision::Correlation::dispDirection::RightToLeft>(
					img_l,
					l_format,
					img_r,
					r_format,
					lowercaseMethod,
					width,
					offset,
					v_radius,
					h_radius
					);
	}

	return pybind11::array_t<disp_t>();

}

template <StereoVision::Correlation::matchingFunctions matchingFunc,
		  StereoVision::Correlation::dispDirection dispDir>
pybind11::array_t<disp_t> callTypedDisparity(pybind11::array img_l,
											  ArraysFormat l_format,
											  pybind11::array img_r,
											  ArraysFormat r_format,
											  uint32_t width,
											 int32_t offset,
											  Multidim::Array<int,2> const& compressor_mask) {

	if (l_format == T_FLOAT) {

		if (r_format == T_FLOAT) {

			return callDisp<matchingFunc, dispDir, float, float>(img_l, img_r, compressor_mask, width, offset);

		} else if (r_format == T_UINT8) {

			return callDisp<matchingFunc, dispDir, float, uint8_t>(img_l, img_r, compressor_mask, width, offset);

		}

	} else if (l_format == T_UINT8) {

		if (r_format == T_FLOAT) {

			return callDisp<matchingFunc, dispDir, uint8_t, float>(img_l, img_r, compressor_mask, width, offset);

		} else if (r_format == T_UINT8) {

			return callDisp<matchingFunc, dispDir, uint8_t, uint8_t>(img_l, img_r, compressor_mask, width, offset);

		}

	}

	return pybind11::array_t<disp_t>();
}



pybind11::tuple computeDisparityHierarchicalConstantRadiuses(pybind11::array img_l,
															 pybind11::array img_r,
															 uint8_t depth,
															 uint32_t width,
															 uint8_t v_radius,
															 uint8_t h_radius,
															 disp_t truncated_cost_volume_radius,
															 std::string const& method,
															 std::string const& direction) {

	pybind11::array_t<uint8_t> radiuses(std::vector<long>({depth+1, 2}));

	for (int i = 0; i <= depth; i++) {
		radiuses.mutable_at(i,0) = v_radius;
		radiuses.mutable_at(i,1) = h_radius;
	}

	return computeDisparityHierarchical(img_l, img_r, width, radiuses, truncated_cost_volume_radius, method, direction);
}

template<StereoVision::Correlation::dispDirection dispDir>
pybind11::tuple callDirectedHierarchicalDisp(pybind11::array & img_l,
											 ArraysFormat l_format,
											 pybind11::array & img_r,
											 ArraysFormat r_format,
											 std::string const& method,
											 uint32_t width,
											 std::vector<uint8_t> const& v_radiuses,
											 std::vector<uint8_t> const& h_radiuses,
											 disp_t truncated_cost_volume_radius) {


	if (method == "ncc") {
		return callTypedHierarchicalDisparity<StereoVision::Correlation::matchingFunctions::NCC, dispDir>(
					img_l,
					l_format,
					img_r,
					r_format,
					width,
					v_radiuses,
					h_radiuses,
					truncated_cost_volume_radius);
	} else if (method == "zncc") {
		return callTypedHierarchicalDisparity<StereoVision::Correlation::matchingFunctions::ZNCC, dispDir>(
					img_l,
					l_format,
					img_r,
					r_format,
					width,
					v_radiuses,
					h_radiuses,
					truncated_cost_volume_radius);
	} else if (method == "sad") {

		return callTypedHierarchicalDisparity<StereoVision::Correlation::matchingFunctions::SAD, dispDir>(
					img_l,
					l_format,
					img_r,
					r_format,
					width,
					v_radiuses,
					h_radiuses,
					truncated_cost_volume_radius);

	} else if (method == "zsad") {

		return callTypedHierarchicalDisparity<StereoVision::Correlation::matchingFunctions::ZSAD, dispDir>(
					img_l,
					l_format,
					img_r,
					r_format,
					width,
					v_radiuses,
					h_radiuses,
					truncated_cost_volume_radius);

	} else if (method == "ssd") {

		return callTypedHierarchicalDisparity<StereoVision::Correlation::matchingFunctions::SSD, dispDir>(
					img_l,
					l_format,
					img_r,
					r_format,
					width,
					v_radiuses,
					h_radiuses,
					truncated_cost_volume_radius);

	} else if (method == "zssd") {

		return callTypedHierarchicalDisparity<StereoVision::Correlation::matchingFunctions::ZSSD, dispDir>(
					img_l,
					l_format,
					img_r,
					r_format,
					width,
					v_radiuses,
					h_radiuses,
					truncated_cost_volume_radius);

	} else if (method == "meds") {

		return callTypedHierarchicalDisparity<StereoVision::Correlation::matchingFunctions::MEDAD, dispDir>(
					img_l,
					l_format,
					img_r,
					r_format,
					width,
					v_radiuses,
					h_radiuses,
					truncated_cost_volume_radius);

	} else if (method == "zmeds") {

		return callTypedHierarchicalDisparity<StereoVision::Correlation::matchingFunctions::ZMEDAD, dispDir>(
					img_l,
					l_format,
					img_r,
					r_format,
					width,
					v_radiuses,
					h_radiuses,
					truncated_cost_volume_radius);
	}

	return py::make_tuple();
}


pybind11::tuple computeDisparityHierarchical(pybind11::array img_l,
											 pybind11::array img_r,
											 uint32_t width,
											 pybind11::array_t<uint8_t> radiuses,
											 disp_t truncated_cost_volume_radius,
											 std::string const& method,
											 std::string const& direction) {

	if (width > 1000) {
		std::stringstream ss;
		ss << "Maximal allowed width is " << 1000 << " !";
		throw std::invalid_argument(ss.str());
	}

	auto toLower = [](unsigned char c){ return std::tolower(c); };

	std::string lowercaseMethod = method;
	std::transform(lowercaseMethod.begin(), lowercaseMethod.end(), lowercaseMethod.begin(), toLower);

	std::string lowercaseDirection = direction;
	std::transform(lowercaseDirection.begin(), lowercaseDirection.end(), lowercaseDirection.begin(), toLower);

	if (lowercaseMethod != "ncc" and
		lowercaseMethod != "zncc" and
		lowercaseMethod != "sad" and
		lowercaseMethod != "zsad" and
		lowercaseMethod != "ssd" and
		lowercaseMethod != "zssd" and
		lowercaseMethod != "meds" and
		lowercaseMethod != "zmeds" and
		lowercaseMethod != "census") {

		std::stringstream ss;
		ss << "Unsupported method " << method << " !";
		throw std::invalid_argument(ss.str());

	}

	if (lowercaseDirection != "r2l" and
		lowercaseDirection != "l2r") {

		std::stringstream ss;
		ss << "Unsupported direction " << direction << " !";
		throw std::invalid_argument(ss.str());

	}

	auto rShapes = radiuses.shape();

	if (rShapes[1] != 2 or rShapes[0] < 1) {

		std::stringstream ss;
		ss << "Non conformant radiuses provided. The function expect an array of shape [depth x 2], got [" << rShapes[0] << " x " << rShapes[1] << "] !";
		throw std::invalid_argument(ss.str());
	}

	int depth = rShapes[0]-1;
	if (depth > MAX_HIERARCHICAL_DISP_DEPTH) {
		std::stringstream ss;
		ss << "The maximal hierarchy depth is " << MAX_HIERARCHICAL_DISP_DEPTH << ", got " << depth << " !";
		throw std::invalid_argument(ss.str());
	}

	std::vector<long> l_shape = img_l.request().shape;
	std::vector<long> r_shape = img_r.request().shape;

	if (l_shape != r_shape) {
		throw std::invalid_argument("computeDisaprityHierarchical needs images of the same shape !");
	}

	if (l_shape.size() != 2 and l_shape.size() != 3) {
		throw std::invalid_argument("computeDisaprityHierarchical needs images with ndims = 2 or 3 !");
	}

	ArraysFormat l_format = formatFromName(img_l.request().format);
	ArraysFormat r_format = formatFromName(img_r.request().format);

	if (l_format == T_INVALID || r_format == T_INVALID) {
		throw std::invalid_argument("computeDisparity uses arrays of either float or uint8 !");
	}

	std::vector<uint8_t> v_radiuses(rShapes[0]);
	std::vector<uint8_t> h_radiuses(rShapes[0]);

	for (int i = 0; i < rShapes[0]; i++) {
		v_radiuses[i] = radiuses.at(i,0);
		h_radiuses[i] = radiuses.at(i,1);
	}

	if (lowercaseDirection == "r2l") {
		return callDirectedHierarchicalDisp<Correlation::dispDirection::RightToLeft>(
					img_l,
					l_format,
					img_r,
					r_format,
					lowercaseMethod,
					width,
					v_radiuses,
					h_radiuses,
					truncated_cost_volume_radius
					);
	}

	if (lowercaseDirection == "l2r") {
		return callDirectedHierarchicalDisp<Correlation::dispDirection::LeftToRight>(
					img_l,
					l_format,
					img_r,
					r_format,
					lowercaseMethod,
					width,
					v_radiuses,
					h_radiuses,
					truncated_cost_volume_radius
					);
	}

	return py::make_tuple();
}

template <StereoVision::Correlation::dispDirection dispDir>
pybind11::array_t<disp_t> callDirectedDisparity(pybind11::array img_l,
												ArraysFormat l_format,
												pybind11::array img_r,
												ArraysFormat r_format,
												std::string const& method,
												uint32_t width,
												int32_t offset,
												Multidim::Array<int,2> const& compressor_mask) {

	if (method == "ncc") {
		return callTypedDisparity<StereoVision::Correlation::matchingFunctions::NCC, dispDir>(
					img_l,
					l_format,
					img_r,
					r_format,
					width,
					offset,
					compressor_mask);
	} else if (method == "zncc") {
		return callTypedDisparity<StereoVision::Correlation::matchingFunctions::ZNCC, dispDir>(
					img_l,
					l_format,
					img_r,
					r_format,
					width,
					offset,
					compressor_mask);
	} else if (method == "sad") {

		return callTypedDisparity<StereoVision::Correlation::matchingFunctions::SAD, dispDir>(
					img_l,
					l_format,
					img_r,
					r_format,
					width,
					offset,
					compressor_mask);

	} else if (method == "zsad") {

		return callTypedDisparity<StereoVision::Correlation::matchingFunctions::ZSAD, dispDir>(
					img_l,
					l_format,
					img_r,
					r_format,
					width,
					offset,
					compressor_mask);

	} else if (method == "ssd") {

		return callTypedDisparity<StereoVision::Correlation::matchingFunctions::SSD, dispDir>(
					img_l,
					l_format,
					img_r,
					r_format,
					width,
					offset,
					compressor_mask);

	} else if (method == "zssd") {

		return callTypedDisparity<StereoVision::Correlation::matchingFunctions::ZSSD, dispDir>(
					img_l,
					l_format,
					img_r,
					r_format,
					width,
					offset,
					compressor_mask);

	} else if (method == "meds") {

		return callTypedDisparity<StereoVision::Correlation::matchingFunctions::MEDAD, dispDir>(
					img_l,
					l_format,
					img_r,
					r_format,
					width,
					offset,
					compressor_mask);

	} else if (method == "zmeds") {

		return callTypedDisparity<StereoVision::Correlation::matchingFunctions::ZMEDAD, dispDir>(
					img_l,
					l_format,
					img_r,
					r_format,
					width,
					offset,
					compressor_mask);

	}

	return pybind11::array_t<disp_t>();

}

pybind11::array_t<disp_t> computeDisparityWithCompressor(pybind11::array img_l,
														  pybind11::array img_r,
														  pybind11::array_t<int> compressor,
														  uint32_t width,
														 int32_t offset,
														 std::string const& method,
														 std::string const& direction) {

	if (width > 1000) {
		std::stringstream ss;
		ss << "Maximal allowed width is " << 1000 << " !";
		throw std::invalid_argument(ss.str());
	}

	auto toLower = [](unsigned char c){ return std::tolower(c); };

	std::string lowercaseMethod = method;
	std::transform(lowercaseMethod.begin(), lowercaseMethod.end(), lowercaseMethod.begin(), toLower);

	std::string lowercaseDirection = direction;
	std::transform(lowercaseDirection.begin(), lowercaseDirection.end(), lowercaseDirection.begin(), toLower);

	if (lowercaseMethod != "ncc" and
		lowercaseMethod != "sad" and
		lowercaseMethod != "ssd" and
		lowercaseMethod != "meds" and
		lowercaseMethod != "zncc" and
		lowercaseMethod != "zsad" and
		lowercaseMethod != "zssd" and
		lowercaseMethod != "zmeds") {

		std::stringstream ss;
		ss << "Unsupported method " << method << " !";
		throw std::invalid_argument(ss.str());

	}

	if (lowercaseDirection != "l2r" and
		lowercaseDirection != "r2l") {

		std::stringstream ss;
		ss << "Unsupported direction " << direction << " !";
		throw std::invalid_argument(ss.str());

	}

	std::vector<long> l_shape = img_l.request().shape;
	std::vector<long> r_shape = img_r.request().shape;

	if (l_shape != r_shape) {
		throw std::invalid_argument("computeCompressedDisparity needs input images of the same shape !");
	}

	if (l_shape.size() != 2 and l_shape.size() != 3) {
		throw std::invalid_argument("computeCompressedDisparity needs input images with ndims = 2 or 3 !");
	}

	std::vector<long> compressor_shape = compressor.request().shape;

	if (compressor_shape.size() != 2) {
		throw std::invalid_argument("computeCompressedDisparity needs a compressor mask with ndims = 2 or 3 !");
	}

	Multidim::Array<int, 2>::ShapeBlock mask_shape;
	mask_shape[0] = compressor_shape[0];
	mask_shape[1] = compressor_shape[1];

	std::vector<long> compressor_strides = compressor.request().strides;
	Multidim::Array<int, 2>::ShapeBlock mask_strides;
	mask_strides[0] = compressor_strides[0]/sizeof (int);
	mask_strides[1] = compressor_strides[1]/sizeof (int);

	Multidim::Array<int,2> compressor_mask(compressor.mutable_data(), mask_shape, mask_strides);

	ArraysFormat l_format = formatFromName(img_l.request().format);
	ArraysFormat r_format = formatFromName(img_r.request().format);

	if (l_format == T_INVALID || r_format == T_INVALID) {
		throw std::invalid_argument("computeCompressedDisparity uses input images of either float or uint8 !");
	}

	if (lowercaseDirection == "l2r") {
		return callDirectedDisparity<StereoVision::Correlation::dispDirection::LeftToRight>(
					img_l,
					l_format,
					img_r,
					r_format,
					lowercaseMethod,
					width,
					offset,
					compressor_mask
					);
	}

	if (lowercaseDirection == "r2l") {
		return callDirectedDisparity<StereoVision::Correlation::dispDirection::RightToLeft>(
					img_l,
					l_format,
					img_r,
					r_format,
					lowercaseMethod,
					width,
					offset,
					compressor_mask
					);
	}

	return pybind11::array_t<disp_t>();

}

template <StereoVision::Correlation::matchingFunctions matchingFunc,
		  StereoVision::Correlation::dispDirection dispDir>
pybind11::array_t<float> callTypedCostVolume(pybind11::array img_l,
											  ArraysFormat l_format,
											  pybind11::array img_r,
											  ArraysFormat r_format,
											  uint32_t width,
											 int32_t offset,
											  uint8_t v_radius,
											  uint8_t h_radius) {

	if (l_format == T_FLOAT) {

		if (r_format == T_FLOAT) {

			return callCostVolume<matchingFunc, dispDir, float, float>(img_l, img_r, h_radius, v_radius, width, offset);

		} else if (r_format == T_UINT8) {

			return callCostVolume<matchingFunc, dispDir, float, uint8_t>(img_l, img_r, h_radius, v_radius, width, offset);

		}

	} else if (l_format == T_UINT8) {

		if (r_format == T_FLOAT) {

			return callCostVolume<matchingFunc, dispDir, uint8_t, float>(img_l, img_r, h_radius, v_radius, width, offset);

		} else if (r_format == T_UINT8) {

			return callCostVolume<matchingFunc, dispDir, uint8_t, uint8_t>(img_l, img_r, h_radius, v_radius, width, offset);

		}

	}

	return pybind11::array_t<float>();
}

template<StereoVision::Correlation::dispDirection dispDir>
pybind11::array_t<float> callDirectedCostVolume(pybind11::array img_l,
												ArraysFormat l_format,
												pybind11::array img_r,
												ArraysFormat r_format,
												std::string const& method,
												uint32_t width,
												int32_t offset,
												uint8_t v_radius,
												uint8_t h_radius) {


	if (method == "ncc") {
		return callTypedCostVolume<StereoVision::Correlation::matchingFunctions::NCC, dispDir>(
					img_l,
					l_format,
					img_r,
					r_format,
					width,
					offset,
					v_radius,
					h_radius);
	} else if (method == "zncc") {
		return callTypedCostVolume<StereoVision::Correlation::matchingFunctions::ZNCC, dispDir>(
					img_l,
					l_format,
					img_r,
					r_format,
					width,
					offset,
					v_radius,
					h_radius);
	} else if (method == "sad") {

		return callTypedCostVolume<StereoVision::Correlation::matchingFunctions::SAD, dispDir>(
					img_l,
					l_format,
					img_r,
					r_format,
					width,
					offset,
					v_radius,
					h_radius);

	} else if (method == "zsad") {

		return callTypedCostVolume<StereoVision::Correlation::matchingFunctions::ZSAD, dispDir>(
					img_l,
					l_format,
					img_r,
					r_format,
					width,
					offset,
					v_radius,
					h_radius);

	} else if (method == "ssd") {

		return callTypedCostVolume<StereoVision::Correlation::matchingFunctions::SSD, dispDir>(
					img_l,
					l_format,
					img_r,
					r_format,
					width,
					offset,
					v_radius,
					h_radius);

	} else if (method == "zssd") {

		return callTypedCostVolume<StereoVision::Correlation::matchingFunctions::ZSSD, dispDir>(
					img_l,
					l_format,
					img_r,
					r_format,
					width,
					offset,
					v_radius,
					h_radius);

	} else if (method == "meds") {

		return callTypedCostVolume<StereoVision::Correlation::matchingFunctions::MEDAD, dispDir>(
					img_l,
					l_format,
					img_r,
					r_format,
					width,
					offset,
					v_radius,
					h_radius);

	} else if (method == "zmeds") {

		return callTypedCostVolume<StereoVision::Correlation::matchingFunctions::ZMEDAD, dispDir>(
					img_l,
					l_format,
					img_r,
					r_format,
					width,
					offset,
					v_radius,
					h_radius);

	}

	return pybind11::array_t<float>();

}


pybind11::array_t<float> computeFullCostVolume(pybind11::array img_l,
											   pybind11::array img_r,
											   uint32_t width,
											   int32_t offset,
											   uint8_t v_radius,
											   uint8_t h_radius,
											   std::string const& method,
											   std::string const& direction) {

	if (width > 1000) {
		std::stringstream ss;
		ss << "Maximal allowed width is " << 1000 << " !";
		throw std::invalid_argument(ss.str());
	}

	auto toLower = [](unsigned char c){ return std::tolower(c); };

	std::string lowercaseMethod = method;
	std::transform(lowercaseMethod.begin(), lowercaseMethod.end(), lowercaseMethod.begin(), toLower);

	std::string lowercaseDirection = direction;
	std::transform(lowercaseDirection.begin(), lowercaseDirection.end(), lowercaseDirection.begin(), toLower);

	if (lowercaseMethod != "ncc" and
		lowercaseMethod != "sad" and
		lowercaseMethod != "ssd" and
		lowercaseMethod != "meds" and
		lowercaseMethod != "zncc" and
		lowercaseMethod != "zsad" and
		lowercaseMethod != "zssd" and
		lowercaseMethod != "zmeds") {

		std::stringstream ss;
		ss << "Unsupported method " << method << " !";
		throw std::invalid_argument(ss.str());

	}

	if (lowercaseDirection != "l2r" and
		lowercaseDirection != "r2l") {

		std::stringstream ss;
		ss << "Unsupported direction " << direction << " !";
		throw std::invalid_argument(ss.str());

	}

	std::vector<long> l_shape = img_l.request().shape;
	std::vector<long> r_shape = img_r.request().shape;

	std::vector<long> l_strides = img_l.request().strides;
	std::vector<long> r_strides = img_r.request().strides;

	if (l_shape != r_shape) {
		throw std::invalid_argument("computeFullCostVolume needs arrays of the same shape !");
	}

	if (l_shape.size() != 2 and l_shape.size() != 3) {
		throw std::invalid_argument("computeFullCostVolume needs arrays with ndims = 2 or 3 !");
	}

	ArraysFormat l_format = formatFromName(img_l.request().format);
	ArraysFormat r_format = formatFromName(img_r.request().format);

	if (l_format == T_INVALID || r_format == T_INVALID) {
		throw std::invalid_argument("computeFullCostVolume uses arrays of either float or uint8 !");
	}

	if (lowercaseDirection == "l2r") {
		return callDirectedCostVolume<StereoVision::Correlation::dispDirection::LeftToRight> (
					img_l,
					l_format,
					img_r,
					r_format,
					lowercaseMethod,
					width,
					offset,
					v_radius,
					h_radius
					);
	}

	if (lowercaseDirection == "r2l") {
		return callDirectedCostVolume<StereoVision::Correlation::dispDirection::RightToLeft> (
					img_l,
					l_format,
					img_r,
					r_format,
					lowercaseMethod,
					width,
					offset,
					v_radius,
					h_radius
					);
	}

	return pybind11::array_t<float>();
}

template <StereoVision::Correlation::matchingFunctions matchingFunc,
		  StereoVision::Correlation::dispDirection dispDir>
pybind11::array_t<float> callTypedCostVolume(pybind11::array img_l,
											  ArraysFormat l_format,
											  pybind11::array img_r,
											  ArraysFormat r_format,
											  uint32_t width,
											 int32_t offset,
											  Multidim::Array<int,2> const& compressor_mask) {

	if (l_format == T_FLOAT) {

		if (r_format == T_FLOAT) {

			return callCostVolume<matchingFunc, dispDir, float, float>(img_l, img_r, compressor_mask, width, offset);

		} else if (r_format == T_UINT8) {

			return callCostVolume<matchingFunc, dispDir, float, uint8_t>(img_l, img_r, compressor_mask, width, offset);

		}

	} else if (l_format == T_UINT8) {

		if (r_format == T_FLOAT) {

			return callCostVolume<matchingFunc, dispDir, uint8_t, float>(img_l, img_r, compressor_mask, width, offset);

		} else if (r_format == T_UINT8) {

			return callCostVolume<matchingFunc, dispDir, uint8_t, uint8_t>(img_l, img_r, compressor_mask, width, offset);

		}

	}

	return pybind11::array_t<float>();
}



template<StereoVision::Correlation::dispDirection dispDir>
pybind11::array_t<float> callDirectedCostVolume(pybind11::array img_l,
												ArraysFormat l_format,
												pybind11::array img_r,
												ArraysFormat r_format,
												std::string const& method,
												uint32_t width,
												int32_t offset,
												Multidim::Array<int,2> const& compressor_mask) {


	if (method == "ncc") {
		return callTypedCostVolume<StereoVision::Correlation::matchingFunctions::NCC, dispDir>(
					img_l,
					l_format,
					img_r,
					r_format,
					width,
					offset,
					compressor_mask);
	} else if (method == "zncc") {
		return callTypedCostVolume<StereoVision::Correlation::matchingFunctions::ZNCC, dispDir>(
					img_l,
					l_format,
					img_r,
					r_format,
					width,
					offset,
					compressor_mask);
	} else if (method == "sad") {

		return callTypedCostVolume<StereoVision::Correlation::matchingFunctions::SAD, dispDir>(
					img_l,
					l_format,
					img_r,
					r_format,
					width,
					offset,
					compressor_mask);

	} else if (method == "zsad") {

		return callTypedCostVolume<StereoVision::Correlation::matchingFunctions::ZSAD, dispDir>(
					img_l,
					l_format,
					img_r,
					r_format,
					width,
					offset,
					compressor_mask);

	} else if (method == "ssd") {

		return callTypedCostVolume<StereoVision::Correlation::matchingFunctions::SSD, dispDir>(
					img_l,
					l_format,
					img_r,
					r_format,
					width,
					offset,
					compressor_mask);

	} else if (method == "zssd") {

		return callTypedCostVolume<StereoVision::Correlation::matchingFunctions::ZSSD, dispDir>(
					img_l,
					l_format,
					img_r,
					r_format,
					width,
					offset,
					compressor_mask);

	} else if (method == "meds") {

		return callTypedCostVolume<StereoVision::Correlation::matchingFunctions::MEDAD, dispDir>(
					img_l,
					l_format,
					img_r,
					r_format,
					width,
					offset,
					compressor_mask);

	} else if (method == "zmeds") {

		return callTypedCostVolume<StereoVision::Correlation::matchingFunctions::ZMEDAD, dispDir>(
					img_l,
					l_format,
					img_r,
					r_format,
					width,
					offset,
					compressor_mask);

	}

	return pybind11::array_t<float>();

}

pybind11::array_t<float> computeFullCostVolumeWithCompressor(pybind11::array img_l,
															 pybind11::array img_r,
															 pybind11::array_t<int> compressor,
															 uint32_t width,
															 int32_t offset,
															 std::string const& method,
															 std::string const& direction) {
	if (width > 1000) {
		std::stringstream ss;
		ss << "Maximal allowed width is " << 1000 << " !";
		throw std::invalid_argument(ss.str());
	}

	auto toLower = [](unsigned char c){ return std::tolower(c); };

	std::string lowercaseMethod = method;
	std::transform(lowercaseMethod.begin(), lowercaseMethod.end(), lowercaseMethod.begin(), toLower);

	std::string lowercaseDirection = direction;
	std::transform(lowercaseDirection.begin(), lowercaseDirection.end(), lowercaseDirection.begin(), toLower);

	if (lowercaseMethod != "ncc" and
		lowercaseMethod != "sad" and
		lowercaseMethod != "ssd" and
		lowercaseMethod != "meds" and
		lowercaseMethod != "zncc" and
		lowercaseMethod != "zsad" and
		lowercaseMethod != "zssd" and
		lowercaseMethod != "zmeds") {

		std::stringstream ss;
		ss << "Unsupported method " << method << " !";
		throw std::invalid_argument(ss.str());

	}

	std::vector<long> l_shape = img_l.request().shape;
	std::vector<long> r_shape = img_r.request().shape;

	std::vector<long> l_strides = img_l.request().strides;
	std::vector<long> r_strides = img_r.request().strides;

	if (l_shape != r_shape) {
		throw std::invalid_argument("computeFullCostVolume needs arrays of the same shape !");
	}

	if (l_shape.size() != 2 and l_shape.size() != 3) {
		throw std::invalid_argument("computeFullCostVolume needs arrays with ndims = 2 or 3 !");
	}

	std::vector<long> compressor_shape = compressor.request().shape;

	if (compressor_shape.size() != 2) {
		throw std::invalid_argument("computeCompressedDisparity needs a compressor mask with ndims = 2 or 3 !");
	}

	Multidim::Array<int, 2>::ShapeBlock mask_shape;
	mask_shape[0] = compressor_shape[0];
	mask_shape[1] = compressor_shape[1];

	std::vector<long> compressor_strides = compressor.request().strides;
	Multidim::Array<int, 2>::ShapeBlock mask_strides;
	mask_strides[0] = compressor_strides[0]/sizeof (int);
	mask_strides[1] = compressor_strides[1]/sizeof (int);

	Multidim::Array<int,2> compressor_mask(compressor.mutable_data(), mask_shape, mask_strides);

	ArraysFormat l_format = formatFromName(img_l.request().format);
	ArraysFormat r_format = formatFromName(img_r.request().format);

	if (l_format == T_INVALID || r_format == T_INVALID) {
		throw std::invalid_argument("computeDisparity uses arrays of either float or uint8 !");
	}


	if (lowercaseDirection == "l2r") {
		return callDirectedCostVolume<StereoVision::Correlation::dispDirection::LeftToRight> (
					img_l,
					l_format,
					img_r,
					r_format,
					lowercaseMethod,
					width,
					offset,
					compressor_mask
					);
	}

	if (lowercaseDirection == "r2l") {
		return callDirectedCostVolume<StereoVision::Correlation::dispDirection::RightToLeft> (
					img_l,
					l_format,
					img_r,
					r_format,
					lowercaseMethod,
					width,
					offset,
					compressor_mask
					);
	}


	return pybind11::array_t<float>();
}

pybind11::array_t<float> computeFullCostVolumeWithCompressor(pybind11::array img_l,
															 pybind11::array img_r,
															 uint32_t width,
															 int32_t offset,
															 pybind11::array_t<int> compressor,
															 std::string const& method,
															 std::string const& direction) {

	return computeFullCostVolumeWithCompressor(img_l, img_r, compressor, width, offset, method, direction);

}



template<Correlation::matchingFunctions matchingFunc>
pybind11::array_t<float> callTypedCostVolume2d(pybind11::array img_l,
												ArraysFormat l_format,
												pybind11::array img_r,
												ArraysFormat r_format,
												Correlation::searchOffset<2> const& searchWindow,
												uint8_t v_radius,
												uint8_t h_radius) {


	if (l_format == T_FLOAT) {

		if (r_format == T_FLOAT) {

			return callCostVolume2d<matchingFunc, float, float>(img_l, img_r, h_radius, v_radius, searchWindow);

		} else if (r_format == T_UINT8) {

			return callCostVolume2d<matchingFunc, float, uint8_t>(img_l, img_r, h_radius, v_radius, searchWindow);

		}

	} else if (l_format == T_UINT8) {

		if (r_format == T_FLOAT) {

			return callCostVolume2d<matchingFunc, uint8_t, float>(img_l, img_r, h_radius, v_radius, searchWindow);

		} else if (r_format == T_UINT8) {

			return callCostVolume2d<matchingFunc, uint8_t, uint8_t>(img_l, img_r, h_radius, v_radius, searchWindow);

		}

	}

	return pybind11::array_t<float>();

}

pybind11::array_t<float> computeFullCostVolume2d(pybind11::array img_l,
												  pybind11::array img_r,
												  std::array<int, 4> searchSpace,
												  uint8_t v_radius,
												  uint8_t h_radius,
												  std::string const& method) {

	for (int searchRadius : searchSpace) {
		constexpr int maxSearchRadius = 1000;
		if (std::abs(searchRadius) > maxSearchRadius) {
			std::stringstream ss;
			ss << "Maximal allowed search radius is " << maxSearchRadius << " !";
			throw std::invalid_argument(ss.str());
		}
	}

	std::string lowercaseMethod = method;
	std::transform(lowercaseMethod.begin(), lowercaseMethod.end(), lowercaseMethod.begin(),
		[](unsigned char c){ return std::tolower(c); });

	if (lowercaseMethod != "ncc" and
		lowercaseMethod != "sad" and
		lowercaseMethod != "ssd" and
		lowercaseMethod != "meds" and
		lowercaseMethod != "zncc" and
		lowercaseMethod != "zsad" and
		lowercaseMethod != "zssd" and
		lowercaseMethod != "zmeds") {

		std::stringstream ss;
		ss << "Unsupported method " << method << " !";
		throw std::invalid_argument(ss.str());

	}

	std::vector<long> l_shape = img_l.request().shape;
	std::vector<long> r_shape = img_r.request().shape;

	std::vector<long> l_strides = img_l.request().strides;
	std::vector<long> r_strides = img_r.request().strides;

	if (l_shape != r_shape) {
		throw std::invalid_argument("computeFullCostVolume2d needs arrays of the same shape !");
	}

	if (l_shape.size() != 2 and l_shape.size() != 3) {
		throw std::invalid_argument("computeFullCostVolume2d needs arrays with ndims = 2 or 3 !");
	}

	ArraysFormat l_format = formatFromName(img_l.request().format);
	ArraysFormat r_format = formatFromName(img_r.request().format);

	if (l_format == T_INVALID || r_format == T_INVALID) {
		throw std::invalid_argument("computeFullCostVolume2d uses arrays of either float or uint8 !");
	}

	Correlation::searchOffset<2> searchWindow(searchSpace[0],searchSpace[1],searchSpace[2],searchSpace[3]);

	if (lowercaseMethod == "ncc") {
		return callTypedCostVolume2d<StereoVision::Correlation::matchingFunctions::NCC>(
					img_l,
					l_format,
					img_r,
					r_format,
					searchWindow,
					v_radius,
					h_radius);
	} else if (lowercaseMethod == "zncc") {
		return callTypedCostVolume2d<StereoVision::Correlation::matchingFunctions::ZNCC>(
					img_l,
					l_format,
					img_r,
					r_format,
					searchWindow,
					v_radius,
					h_radius);
	} else if (lowercaseMethod == "sad") {

		return callTypedCostVolume2d<StereoVision::Correlation::matchingFunctions::SAD>(
					img_l,
					l_format,
					img_r,
					r_format,
					searchWindow,
					v_radius,
					h_radius);

	} else if (lowercaseMethod == "zsad") {

		return callTypedCostVolume2d<StereoVision::Correlation::matchingFunctions::ZSAD>(
					img_l,
					l_format,
					img_r,
					r_format,
					searchWindow,
					v_radius,
					h_radius);

	} else if (lowercaseMethod == "ssd") {

		return callTypedCostVolume2d<StereoVision::Correlation::matchingFunctions::SSD>(
					img_l,
					l_format,
					img_r,
					r_format,
					searchWindow,
					v_radius,
					h_radius);

	} else if (lowercaseMethod == "zssd") {

		return callTypedCostVolume2d<StereoVision::Correlation::matchingFunctions::ZSSD>(
					img_l,
					l_format,
					img_r,
					r_format,
					searchWindow,
					v_radius,
					h_radius);

	} else if (lowercaseMethod == "meds") {

		return callTypedCostVolume2d<StereoVision::Correlation::matchingFunctions::MEDAD>(
					img_l,
					l_format,
					img_r,
					r_format,
					searchWindow,
					v_radius,
					h_radius);

	} else if (lowercaseMethod == "zmeds") {

		return callTypedCostVolume2d<StereoVision::Correlation::matchingFunctions::ZMEDAD>(
					img_l,
					l_format,
					img_r,
					r_format,
					searchWindow,
					v_radius,
					h_radius);

	}

	return pybind11::array_t<float>();
}

template<Correlation::matchingFunctions matchingFunc>
pybind11::array_t<float> callTypedCostVolume2d(pybind11::array img_l,
												ArraysFormat l_format,
												pybind11::array img_r,
												ArraysFormat r_format,
												Correlation::searchOffset<2> const& searchWindow,
												Multidim::Array<int,2> const& compressor_mask) {


	if (l_format == T_FLOAT) {

		if (r_format == T_FLOAT) {

			return callCostVolume2d<matchingFunc, float, float>(img_l, img_r, compressor_mask, searchWindow);

		} else if (r_format == T_UINT8) {

			return callCostVolume2d<matchingFunc, float, uint8_t>(img_l, img_r, compressor_mask, searchWindow);

		}

	} else if (l_format == T_UINT8) {

		if (r_format == T_FLOAT) {

			return callCostVolume2d<matchingFunc, uint8_t, float>(img_l, img_r, compressor_mask, searchWindow);

		} else if (r_format == T_UINT8) {

			return callCostVolume2d<matchingFunc, uint8_t, uint8_t>(img_l, img_r, compressor_mask, searchWindow);

		}

	}

	return pybind11::array_t<float>();

}

pybind11::array_t<float> computeFullCostVolume2dWithCompressor(pybind11::array img_l,
																pybind11::array img_r,
																pybind11::array_t<int> compressor,
																std::array<int, 4> searchSpace,
																std::string const& method) {

	for (int searchRadius : searchSpace) {
		constexpr int maxSearchRadius = 1000;
		if (std::abs(searchRadius) > maxSearchRadius) {
			std::stringstream ss;
			ss << "Maximal allowed search radius is " << maxSearchRadius << " !";
			throw std::invalid_argument(ss.str());
		}
	}

	std::string lowercaseMethod = method;
	std::transform(lowercaseMethod.begin(), lowercaseMethod.end(), lowercaseMethod.begin(),
		[](unsigned char c){ return std::tolower(c); });

	if (lowercaseMethod != "ncc" and
		lowercaseMethod != "sad" and
		lowercaseMethod != "ssd" and
		lowercaseMethod != "meds" and
		lowercaseMethod != "zncc" and
		lowercaseMethod != "zsad" and
		lowercaseMethod != "zssd" and
		lowercaseMethod != "zmeds") {

		std::stringstream ss;
		ss << "Unsupported method " << method << " !";
		throw std::invalid_argument(ss.str());

	}

	std::vector<long> l_shape = img_l.request().shape;
	std::vector<long> r_shape = img_r.request().shape;

	std::vector<long> l_strides = img_l.request().strides;
	std::vector<long> r_strides = img_r.request().strides;

	if (l_shape != r_shape) {
		throw std::invalid_argument("computeFullCostVolume2d needs arrays of the same shape !");
	}

	if (l_shape.size() != 2 and l_shape.size() != 3) {
		throw std::invalid_argument("computeFullCostVolume2d needs arrays with ndims = 2 or 3 !");
	}

	std::vector<long> compressor_shape = compressor.request().shape;

	if (compressor_shape.size() != 2) {
		throw std::invalid_argument("computeFullCostVolume2d needs a compressor mask with ndims = 2 !");
	}

	Multidim::Array<int, 2>::ShapeBlock mask_shape;
	mask_shape[0] = compressor_shape[0];
	mask_shape[1] = compressor_shape[1];

	std::vector<long> compressor_strides = compressor.request().strides;
	Multidim::Array<int, 2>::ShapeBlock mask_strides;
	mask_strides[0] = compressor_strides[0]/sizeof (int);
	mask_strides[1] = compressor_strides[1]/sizeof (int);

	Multidim::Array<int,2> compressor_mask(compressor.mutable_data(), mask_shape, mask_strides);

	ArraysFormat l_format = formatFromName(img_l.request().format);
	ArraysFormat r_format = formatFromName(img_r.request().format);

	if (l_format == T_INVALID || r_format == T_INVALID) {
		throw std::invalid_argument("computeFullCostVolume2d uses arrays of either float or uint8 !");
	}

	Correlation::searchOffset<2> searchWindow(searchSpace[0],searchSpace[1],searchSpace[2],searchSpace[3]);

	if (lowercaseMethod == "ncc") {
		return callTypedCostVolume2d<StereoVision::Correlation::matchingFunctions::NCC>(
					img_l,
					l_format,
					img_r,
					r_format,
					searchWindow,
					compressor_mask);
	} else if (lowercaseMethod == "zncc") {
		return callTypedCostVolume2d<StereoVision::Correlation::matchingFunctions::ZNCC>(
					img_l,
					l_format,
					img_r,
					r_format,
					searchWindow,
					compressor_mask);
	} else if (lowercaseMethod == "sad") {

		return callTypedCostVolume2d<StereoVision::Correlation::matchingFunctions::SAD>(
					img_l,
					l_format,
					img_r,
					r_format,
					searchWindow,
					compressor_mask);

	} else if (lowercaseMethod == "zsad") {

		return callTypedCostVolume2d<StereoVision::Correlation::matchingFunctions::ZSAD>(
					img_l,
					l_format,
					img_r,
					r_format,
					searchWindow,
					compressor_mask);

	} else if (lowercaseMethod == "ssd") {

		return callTypedCostVolume2d<StereoVision::Correlation::matchingFunctions::SSD>(
					img_l,
					l_format,
					img_r,
					r_format,
					searchWindow,
					compressor_mask);

	} else if (lowercaseMethod == "zssd") {

		return callTypedCostVolume2d<StereoVision::Correlation::matchingFunctions::ZSSD>(
					img_l,
					l_format,
					img_r,
					r_format,
					searchWindow,
					compressor_mask);

	} else if (lowercaseMethod == "meds") {

		return callTypedCostVolume2d<StereoVision::Correlation::matchingFunctions::MEDAD>(
					img_l,
					l_format,
					img_r,
					r_format,
					searchWindow,
					compressor_mask);

	} else if (lowercaseMethod == "zmeds") {

		return callTypedCostVolume2d<StereoVision::Correlation::matchingFunctions::ZMEDAD>(
					img_l,
					l_format,
					img_r,
					r_format,
					searchWindow,
					compressor_mask);

	}

	return pybind11::array_t<float>();

}

pybind11::array_t<disp_t> extractRawDisparity(pybind11::array_t<float> costVolume, std::string const& cv_type) {

	std::string lowercaseType = cv_type;
	std::transform(lowercaseType.begin(), lowercaseType.end(), lowercaseType.begin(),
		[](unsigned char c){ return std::tolower(c); });

	if (lowercaseType != "score" and
		lowercaseType != "cost") {

		std::stringstream ss;
		ss << "Unsupported cost volume type " << cv_type << " !";
		throw std::invalid_argument(ss.str());

	}

	auto shape = costVolume.request().shape;

	if (shape.size() != 3) {
		std::stringstream ss;
		ss << "Expected a cost volume of dimension 3, got " << shape.size() << " !";
		throw std::invalid_argument(ss.str());
	}

	auto strides = costVolume.request().strides;

	Multidim::Array<float,3> cv(static_cast<float*>(costVolume.mutable_data(0)),
								{static_cast<int>(shape[0]), static_cast<int>(shape[1]), static_cast<int>(shape[2])},
								{static_cast<int>(strides[0]/sizeof (float)),
								 static_cast<int>(strides[1]/sizeof (float)),
								 static_cast<int>(strides[2]/sizeof (float))});

	if (lowercaseType == "score") {
		return Multidim2NumpyArray(
					StereoVision::Correlation::extractSelectedIndex<StereoVision::Correlation::dispExtractionStartegy::Score, float>(cv)
				);
	} else if (lowercaseType == "cost") {
		return Multidim2NumpyArray(
					StereoVision::Correlation::extractSelectedIndex<StereoVision::Correlation::dispExtractionStartegy::Cost, float>(cv)
				);
	}

	return pybind11::array_t<disp_t>();


}

pybind11::array_t<disp_t> extractDisparityDP(pybind11::array_t<float> costVolume,
											 std::string const& cv_type) {

	std::string lowercaseType = cv_type;
	std::transform(lowercaseType.begin(), lowercaseType.end(), lowercaseType.begin(),
		[](unsigned char c){ return std::tolower(c); });

	if (lowercaseType != "score" and
		lowercaseType != "cost") {

		std::stringstream ss;
		ss << "Unsupported cost volume type " << cv_type << " !";
		throw std::invalid_argument(ss.str());

	}

	constexpr StereoVision::Correlation::dispExtractionStartegy sStrat = StereoVision::Correlation::dispExtractionStartegy::Score;
	constexpr StereoVision::Correlation::dispExtractionStartegy cStrat = StereoVision::Correlation::dispExtractionStartegy::Cost;

	StereoVision::Correlation::DynamicProgramming::JumpCostPolicy<float> jumpCostPolicy;

	if (lowercaseType == "score") {
		float  cost1 = 0.3;
		float cost2 = 0.1;
		jumpCostPolicy = StereoVision::Correlation::DynamicProgramming::SGMLikeJumpCostPolicy<sStrat, float>(cost1, cost2);
	} else if (lowercaseType == "cost") {
		float  cost1 = 0.2;
		float cost2 = 0.1;
		jumpCostPolicy = StereoVision::Correlation::DynamicProgramming::SGMLikeJumpCostPolicy<cStrat, float>(cost1, cost2);
	}

	auto shape = costVolume.request().shape;

	if (shape.size() != 3) {
		std::stringstream ss;
		ss << "Expected a cost volume of dimension 3, got " << shape.size() << " !";
		throw std::invalid_argument(ss.str());
	}

	auto strides = costVolume.request().strides;

	Multidim::Array<float,3> cv(static_cast<float*>(costVolume.mutable_data(0)),
								{static_cast<int>(shape[0]), static_cast<int>(shape[1]), static_cast<int>(shape[2])},
								{static_cast<int>(strides[0]/sizeof (float)),
								 static_cast<int>(strides[1]/sizeof (float)),
								 static_cast<int>(strides[2]/sizeof (float))});

	if (lowercaseType == "score") {
		return Multidim2NumpyArray(
					StereoVision::Correlation::DynamicProgramming::extractOptimalIndex<sStrat, float>(cv, jumpCostPolicy)
				);
	} else if (lowercaseType == "cost") {
		return Multidim2NumpyArray(
					StereoVision::Correlation::DynamicProgramming::extractOptimalIndex<cStrat, float>(cv, jumpCostPolicy)
				);
	}

	return pybind11::array_t<disp_t>();
}

pybind11::array_t<disp_t> extractRawDisparity2d(pybind11::array_t<float> costVolume,
												 std::string const& cv_type) {

	std::string lowercaseType = cv_type;
	std::transform(lowercaseType.begin(), lowercaseType.end(), lowercaseType.begin(),
		[](unsigned char c){ return std::tolower(c); });

	if (lowercaseType != "score" and
		lowercaseType != "cost") {

		std::stringstream ss;
		ss << "Unsupported cost volume type " << cv_type << " !";
		throw std::invalid_argument(ss.str());

	}

	auto shape = costVolume.request().shape;

	if (shape.size() != 4) {
		std::stringstream ss;
		ss << "Expected a cost volume of dimension 4, got " << shape.size() << " !";
		throw std::invalid_argument(ss.str());
	}

	auto strides = costVolume.request().strides;

	Multidim::Array<float,4> cv(static_cast<float*>(costVolume.mutable_data(0)),
								{static_cast<int>(shape[0]),
								 static_cast<int>(shape[1]),
								 static_cast<int>(shape[2]),
								 static_cast<int>(shape[3])},
								{static_cast<int>(strides[0]/sizeof (float)),
								 static_cast<int>(strides[1]/sizeof (float)),
								 static_cast<int>(strides[2]/sizeof (float)),
								 static_cast<int>(strides[3]/sizeof (float))});

	if (lowercaseType == "score") {
		return Multidim2NumpyArray(
					Correlation::extractSelected2dIndex<Correlation::dispExtractionStartegy::Score, float>(cv)
				);
	} else if (lowercaseType == "cost") {
		return Multidim2NumpyArray(
					Correlation::extractSelected2dIndex<Correlation::dispExtractionStartegy::Cost, float>(cv)
				);
	}

	return pybind11::array_t<disp_t>();


}

pybind11::array_t<disp_t> offset2dDisparity( pybind11::array_t<disp_t> rawDisp,
											  std::array<int, 4> searchSpace) {

	auto d_shape = rawDisp.request().shape;

	if (d_shape.size() != 3) {
		std::stringstream ss;
		ss << "Expected a disparity map of dimension 3, got " << d_shape.size() << " !";
		throw std::invalid_argument(ss.str());
	}

	if (d_shape[2] != 2) {
		std::stringstream ss;
		ss << "Expected a disparity map with 2 disparity channels, got " << d_shape[2] << " !";
		throw std::invalid_argument(ss.str());
	}

	auto d_strides = rawDisp.request().strides;

	Multidim::Array<disp_t,3> disp(static_cast<disp_t*>(rawDisp.mutable_data(0)),
								{static_cast<int>(d_shape[0]),
								 static_cast<int>(d_shape[1]),
								 static_cast<int>(d_shape[2])},
								{static_cast<int>(d_strides[0]/sizeof (disp_t)),
								 static_cast<int>(d_strides[1]/sizeof (disp_t)),
								 static_cast<int>(d_strides[2]/sizeof (disp_t))});

	Correlation::searchOffset<2> searchWindow(searchSpace[0],searchSpace[1],searchSpace[2],searchSpace[3]);

	return Multidim2NumpyArray(Correlation::selected2dIndexToDisp(disp, searchWindow));

}

template<typename T_Guide, int nDim>
Multidim::Array<float, 3> getGuidedAggregatedCostVolumeImpl(Multidim::Array<float,3> & costVolume,
															pybind11::array & guide,
															uint8_t h_radius,
															uint8_t v_radius,
															float sigma) {

	using GuideArrayT = Multidim::Array<T_Guide, nDim>;

	auto guide_shape = guide.request().shape;
	auto guide_strides = guide.request().strides;

	if (guide_shape.size() != nDim) {
		return Multidim::Array<float, 3>();
	}

	typename GuideArrayT::ShapeBlock shape;
	typename GuideArrayT::ShapeBlock strides;

	for (int d = 0; d < nDim; d++) {
		shape[d] = guide_shape[d];
		strides[d] = guide_strides[d]/sizeof (T_Guide);
	}

	GuideArrayT guideArray(static_cast<T_Guide*>(guide.mutable_data(0)), shape, strides);

	auto spatialWeightFunc = [sigma] (int di, int dj) -> float {
		float w = (di*di)/(sigma*sigma) + (dj*dj)/(sigma*sigma);
		return std::exp(-w);
	};


	auto matchWeightFunc = [sigma] (Multidim::Array<T_Guide,1> const& ref,
			Multidim::Array<T_Guide,1> const& target) -> float {

		float w = 0;

		int nChannels = std::min(ref.shape()[0], target.shape()[0]);

		for (int c = 0; c < nChannels; c++) {
			float delta = ref.valueUnchecked(c) - target.valueUnchecked(c);
			w += (delta*delta)/(sigma*sigma);
		}
		return std::exp(-w/ref.shape()[0]);
	};

	Multidim::Array<float, 4> aggregatonWindows =
			StereoVision::Correlation::computeAggregationWindows<float, T_Guide, nDim>(guideArray,
																					   h_radius,
																					   v_radius,
																					   spatialWeightFunc,
																					   matchWeightFunc);

	return StereoVision::Correlation::variableCostVolumeAggregation(costVolume, aggregatonWindows);

}

pybind11::array_t<float> getGuidedAggregatedCostVolume(pybind11::array_t<float> costVolume,
													   pybind11::array guide,
													   uint8_t h_radius,
													   uint8_t v_radius,
													   float sigma) {

	auto cv_shape = costVolume.request().shape;

	if (cv_shape.size() != 3) {
		std::stringstream ss;
		ss << "Expected a cost volume of dimension 3, got " << cv_shape.size() << " !";
		throw std::invalid_argument(ss.str());
	}

	auto guide_shape = guide.request().shape;

	if (guide_shape.size() != 2 and guide_shape.size() != 3) {
		std::stringstream ss;
		ss << "Expected a guide of dimension 2 or 3, got " << guide_shape.size() << " !";
		throw std::invalid_argument(ss.str());
	}

	auto cv_strides = costVolume.request().strides;

	Multidim::Array<float,3> cv(static_cast<float*>(costVolume.mutable_data(0)),
								{static_cast<int>(cv_shape[0]), static_cast<int>(cv_shape[1]), static_cast<int>(cv_shape[2])},
								{static_cast<int>(cv_strides[0]/sizeof (float)),
								 static_cast<int>(cv_strides[1]/sizeof (float)),
								 static_cast<int>(cv_strides[2]/sizeof (float))});


	ArraysFormat g_format = formatFromName(guide.request().format);

	if (g_format == T_INVALID) {
		std::stringstream ss;
		ss << "Invalid guide pixel format: " << guide.request().format << " !";
		throw std::invalid_argument(ss.str());
	}

	switch (g_format) {
	case T_UINT8:

		if (guide_shape.size() == 2) {
		return Multidim2NumpyArray(
					getGuidedAggregatedCostVolumeImpl<uint8_t, 2>(cv, guide, h_radius, v_radius, sigma)
				);
		} else if (guide_shape.size() == 3) {
			return Multidim2NumpyArray(
					getGuidedAggregatedCostVolumeImpl<uint8_t, 3>(cv, guide, h_radius, v_radius, sigma)
				);
		}
		break;

	case T_FLOAT:

		if (guide_shape.size() == 2) {
		return Multidim2NumpyArray(
					getGuidedAggregatedCostVolumeImpl<float, 2>(cv, guide, h_radius, v_radius, sigma)
				);
		} else if (guide_shape.size() == 3) {
			return Multidim2NumpyArray(
					getGuidedAggregatedCostVolumeImpl<float, 3>(cv, guide, h_radius, v_radius, sigma)
				);
		}
		break;
	default:
		break;
	}

	return pybind11::array_t<float>();
}

pybind11::array_t<float> getTruncatedCostVolume(pybind11::array_t<float> costVolume,
												 pybind11::array_t<disp_t> rawDisp,
												 uint8_t h_radius,
												 uint8_t v_radius,
												 uint8_t cost_vol_radius,
												 std::string const& dir,
												 std::string const& tcv_type) {

	auto cv_shape = costVolume.request().shape;

	if (cv_shape.size() != 3) {
		std::stringstream ss;
		ss << "Expected a cost volume of dimension 3, got " << cv_shape.size() << " !";
		throw std::invalid_argument(ss.str());
	}

	std::string lowercaseDir = dir;
	std::transform(lowercaseDir.begin(), lowercaseDir.end(), lowercaseDir.begin(),
		[](unsigned char c){ return std::tolower(c); });

	if (lowercaseDir != "r2l" and
		lowercaseDir != "l2r") {

		std::stringstream ss;
		ss << "Unsupported matching direction " << dir << " !";
		throw std::invalid_argument(ss.str());

	}

	std::string lowercaseType = tcv_type;
	std::transform(lowercaseType.begin(), lowercaseType.end(), lowercaseType.begin(),
		[](unsigned char c){ return std::tolower(c); });

	if (lowercaseType != "same" and
		lowercaseType != "reverse" and
		lowercaseType != "both") {

		std::stringstream ss;
		ss << "Unsupported cost volume type " << tcv_type << " !";
		throw std::invalid_argument(ss.str());

	}

	auto cv_strides = costVolume.request().strides;

	Multidim::Array<float,3> cv(static_cast<float*>(costVolume.mutable_data(0)),
								{static_cast<int>(cv_shape[0]), static_cast<int>(cv_shape[1]), static_cast<int>(cv_shape[2])},
								{static_cast<int>(cv_strides[0]/sizeof (float)),
								 static_cast<int>(cv_strides[1]/sizeof (float)),
								 static_cast<int>(cv_strides[2]/sizeof (float))});

	auto d_shape = rawDisp.request().shape;

	if (d_shape.size() != 2) {
		std::stringstream ss;
		ss << "Expected a disparity map of dimension 2, got " << d_shape.size() << " !";
		throw std::invalid_argument(ss.str());
	}

	auto d_strides = rawDisp.request().strides;

	Multidim::Array<disp_t,2> disp(static_cast<disp_t*>(rawDisp.mutable_data(0)),
								{static_cast<int>(d_shape[0]), static_cast<int>(d_shape[1])},
								{static_cast<int>(d_strides[0]/sizeof (disp_t)),
								 static_cast<int>(d_strides[1]/sizeof (disp_t))});

	if (lowercaseDir == "r2l") {

		if (lowercaseType == "same") {
			return Multidim2NumpyArray(
						StereoVision::Correlation::truncatedCostVolume<float,
						StereoVision::Correlation::dispDirection::RightToLeft,
						StereoVision::Correlation::truncatedCostVolumeDirection::Same>
						(cv, disp, h_radius, v_radius, cost_vol_radius)
					);
		} else if (lowercaseType == "reverse") {
			return Multidim2NumpyArray(
						StereoVision::Correlation::truncatedCostVolume<float,
						StereoVision::Correlation::dispDirection::RightToLeft,
						StereoVision::Correlation::truncatedCostVolumeDirection::Reversed>
						(cv, disp, h_radius, v_radius, cost_vol_radius)
					);
		} else if (lowercaseType == "both") {
			return Multidim2NumpyArray(
						StereoVision::Correlation::truncatedCostVolume<float,
						StereoVision::Correlation::dispDirection::RightToLeft,
						StereoVision::Correlation::truncatedCostVolumeDirection::Both>
						(cv, disp, h_radius, v_radius, cost_vol_radius)
					);
		}

	} else if (lowercaseDir == "l2r") {

		if (lowercaseType == "same") {
			return Multidim2NumpyArray(
						StereoVision::Correlation::truncatedCostVolume<float,
						StereoVision::Correlation::dispDirection::LeftToRight,
						StereoVision::Correlation::truncatedCostVolumeDirection::Same>
						(cv, disp, h_radius, v_radius, cost_vol_radius)
					);
		} else if (lowercaseType == "reverse") {
			return Multidim2NumpyArray(
						StereoVision::Correlation::truncatedCostVolume<float,
						StereoVision::Correlation::dispDirection::LeftToRight,
						StereoVision::Correlation::truncatedCostVolumeDirection::Reversed>
						(cv, disp, h_radius, v_radius, cost_vol_radius)
					);
		} else if (lowercaseType == "both") {
			return Multidim2NumpyArray(
						StereoVision::Correlation::truncatedCostVolume<float,
						StereoVision::Correlation::dispDirection::LeftToRight,
						StereoVision::Correlation::truncatedCostVolumeDirection::Both>
						(cv, disp, h_radius, v_radius, cost_vol_radius)
					);
		}

	}

	return Multidim2NumpyArray(
				StereoVision::Correlation::truncatedCostVolume(cv, disp, h_radius, v_radius, cost_vol_radius)
			);

}



pybind11::array_t<float> getTruncatedCostVolume2d(pybind11::array_t<float> costVolume,
												   pybind11::array_t<disp_t> rawDisp,
												   uint8_t cost_vol_radius_h,
												   uint8_t cost_vol_radius_v) {

	auto cv_shape = costVolume.request().shape;

	if (cv_shape.size() != 4) {
		std::stringstream ss;
		ss << "Expected a cost volume of dimension 4, got " << cv_shape.size() << " !";
		throw std::invalid_argument(ss.str());
	}

	auto cv_strides = costVolume.request().strides;

	Multidim::Array<float,4> cv(static_cast<float*>(costVolume.mutable_data(0)),
								{static_cast<int>(cv_shape[0]),
								 static_cast<int>(cv_shape[1]),
								 static_cast<int>(cv_shape[2]),
								 static_cast<int>(cv_shape[3])},
								{static_cast<int>(cv_strides[0]/sizeof (float)),
								 static_cast<int>(cv_strides[1]/sizeof (float)),
								 static_cast<int>(cv_strides[2]/sizeof (float)),
								 static_cast<int>(cv_strides[3]/sizeof (float))});

	auto d_shape = rawDisp.request().shape;

	if (d_shape.size() != 3) {
		std::stringstream ss;
		ss << "Expected a disparity map of dimension 3, got " << d_shape.size() << " !";
		throw std::invalid_argument(ss.str());
	}

	if (d_shape[2] != 2) {
		std::stringstream ss;
		ss << "Expected a disparity map with 2 disparity channels, got " << d_shape[2] << " !";
		throw std::invalid_argument(ss.str());
	}

	auto d_strides = rawDisp.request().strides;

	Multidim::Array<disp_t,3> disp(static_cast<disp_t*>(rawDisp.mutable_data(0)),
								{static_cast<int>(d_shape[0]),
								 static_cast<int>(d_shape[1]),
								 static_cast<int>(d_shape[2])},
								{static_cast<int>(d_strides[0]/sizeof (disp_t)),
								 static_cast<int>(d_strides[1]/sizeof (disp_t)),
								 static_cast<int>(d_strides[2]/sizeof (disp_t))});

	return Multidim2NumpyArray(Correlation::truncatedBidirectionaCostVolume(cv, disp, cost_vol_radius_v, cost_vol_radius_h));

}

pybind11::array_t<float> getInBound(pybind11::array_t<disp_t> rawDisp,
									 uint32_t width,
									 uint8_t h_radius,
									 uint8_t v_radius,
									 uint8_t cost_vol_radius,
									 std::string const& dir,
									 std::string const& tcv_type) {

	auto d_shape = rawDisp.request().shape;

	if (d_shape.size() != 2) {
		std::stringstream ss;
		ss << "Expected a raw disparity of dimension 2, got " << d_shape.size() << " !";
		throw std::invalid_argument(ss.str());
	}

	if (d_shape.size() != 2) {
		std::stringstream ss;
		ss << "Expected a disparity map of dimension 2, got " << d_shape.size() << " !";
		throw std::invalid_argument(ss.str());
	}

	std::string lowercaseDir = dir;
	std::transform(lowercaseDir.begin(), lowercaseDir.end(), lowercaseDir.begin(),
		[](unsigned char c){ return std::tolower(c); });

	if (lowercaseDir != "r2l" and
		lowercaseDir != "l2r") {

		std::stringstream ss;
		ss << "Unsupported matching direction " << dir << " !";
		throw std::invalid_argument(ss.str());

	}

	std::string lowercaseType = tcv_type;
	std::transform(lowercaseType.begin(), lowercaseType.end(), lowercaseType.begin(),
		[](unsigned char c){ return std::tolower(c); });

	if (lowercaseType != "same" and
		lowercaseType != "reverse" and
		lowercaseType != "both") {

		std::stringstream ss;
		ss << "Unsupported cost volume type " << tcv_type << " !";
		throw std::invalid_argument(ss.str());

	}


	auto d_strides = rawDisp.request().strides;

	Multidim::Array<disp_t,2> disp(static_cast<disp_t*>(rawDisp.mutable_data(0)),
								{static_cast<int>(d_shape[0]), static_cast<int>(d_shape[1])},
								{static_cast<int>(d_strides[0]/sizeof (disp_t)),
								 static_cast<int>(d_strides[1]/sizeof (disp_t))});

	if (lowercaseDir == "r2l") {

		if (lowercaseType == "same") {
			return Multidim2NumpyArray(
						StereoVision::Correlation::extractInBoundDomain<float,
						StereoVision::Correlation::dispDirection::RightToLeft,
						StereoVision::Correlation::truncatedCostVolumeDirection::Same>
						(disp, width, h_radius, v_radius, cost_vol_radius)
					);
		} else if (lowercaseType == "reverse") {
			return Multidim2NumpyArray(
						StereoVision::Correlation::extractInBoundDomain<float,
						StereoVision::Correlation::dispDirection::RightToLeft,
						StereoVision::Correlation::truncatedCostVolumeDirection::Reversed>
						(disp, width, h_radius, v_radius, cost_vol_radius)
					);
		} else if (lowercaseType == "both") {
			return Multidim2NumpyArray(
						StereoVision::Correlation::extractInBoundDomain<float,
						StereoVision::Correlation::dispDirection::RightToLeft,
						StereoVision::Correlation::truncatedCostVolumeDirection::Both>
						(disp, width, h_radius, v_radius, cost_vol_radius)
					);
		}

	} else if (lowercaseDir == "l2r") {

		if (lowercaseType == "same") {
			return Multidim2NumpyArray(
						StereoVision::Correlation::extractInBoundDomain<float,
						StereoVision::Correlation::dispDirection::LeftToRight,
						StereoVision::Correlation::truncatedCostVolumeDirection::Same>
						(disp, width, h_radius, v_radius, cost_vol_radius)
					);
		} else if (lowercaseType == "reverse") {
			return Multidim2NumpyArray(
						StereoVision::Correlation::extractInBoundDomain<float,
						StereoVision::Correlation::dispDirection::LeftToRight,
						StereoVision::Correlation::truncatedCostVolumeDirection::Reversed>
						(disp, width, h_radius, v_radius, cost_vol_radius)
					);
		} else if (lowercaseType == "both") {
			return Multidim2NumpyArray(
						StereoVision::Correlation::extractInBoundDomain<float,
						StereoVision::Correlation::dispDirection::LeftToRight,
						StereoVision::Correlation::truncatedCostVolumeDirection::Both>
						(disp, width, h_radius, v_radius, cost_vol_radius)
					);
		}

	}

	return Multidim2NumpyArray(
				StereoVision::Correlation::extractInBoundDomain<float>(disp, width, h_radius, v_radius, cost_vol_radius)
			);
}

pybind11::array_t<float> getCostRefinedDisparity(pybind11::array_t<float> truncatedCostVolume,
												  pybind11::array_t<disp_t> rawDisp,
												  std::string const& interpolation) {

	std::string lowercaseInterpolation = interpolation;
	std::transform(lowercaseInterpolation.begin(), lowercaseInterpolation.end(), lowercaseInterpolation.begin(),
		[](unsigned char c){ return std::tolower(c); });

	if (lowercaseInterpolation != "parabola" and
			lowercaseInterpolation != "equiangular" and
			lowercaseInterpolation != "gaussian") {

		std::stringstream ss;
		ss << "Unsupported cost interpolation method " << interpolation << " !";
		throw std::invalid_argument(ss.str());

	}

	auto cv_shape = truncatedCostVolume.request().shape;

	if (cv_shape.size() != 3) {
		std::stringstream ss;
		ss << "Expected a cost volume of dimension 3, got " << cv_shape.size() << " !";
		throw std::invalid_argument(ss.str());
	}

	auto cv_strides = truncatedCostVolume.request().strides;

	Multidim::Array<float,3> tcv(static_cast<float*>(truncatedCostVolume.mutable_data(0)),
								{static_cast<int>(cv_shape[0]), static_cast<int>(cv_shape[1]), static_cast<int>(cv_shape[2])},
								{static_cast<int>(cv_strides[0]/sizeof (float)),
								 static_cast<int>(cv_strides[1]/sizeof (float)),
								 static_cast<int>(cv_strides[2]/sizeof (float))});

	auto d_shape = rawDisp.request().shape;

	if (d_shape.size() != 2) {
		std::stringstream ss;
		ss << "Expected a disparity map of dimension 2, got " << d_shape.size() << " !";
		throw std::invalid_argument(ss.str());
	}

	auto d_strides = rawDisp.request().strides;

	Multidim::Array<disp_t,2> disp(static_cast<disp_t*>(rawDisp.mutable_data(0)),
								{static_cast<int>(d_shape[0]), static_cast<int>(d_shape[1])},
								{static_cast<int>(d_strides[0]/sizeof (disp_t)),
								 static_cast<int>(d_strides[1]/sizeof (disp_t))});

	if (lowercaseInterpolation == "parabola") {
		constexpr auto parabola = StereoVision::Correlation::InterpolationKernel::Parabola;
		return Multidim2NumpyArray(
					StereoVision::Correlation::refineDispCostInterpolation<parabola>(tcv, disp)
				);
	} else if (lowercaseInterpolation == "equiangular") {
		constexpr auto equiangular = StereoVision::Correlation::InterpolationKernel::Equiangular;
		return Multidim2NumpyArray(
					StereoVision::Correlation::refineDispCostInterpolation<equiangular>(tcv, disp)
				);
	} else if (lowercaseInterpolation == "gaussian") {
		constexpr auto gaussian = StereoVision::Correlation::InterpolationKernel::Gaussian;
		return Multidim2NumpyArray(
					StereoVision::Correlation::refineDispCostInterpolation<gaussian>(tcv, disp)
				);
	}

	return pybind11::array_t<float>();

}


template<Correlation::matchingFunctions matchFunc,
		 Correlation::dispDirection dispDir,
		 typename T_L,
		 typename T_R,
		 Multidim::array_size_t nDim>
inline pybind11::array_t<float> callImageRefinedDisparity(pybind11::array const& img_l,
														  pybind11::array const& img_r,
														  uint8_t v_radius,
														  uint8_t h_radius,
														  pybind11::array_t<disp_t> const& rawDisp) {

	Multidim::Array<T_L, nDim> image_left = Numpy2MultidimArray<T_L, nDim>(img_l);
	Multidim::Array<T_R, nDim> image_right = Numpy2MultidimArray<T_R, nDim>(img_r);

	Multidim::Array<disp_t, 2> raw_disparity = Numpy2MultidimArray<disp_t, 2>(rawDisp);

	return Multidim2NumpyArray(Correlation::refineBarycentricDisp<matchFunc, dispDir>(
								   Correlation::getFeatureVolumeForMatchFunc<matchFunc>(
									   Correlation::unfold(v_radius, h_radius, image_left)),
								   Correlation::getFeatureVolumeForMatchFunc<matchFunc>(
									   Correlation::unfold(v_radius, h_radius, image_right)),
								   raw_disparity
								   ));

}

template<Correlation::matchingFunctions matchFunc,
		 Correlation::dispDirection dispDir,
		 Multidim::array_size_t nDim>
inline pybind11::array_t<float> callTypedImageRefinedDisparity(pybind11::array const& img_l,
															   pybind11::array const& img_r,
															   uint8_t v_radius,
															   uint8_t h_radius,
															   pybind11::array_t<disp_t> const& rawDisp) {

	auto type_l = formatFromName(img_l.request().format);
	auto type_r = formatFromName(img_r.request().format);

	if (type_l == T_INVALID or type_r == T_INVALID) {
		throw std::invalid_argument("Unsupported image types, only float or bytes tensors are supported !");
	}

	if (type_l == T_FLOAT) {

		if (type_r == T_FLOAT) {
			return callImageRefinedDisparity<matchFunc,
					dispDir,
					float,
					float,
					nDim>(
						img_l,
						img_r,
						v_radius,
						h_radius,
						rawDisp
						);
		} else if (type_r == T_UINT8) {
			return callImageRefinedDisparity<matchFunc,
					dispDir,
					float,
					uint8_t,
					nDim>(
						img_l,
						img_r,
						v_radius,
						h_radius,
						rawDisp
						);
		}

	} else if (type_l == T_UINT8) {

		if (type_r == T_FLOAT) {
			return callImageRefinedDisparity<matchFunc,
					dispDir,
					uint8_t,
					float,
					nDim>(
						img_l,
						img_r,
						v_radius,
						h_radius,
						rawDisp
						);
		} else if (type_r == T_UINT8) {
			return callImageRefinedDisparity<matchFunc,
					dispDir,
					uint8_t,
					uint8_t,
					nDim>(
						img_l,
						img_r,
						v_radius,
						h_radius,
						rawDisp
						);
		}
	}

	return pybind11::array_t<float>();
}

template<Correlation::matchingFunctions matchFunc,
		 Multidim::array_size_t nDim>
inline pybind11::array_t<float> callDirectedImageRefinedDisparity(pybind11::array const& img_l,
																  pybind11::array const& img_r,
																  uint8_t v_radius,
																  uint8_t h_radius,
																  pybind11::array_t<disp_t> const& rawDisp,
																  std::string const& direction) {
	if (direction == "r2l") {
		return callTypedImageRefinedDisparity<matchFunc,
											Correlation::dispDirection::RightToLeft,
											nDim>(
					img_l,
					img_r,
					v_radius,
					h_radius,
					rawDisp
					);
	}

	if (direction == "l2r") {
		return callTypedImageRefinedDisparity<matchFunc,
											Correlation::dispDirection::LeftToRight,
											nDim>(
					img_l,
					img_r,
					v_radius,
					h_radius,
					rawDisp
					);
	}

	return pybind11::array_t<float>();
}

template<Multidim::array_size_t nDim>
inline pybind11::array_t<float> callDimSetImageRefinedDisparity(pybind11::array const& img_l,
																pybind11::array const& img_r,
																uint8_t v_radius,
																uint8_t h_radius,
																pybind11::array_t<disp_t> const& rawDisp,
																std::string const& costFunc,
																std::string const& direction) {

	if (costFunc == "ncc") {
		return callDirectedImageRefinedDisparity<Correlation::matchingFunctions::NCC, nDim> (
					img_l,
					img_r,
					v_radius,
					h_radius,
					rawDisp,
					direction
					);
	}

	if (costFunc == "zncc") {
		return callDirectedImageRefinedDisparity<Correlation::matchingFunctions::ZNCC, nDim> (
					img_l,
					img_r,
					v_radius,
					h_radius,
					rawDisp,
					direction
					);
	}

	if (costFunc == "sad") {
		return callDirectedImageRefinedDisparity<Correlation::matchingFunctions::SAD, nDim> (
					img_l,
					img_r,
					v_radius,
					h_radius,
					rawDisp,
					direction
					);
	}

	if (costFunc == "zsad") {
		return callDirectedImageRefinedDisparity<Correlation::matchingFunctions::ZSAD, nDim> (
					img_l,
					img_r,
					v_radius,
					h_radius,
					rawDisp,
					direction
					);
	}

	if (costFunc == "ssd") {
		return callDirectedImageRefinedDisparity<Correlation::matchingFunctions::SSD, nDim> (
					img_l,
					img_r,
					v_radius,
					h_radius,
					rawDisp,
					direction
					);
	}

	if (costFunc == "zssd") {
		return callDirectedImageRefinedDisparity<Correlation::matchingFunctions::ZSSD, nDim> (
					img_l,
					img_r,
					v_radius,
					h_radius,
					rawDisp,
					direction
					);
	}

	return pybind11::array_t<float>();
}

pybind11::array_t<float> getImageRefinedDisparity(pybind11::array img_l,
												  pybind11::array img_r,
												  pybind11::array_t<disp_t> rawDisp,
												  uint8_t v_radius,
												  uint8_t h_radius,
												  std::string const& costFunc,
												  std::string const& direction) {


	std::string lowercaseMethod = costFunc;
	std::transform(lowercaseMethod.begin(), lowercaseMethod.end(), lowercaseMethod.begin(),
		[](unsigned char c){ return std::tolower(c); });

	if (lowercaseMethod != "ncc" and
		lowercaseMethod != "sad" and
		lowercaseMethod != "ssd" and
		lowercaseMethod != "zncc" and
		lowercaseMethod != "zsad" and
		lowercaseMethod != "zssd") {

		std::stringstream ss;
		ss << "Unsupported method " << costFunc << " !";
		throw std::invalid_argument(ss.str());

	}

	int dims_l = img_l.ndim();
	int dims_r = img_r.ndim();

	if (dims_l != 2 and dims_l != 3) {

		std::stringstream ss;
		ss << "Unsupported image dimension " << dims_l << ", image should be a two or three dimensional tensor !";
		throw std::invalid_argument(ss.str());
	}

	if (dims_l != dims_r) {

		std::stringstream ss;
		ss << "Left image and right image dimensions are not matching: " << dims_l << " != " << dims_r << " !";
		throw std::invalid_argument(ss.str());
	}

	if (dims_l == 2) {
		return callDimSetImageRefinedDisparity<2>(img_l,
												  img_r,
												  v_radius,
												  h_radius,
												  rawDisp,
												  costFunc,
												  direction);
	}

	if (dims_l == 3) {
		return callDimSetImageRefinedDisparity<3>(img_l,
												  img_r,
												  v_radius,
												  h_radius,
												  rawDisp,
												  costFunc,
												  direction);
	}

	return pybind11::array_t<float>();

}

template<Correlation::IsotropyHypothesis isotropHypothesis = Correlation::IsotropyHypothesis::Isotropic>
inline pybind11::array_t<float> callCostRefinedDisparity2d(Multidim::Array<float,4> & tcv,
															Multidim::Array<disp_t,3> & disp,
															std::string const& interpolation,
															bool use2dFunctionMatching) {

	if (interpolation == "parabola") {
		constexpr auto parabola = StereoVision::Correlation::InterpolationKernel::Parabola;

		if (use2dFunctionMatching) {
			return Multidim2NumpyArray(
						StereoVision::Correlation::refineDisp2dCostPatchInterpolation<parabola>(tcv,disp)
					);
		}

		return Multidim2NumpyArray(
					StereoVision::Correlation::refineDisp2dCostInterpolation<parabola,isotropHypothesis>(tcv, disp)
				);

	} else if (interpolation == "equiangular") {

		constexpr auto equiangular = StereoVision::Correlation::InterpolationKernel::Equiangular;
		return Multidim2NumpyArray(
					StereoVision::Correlation::refineDisp2dCostInterpolation<equiangular,isotropHypothesis>(tcv, disp)
				);

	} else if (interpolation == "gaussian") {
		constexpr auto gaussian = StereoVision::Correlation::InterpolationKernel::Gaussian;

		if (use2dFunctionMatching) {
			return Multidim2NumpyArray(
						StereoVision::Correlation::refineDisp2dCostPatchInterpolation<gaussian>(tcv,disp)
					);
		}

		return Multidim2NumpyArray(
					StereoVision::Correlation::refineDisp2dCostInterpolation<gaussian,isotropHypothesis>(tcv, disp)
				);
	}

	return pybind11::array_t<float>();
}

pybind11::array_t<float> getCostRefinedDisparity2d(pybind11::array_t<float> truncatedCostVolume,
													pybind11::array_t<disp_t> rawDisp,
													std::string const& interpolation,
													bool assumeIsotropy,
													bool use2dFunctionMatching) {

	std::string lowercaseInterpolation = interpolation;
	std::transform(lowercaseInterpolation.begin(), lowercaseInterpolation.end(), lowercaseInterpolation.begin(),
		[](unsigned char c){ return std::tolower(c); });

	if (lowercaseInterpolation != "parabola" and
			lowercaseInterpolation != "equiangular" and
			lowercaseInterpolation != "gaussian") {

		std::stringstream ss;
		ss << "Unsupported cost interpolation method " << interpolation << " !";
		throw std::invalid_argument(ss.str());

	}

	auto cv_shape = truncatedCostVolume.request().shape;

	if (cv_shape.size() != 4) {
		std::stringstream ss;
		ss << "Expected a cost volume of dimension 4, got " << cv_shape.size() << " !";
		throw std::invalid_argument(ss.str());
	}

	auto cv_strides = truncatedCostVolume.request().strides;

	Multidim::Array<float,4> tcv(static_cast<float*>(truncatedCostVolume.mutable_data(0)),
								{static_cast<int>(cv_shape[0]),
								 static_cast<int>(cv_shape[1]),
								 static_cast<int>(cv_shape[2]),
								 static_cast<int>(cv_shape[3])},
								{static_cast<int>(cv_strides[0]/sizeof (float)),
								 static_cast<int>(cv_strides[1]/sizeof (float)),
								 static_cast<int>(cv_strides[2]/sizeof (float)),
								 static_cast<int>(cv_strides[3]/sizeof (float))});

	auto d_shape = rawDisp.request().shape;

	if (d_shape.size() != 3) {
		std::stringstream ss;
		ss << "Expected a disparity map of dimension 3, got " << d_shape.size() << " !";
		throw std::invalid_argument(ss.str());
	}

	if (d_shape[2] != 2) {
		std::stringstream ss;
		ss << "Expected a disparity map with 2 disparity channels, got " << d_shape[2] << " !";
		throw std::invalid_argument(ss.str());
	}

	auto d_strides = rawDisp.request().strides;

	Multidim::Array<disp_t,3> disp(static_cast<disp_t*>(rawDisp.mutable_data(0)),
								{static_cast<int>(d_shape[0]),
								 static_cast<int>(d_shape[1]),
								 static_cast<int>(d_shape[2])},
								{static_cast<int>(d_strides[0]/sizeof (disp_t)),
								 static_cast<int>(d_strides[1]/sizeof (disp_t)),
								 static_cast<int>(d_strides[2]/sizeof (disp_t))});

	if (assumeIsotropy) {
		return callCostRefinedDisparity2d<Correlation::IsotropyHypothesis::Isotropic>(tcv, disp, lowercaseInterpolation, use2dFunctionMatching);
	} else {
		return callCostRefinedDisparity2d<Correlation::IsotropyHypothesis::Anisotropic>(tcv, disp, lowercaseInterpolation, use2dFunctionMatching);
	}

	return pybind11::array_t<float>();
}

pybind11::array_t<float> computeRefinedDisparity(pybind11::array img_l,
												  pybind11::array img_r,
												  uint32_t width,
												  uint8_t v_radius,
												  uint8_t h_radius,
												  std::string const& method,
												  std::string const& interpolate,
												  bool preNormalize,
												 std::string const& direction) {


	std::string lowercaseMethod = interpolate;
	std::transform(lowercaseMethod.begin(), lowercaseMethod.end(), lowercaseMethod.begin(),
		[](unsigned char c){ return std::tolower(c); });

	if (lowercaseMethod == "image") {
		return computeImageRefinedDisparity(img_l, img_r, width, v_radius, h_radius, method, preNormalize, direction);
	} else if (lowercaseMethod == "barycentricsymmetric") {
		return computeBarycentricImageRefinedDisparity(img_l, img_r, width, v_radius, h_radius, method, preNormalize, direction);
	} else if (lowercaseMethod == "costsymmetric") {
		return computeSymmetricRefinedDisparity(img_l, img_r, width, v_radius, h_radius, method, preNormalize, direction);
	}

	std::stringstream ss;
	ss << "Unsupported interpolation approach " << interpolate << " !";
	throw std::invalid_argument(ss.str());

	return pybind11::array_t<float>();

}

pybind11::array_t<float> computeRefinedDisparityWithCompressor(pybind11::array img_l,
																pybind11::array img_r,
																pybind11::array_t<int> compressor_mask,
																uint32_t width,
																std::string const& method,
																std::string const& interpolate,
																bool preNormalize,
															   std::string const& direction) {
	std::string lowercaseMethod = interpolate;
	std::transform(lowercaseMethod.begin(), lowercaseMethod.end(), lowercaseMethod.begin(),
		[](unsigned char c){ return std::tolower(c); });

	if (lowercaseMethod == "image") {
		return computeImageRefinedDisparityWithCompressor(img_l, img_r, compressor_mask, width, method, preNormalize, direction);
	} else if (lowercaseMethod == "barycentricsymmetric") {
		return computeBarycentricImageRefinedDisparityWithCompressor(img_l, img_r, compressor_mask, width, method, preNormalize, direction);
	} else if (lowercaseMethod == "costsymmetric") {
		return computeSymmetricRefinedDisparityWithCompressor(img_l, img_r, compressor_mask, width, method, preNormalize, direction);
	}

	std::stringstream ss;
	ss << "Unsupported interpolation approach " << interpolate << " !";
	throw std::invalid_argument(ss.str());

	return pybind11::array_t<float>();
}



pybind11::array_t<float> computeRefinedDisparity2d(pybind11::array img_l,
													pybind11::array img_r,
													std::array<int,4> searchRegion,
													uint8_t v_radius,
													uint8_t h_radius,
													std::string const& method,
													std::string const& interpolate,
													std::string const& contiguity,
													bool preNormalize) {
	std::string lowercaseMethod = interpolate;
	std::transform(lowercaseMethod.begin(), lowercaseMethod.end(), lowercaseMethod.begin(),
		[](unsigned char c){ return std::tolower(c); });

	if (lowercaseMethod == "image") {
		bool useSymmetricBarycentricCoordinates = false;
		return computeImageRefinedDisparity2d(img_l,
											  img_r,
											  searchRegion,
											  v_radius,
											  h_radius,
											  method,
											  contiguity,
											  preNormalize,
											  useSymmetricBarycentricCoordinates);
	} else if (lowercaseMethod == "barycentricsymmetric") {
		bool useSymmetricBarycentricCoordinates = true;
		return computeImageRefinedDisparity2d(img_l,
											  img_r,
											  searchRegion,
											  v_radius,
											  h_radius,
											  method,
											  contiguity,
											  preNormalize,
											  useSymmetricBarycentricCoordinates);
	}

	std::stringstream ss;
	ss << "Unsupported interpolation approach " << interpolate << " !";
	throw std::invalid_argument(ss.str());

	return pybind11::array_t<float>();
}

pybind11::array_t<float> computeRefinedDisparity2dWithCompressor(pybind11::array img_l,
																  pybind11::array img_r,
																  std::array<int,4> searchRegion,
																  pybind11::array_t<int> compressor,
																  std::string const& method,
																  std::string const& interpolate,
																  std::string const& contiguity,
																  bool preNormalize) {
	std::string lowercaseMethod = interpolate;
	std::transform(lowercaseMethod.begin(), lowercaseMethod.end(), lowercaseMethod.begin(),
		[](unsigned char c){ return std::tolower(c); });

	if (lowercaseMethod == "image") {
		bool useSymmetricBarycentricCoordinates = false;
		return computeImageRefinedDisparity2dWithCompressor(img_l,
															img_r,
															searchRegion,
															compressor,
															method,
															contiguity,
															preNormalize,
															useSymmetricBarycentricCoordinates);
	} else if (lowercaseMethod == "barycentricsymmetric") {
		bool useSymmetricBarycentricCoordinates = true;
		return computeImageRefinedDisparity2dWithCompressor(img_l,
															img_r,
															searchRegion,
															compressor,
															method,
															contiguity,
															preNormalize,
															useSymmetricBarycentricCoordinates);
	}

	std::stringstream ss;
	ss << "Unsupported interpolation approach " << interpolate << " !";
	throw std::invalid_argument(ss.str());

	return pybind11::array_t<float>();
}


template <StereoVision::Correlation::matchingFunctions matchingFunc,
		  StereoVision::Correlation::dispDirection dispDir>
pybind11::array_t<float> callTypedImgRefDisp(pybind11::array img_l,
											  ArraysFormat l_format,
											  pybind11::array img_r,
											  ArraysFormat r_format,
											  uint32_t width,
											  uint8_t v_radius,
											  uint8_t h_radius,
											  bool preNormalize) {

	if (l_format == T_FLOAT) {

		if (r_format == T_FLOAT) {

			return callRefinedBarycentricDisp<matchingFunc, dispDir, float, float>(img_l, img_r, h_radius, v_radius, width, preNormalize);

		} else if (r_format == T_UINT8) {

			return callRefinedBarycentricDisp<matchingFunc, dispDir, float, uint8_t>(img_l, img_r, h_radius, v_radius, width, preNormalize);

		}

	} else if (l_format == T_UINT8) {

		if (r_format == T_FLOAT) {

			return callRefinedBarycentricDisp<matchingFunc, dispDir, uint8_t, float>(img_l, img_r, h_radius, v_radius, width, preNormalize);

		} else if (r_format == T_UINT8) {

			return callRefinedBarycentricDisp<matchingFunc, dispDir, uint8_t, uint8_t>(img_l, img_r, h_radius, v_radius, width, preNormalize);

		}

	}

	return pybind11::array_t<float>();
}

template<StereoVision::Correlation::dispDirection dispDir>
pybind11::array_t<float> callDirectedImgRefDisp(pybind11::array img_l,
												ArraysFormat l_format,
												pybind11::array img_r,
												ArraysFormat r_format,
												std::string const& method,
												uint32_t width,
												uint8_t v_radius,
												uint8_t h_radius,
												bool preNormalize) {


	if (method == "ncc") {
		return callTypedImgRefDisp<StereoVision::Correlation::matchingFunctions::NCC, dispDir>(
					img_l,
					l_format,
					img_r,
					r_format,
					width,
					v_radius,
					h_radius,
					preNormalize);
	} else if (method == "zncc") {
		return callTypedImgRefDisp<StereoVision::Correlation::matchingFunctions::ZNCC, dispDir>(
					img_l,
					l_format,
					img_r,
					r_format,
					width,
					v_radius,
					h_radius,
					preNormalize);
	} else if (method == "sad") {

		return callTypedImgRefDisp<StereoVision::Correlation::matchingFunctions::SAD, dispDir>(
					img_l,
					l_format,
					img_r,
					r_format,
					width,
					v_radius,
					h_radius,
					preNormalize);

	} else if (method == "zsad") {

		return callTypedImgRefDisp<StereoVision::Correlation::matchingFunctions::ZSAD, dispDir>(
					img_l,
					l_format,
					img_r,
					r_format,
					width,
					v_radius,
					h_radius,
					preNormalize);

	} else if (method == "ssd") {

		return callTypedImgRefDisp<StereoVision::Correlation::matchingFunctions::SSD, dispDir>(
					img_l,
					l_format,
					img_r,
					r_format,
					width,
					v_radius,
					h_radius,
					preNormalize);

	} else if (method == "zssd") {

		return callTypedImgRefDisp<StereoVision::Correlation::matchingFunctions::ZSSD, dispDir>(
					img_l,
					l_format,
					img_r,
					r_format,
					width,
					v_radius,
					h_radius,
					preNormalize);

	} else if (method == "meds") {

		return callTypedImgRefDisp<StereoVision::Correlation::matchingFunctions::MEDAD, dispDir>(
					img_l,
					l_format,
					img_r,
					r_format,
					width,
					v_radius,
					h_radius,
					preNormalize);

	} else if (method == "zmeds") {

		return callTypedImgRefDisp<StereoVision::Correlation::matchingFunctions::ZMEDAD, dispDir>(
					img_l,
					l_format,
					img_r,
					r_format,
					width,
					v_radius,
					h_radius,
					preNormalize);

	}

	return pybind11::array_t<float>();
}

pybind11::array_t<float> computeImageRefinedDisparity(pybind11::array img_l,
													  pybind11::array img_r,
													  uint32_t width,
													  uint8_t v_radius,
													  uint8_t h_radius,
													  std::string const& method,
													  bool preNormalize,
													  std::string const& direction) {

	if (width > 1000) {
		std::stringstream ss;
		ss << "Maximal allowed width is " << 1000 << " !";
		throw std::invalid_argument(ss.str());
	}

	auto toLower = [](unsigned char c){ return std::tolower(c); };

	std::string lowercaseMethod = method;
	std::transform(lowercaseMethod.begin(), lowercaseMethod.end(), lowercaseMethod.begin(), toLower);

	std::string lowercaseDirection = direction;
	std::transform(lowercaseDirection.begin(), lowercaseDirection.end(), lowercaseDirection.begin(), toLower);

	if (lowercaseMethod != "ncc" and
		lowercaseMethod != "sad" and
		lowercaseMethod != "ssd" and
		lowercaseMethod != "meds" and
		lowercaseMethod != "zncc" and
		lowercaseMethod != "zsad" and
		lowercaseMethod != "zssd" and
		lowercaseMethod != "zmeds") {

		std::stringstream ss;
		ss << "Unsupported method " << method << " !";
		throw std::invalid_argument(ss.str());

	}

	if (lowercaseDirection != "r2l" and
		lowercaseDirection != "l2r") {

		std::stringstream ss;
		ss << "Unsupported direction " << direction << " !";
		throw std::invalid_argument(ss.str());

	}

	std::vector<long> l_shape = img_l.request().shape;
	std::vector<long> r_shape = img_r.request().shape;

	std::vector<long> l_strides = img_l.request().strides;
	std::vector<long> r_strides = img_r.request().strides;

	if (l_shape != r_shape) {
		throw std::invalid_argument("computeCostVolume needs arrays of the same shape !");
	}

	if (l_shape.size() != 2 and l_shape.size() != 3) {
		throw std::invalid_argument("computeCostVolume needs arrays with ndims = 2 or 3 !");
	}

	ArraysFormat l_format = formatFromName(img_l.request().format);
	ArraysFormat r_format = formatFromName(img_r.request().format);

	if (l_format == T_INVALID || r_format == T_INVALID) {
		throw std::invalid_argument("computeDisparity uses arrays of either float or uint8 !");
	}

	if (lowercaseDirection == "r2l") {
		return callDirectedImgRefDisp<Correlation::dispDirection::RightToLeft>(
					img_l,
					l_format,
					img_r,
					r_format,
					lowercaseMethod,
					width,
					v_radius,
					h_radius,
					preNormalize
					);
	}

	if (lowercaseDirection == "l2r") {
		return callDirectedImgRefDisp<Correlation::dispDirection::LeftToRight>(
					img_l,
					l_format,
					img_r,
					r_format,
					lowercaseMethod,
					width,
					v_radius,
					h_radius,
					preNormalize
					);
	}

	return pybind11::array_t<float>();

}

template <StereoVision::Correlation::matchingFunctions matchingFunc,
		  Correlation::dispDirection dispDir>
pybind11::array_t<float> callTypedImgRefDisp(pybind11::array img_l,
											  ArraysFormat l_format,
											  pybind11::array img_r,
											  ArraysFormat r_format,
											  uint32_t width,
											  Multidim::Array<int,2> const& compressor_mask,
											  bool preNormalize) {

	if (l_format == T_FLOAT) {

		if (r_format == T_FLOAT) {

			return callRefinedBarycentricDisp<matchingFunc, dispDir, float, float>(img_l, img_r, compressor_mask, width, preNormalize);

		} else if (r_format == T_UINT8) {

			return callRefinedBarycentricDisp<matchingFunc, dispDir, float, uint8_t>(img_l, img_r, compressor_mask, width, preNormalize);

		}

	} else if (l_format == T_UINT8) {

		if (r_format == T_FLOAT) {

			return callRefinedBarycentricDisp<matchingFunc, dispDir, uint8_t, float>(img_l, img_r, compressor_mask, width, preNormalize);

		} else if (r_format == T_UINT8) {

			return callRefinedBarycentricDisp<matchingFunc, dispDir, uint8_t, uint8_t>(img_l, img_r, compressor_mask, width, preNormalize);

		}

	}

	return pybind11::array_t<float>();
}

template<Correlation::dispDirection dispDir>
pybind11::array_t<float> callDirectedImgRefDisp(pybind11::array img_l,
												ArraysFormat l_format,
												pybind11::array img_r,
												ArraysFormat r_format,
												std::string const& method,
												uint32_t width,
												Multidim::Array<int,2> const& compressor_mask,
												bool preNormalize)
{
	if (method == "ncc") {
		return callTypedImgRefDisp<StereoVision::Correlation::matchingFunctions::NCC, dispDir>(
					img_l,
					l_format,
					img_r,
					r_format,
					width,
					compressor_mask,
					preNormalize);

	} else if (method == "zncc") {
		return callTypedImgRefDisp<StereoVision::Correlation::matchingFunctions::ZNCC, dispDir>(
					img_l,
					l_format,
					img_r,
					r_format,
					width,
					compressor_mask,
					preNormalize);

	} else if (method == "sad") {
		return callTypedImgRefDisp<StereoVision::Correlation::matchingFunctions::SAD, dispDir>(
					img_l,
					l_format,
					img_r,
					r_format,
					width,
					compressor_mask,
					preNormalize);

	} else if (method == "zsad") {

		return callTypedImgRefDisp<StereoVision::Correlation::matchingFunctions::ZSAD, dispDir>(
					img_l,
					l_format,
					img_r,
					r_format,
					width,
					compressor_mask,
					preNormalize);

	} else if (method == "ssd") {

		return callTypedImgRefDisp<StereoVision::Correlation::matchingFunctions::SSD, dispDir>(
					img_l,
					l_format,
					img_r,
					r_format,
					width,
					compressor_mask,
					preNormalize);

	} else if (method == "zssd") {

		return callTypedImgRefDisp<StereoVision::Correlation::matchingFunctions::ZSSD, dispDir>(
					img_l,
					l_format,
					img_r,
					r_format,
					width,
					compressor_mask,
					preNormalize);

	} else if (method == "meds") {

		return callTypedImgRefDisp<StereoVision::Correlation::matchingFunctions::MEDAD, dispDir>(
					img_l,
					l_format,
					img_r,
					r_format,
					width,
					compressor_mask,
					preNormalize);

	} else if (method == "zmeds") {

		return callTypedImgRefDisp<StereoVision::Correlation::matchingFunctions::ZMEDAD, dispDir>(
					img_l,
					l_format,
					img_r,
					r_format,
					width,
					compressor_mask,
					preNormalize);

	}

	return pybind11::array_t<float>();
}

pybind11::array_t<float> computeImageRefinedDisparityWithCompressor(pybind11::array img_l,
																	pybind11::array img_r,
																	pybind11::array_t<int> compressor,
																	uint32_t width,
																	std::string const& method,
																	bool preNormalize,
																	std::string const& direction) {

	if (width > 1000) {
		std::stringstream ss;
		ss << "Maximal allowed width is " << 1000 << " !";
		throw std::invalid_argument(ss.str());
	}

	auto toLower = [](unsigned char c){ return std::tolower(c); };

	std::string lowercaseMethod = method;
	std::transform(lowercaseMethod.begin(), lowercaseMethod.end(), lowercaseMethod.begin(), toLower);

	std::string lowercaseDirection = direction;
	std::transform(lowercaseDirection.begin(), lowercaseDirection.end(), lowercaseDirection.begin(), toLower);

	if (lowercaseMethod != "ncc" and
		lowercaseMethod != "sad" and
		lowercaseMethod != "ssd" and
		lowercaseMethod != "meds" and
		lowercaseMethod != "zncc" and
		lowercaseMethod != "zsad" and
		lowercaseMethod != "zssd" and
		lowercaseMethod != "zmeds") {

		std::stringstream ss;
		ss << "Unsupported method " << method << " !";
		throw std::invalid_argument(ss.str());

	}

	if (lowercaseDirection != "r2l" and
		lowercaseDirection != "l2r") {

		std::stringstream ss;
		ss << "Unsupported direction " << direction << " !";
		throw std::invalid_argument(ss.str());

	}


	std::vector<long> l_shape = img_l.request().shape;
	std::vector<long> r_shape = img_r.request().shape;

	std::vector<long> l_strides = img_l.request().strides;
	std::vector<long> r_strides = img_r.request().strides;

	if (l_shape != r_shape) {
		throw std::invalid_argument("computeCostVolume needs arrays of the same shape !");
	}

	if (l_shape.size() != 2 and l_shape.size() != 3) {
		throw std::invalid_argument("computeCostVolume needs arrays with ndims = 2 or 3 !");
	}

	std::vector<long> compressor_shape = compressor.request().shape;

	if (compressor_shape.size() != 2) {
		throw std::invalid_argument("computeCompressedDisparity needs a compressor mask with ndims = 2 or 3 !");
	}

	Multidim::Array<int, 2>::ShapeBlock mask_shape;
	mask_shape[0] = compressor_shape[0];
	mask_shape[1] = compressor_shape[1];

	std::vector<long> compressor_strides = compressor.request().strides;
	Multidim::Array<int, 2>::ShapeBlock mask_strides;
	mask_strides[0] = compressor_strides[0]/sizeof (int);
	mask_strides[1] = compressor_strides[1]/sizeof (int);

	Multidim::Array<int,2> compressor_mask(compressor.mutable_data(), mask_shape, mask_strides);

	ArraysFormat l_format = formatFromName(img_l.request().format);
	ArraysFormat r_format = formatFromName(img_r.request().format);

	if (l_format == T_INVALID || r_format == T_INVALID) {
		throw std::invalid_argument("computeDisparity uses arrays of either float or uint8 !");
	}

	if (lowercaseDirection == "r2l") {
		return callDirectedImgRefDisp<Correlation::dispDirection::RightToLeft>(
					img_l,
					l_format,
					img_r,
					r_format,
					lowercaseMethod,
					width,
					compressor_mask,
					preNormalize
					);
	}

	if (lowercaseDirection == "l2r") {
		return callDirectedImgRefDisp<Correlation::dispDirection::LeftToRight>(
					img_l,
					l_format,
					img_r,
					r_format,
					lowercaseMethod,
					width,
					compressor_mask,
					preNormalize
					);
	}

	return pybind11::array_t<float>();

}

template<Correlation::matchingFunctions matchingFunc, typename T_L, typename T_R>
pybind11::array_t<float, 3> callTypedContiguityImgRefDisp2d(
		pybind11::array img_l,
		pybind11::array img_r,
		Correlation::searchOffset<2> const& searchWindow,
		int v_radius,
		int h_radius,
		StereoVision::Contiguity::bidimensionalContiguity contiguity,
		bool preNormalize,
		bool useSymmetricBarycentricCoordinates) {

	switch (contiguity) {
	case StereoVision::Contiguity::Queen:

		return callRefinedBarycentricDisp2d<matchingFunc, float, float, StereoVision::Contiguity::Queen>
				(img_l,
				 img_r,
				 h_radius,
				 v_radius,
				 searchWindow,
				 preNormalize,
				 useSymmetricBarycentricCoordinates);

	case StereoVision::Contiguity::Rook:

		return callRefinedBarycentricDisp2d<matchingFunc, float, float, StereoVision::Contiguity::Rook>
				(img_l,
				 img_r,
				 h_radius,
				 v_radius,
				 searchWindow,
				 preNormalize,
				 useSymmetricBarycentricCoordinates);
	case StereoVision::Contiguity::Bishop:

		return callRefinedBarycentricDisp2d<matchingFunc, float, float, StereoVision::Contiguity::Bishop>
				(img_l,
				 img_r,
				 h_radius,
				 v_radius,
				 searchWindow,
				 preNormalize,
				 useSymmetricBarycentricCoordinates);
	}

	return pybind11::array_t<float>();

}

template <Correlation::matchingFunctions matchingFunc>
pybind11::array_t<float> callTypedImgRefDisp2d(
		pybind11::array img_l,
		ArraysFormat l_format,
		pybind11::array img_r,
		ArraysFormat r_format,
		Correlation::searchOffset<2> const& searchWindow,
		int v_radius,
		int h_radius,
		StereoVision::Contiguity::bidimensionalContiguity contiguity,
		bool preNormalize,
		bool useSymmetricBarycentricCoordinates) {

	if (l_format == T_FLOAT) {

		if (r_format == T_FLOAT) {

			return callTypedContiguityImgRefDisp2d<matchingFunc, float, float>(
						img_l,
						img_r,
						searchWindow,
						v_radius,
						h_radius,
						contiguity,
						preNormalize,
						useSymmetricBarycentricCoordinates);

		} else if (r_format == T_UINT8) {

			return callTypedContiguityImgRefDisp2d<matchingFunc, float, uint8_t>(
						img_l,
						img_r,
						searchWindow,
						v_radius,
						h_radius,
						contiguity,
						preNormalize,
						useSymmetricBarycentricCoordinates);

		}

	} else if (l_format == T_UINT8) {

		if (r_format == T_FLOAT) {

			return callTypedContiguityImgRefDisp2d<matchingFunc, uint8_t, float>(
						img_l,
						img_r,
						searchWindow,
						v_radius,
						h_radius,
						contiguity,
						preNormalize,
						useSymmetricBarycentricCoordinates);

		} else if (r_format == T_UINT8) {

			return callTypedContiguityImgRefDisp2d<matchingFunc, uint8_t, uint8_t>(
						img_l,
						img_r,
						searchWindow,
						v_radius,
						h_radius,
						contiguity,
						preNormalize,
						useSymmetricBarycentricCoordinates);

		}

	}

	return pybind11::array_t<float>();

}

pybind11::array_t<float> computeImageRefinedDisparity2d(pybind11::array img_l,
														 pybind11::array img_r,
														 std::array<int,4> searchRegion,
														 uint8_t v_radius,
														 uint8_t h_radius,
														 std::string const& method,
														 std::string const& contiguity,
														 bool preNormalize,
														 bool useSymmetricBarycentricCoordinates) {

	for (int searchRadius : searchRegion) {
		constexpr int maxsearchRadius = 1000;
		if (std::abs(searchRadius) > maxsearchRadius) {
			std::stringstream ss;
			ss << "Maximal allowed width is " << maxsearchRadius << " !";
			throw std::invalid_argument(ss.str());
		}
	}

	std::string lowercaseMethod = method;
	std::transform(lowercaseMethod.begin(), lowercaseMethod.end(), lowercaseMethod.begin(),
		[](unsigned char c){ return std::tolower(c); });

	if (lowercaseMethod != "ncc" and
		lowercaseMethod != "sad" and
		lowercaseMethod != "ssd" and
		lowercaseMethod != "meds" and
		lowercaseMethod != "zncc" and
		lowercaseMethod != "zsad" and
		lowercaseMethod != "zssd" and
		lowercaseMethod != "zmeds") {

		std::stringstream ss;
		ss << "Unsupported method " << method << " !";
		throw std::invalid_argument(ss.str());

	}

	std::string lowercaseContiguity = contiguity;
	std::transform(lowercaseContiguity.begin(), lowercaseContiguity.end(), lowercaseContiguity.begin(),
		[](unsigned char c){ return std::tolower(c); });

	if (lowercaseContiguity != "queen" and
		lowercaseContiguity != "rook" and
		lowercaseContiguity != "bishop") {

		std::stringstream ss;
		ss << "Unsupported contiguity pattern " << contiguity << " !";
		throw std::invalid_argument(ss.str());

	}

	StereoVision::Contiguity::bidimensionalContiguity contiguityVal;
	if (lowercaseContiguity == "queen") {
		contiguityVal = StereoVision::Contiguity::Queen;
	}
	if (lowercaseContiguity == "rook") {
		contiguityVal = StereoVision::Contiguity::Rook;
	}
	if (lowercaseContiguity == "bishop") {
		contiguityVal = StereoVision::Contiguity::Bishop;
	}

	Correlation::searchOffset<2> searchWindow(searchRegion[0],searchRegion[1],searchRegion[2],searchRegion[3]);

	std::vector<long> l_shape = img_l.request().shape;
	std::vector<long> r_shape = img_r.request().shape;

	std::vector<long> l_strides = img_l.request().strides;
	std::vector<long> r_strides = img_r.request().strides;

	if (l_shape != r_shape) {
		throw std::invalid_argument("computeCostVolume needs arrays of the same shape !");
	}

	if (l_shape.size() != 2 and l_shape.size() != 3) {
		throw std::invalid_argument("computeCostVolume needs arrays with ndims = 2 or 3 !");
	}

	ArraysFormat l_format = formatFromName(img_l.request().format);
	ArraysFormat r_format = formatFromName(img_r.request().format);

	if (l_format == T_INVALID || r_format == T_INVALID) {
		throw std::invalid_argument("computeDisparity uses arrays of either float or uint8 !");
	}

	if (lowercaseMethod == "ncc") {
		return callTypedImgRefDisp2d<Correlation::matchingFunctions::NCC>(
					img_l,
					l_format,
					img_r,
					r_format,
					searchWindow,
					v_radius,
					h_radius,
					contiguityVal,
					preNormalize,
					useSymmetricBarycentricCoordinates);
	} else if (lowercaseMethod == "zncc") {
		return callTypedImgRefDisp2d<Correlation::matchingFunctions::ZNCC>(
					img_l,
					l_format,
					img_r,
					r_format,
					searchWindow,
					v_radius,
					h_radius,
					contiguityVal,
					preNormalize,
					useSymmetricBarycentricCoordinates);
	} else if (lowercaseMethod == "sad") {

		return callTypedImgRefDisp2d<Correlation::matchingFunctions::SAD>(
					img_l,
					l_format,
					img_r,
					r_format,
					searchWindow,
					v_radius,
					h_radius,
					contiguityVal,
					preNormalize,
					useSymmetricBarycentricCoordinates);

	} else if (lowercaseMethod == "zsad") {

		return callTypedImgRefDisp2d<Correlation::matchingFunctions::ZSAD>(
					img_l,
					l_format,
					img_r,
					r_format,
					searchWindow,
					v_radius,
					h_radius,
					contiguityVal,
					preNormalize,
					useSymmetricBarycentricCoordinates);

	} else if (lowercaseMethod == "ssd") {

		return callTypedImgRefDisp2d<Correlation::matchingFunctions::SSD>(
					img_l,
					l_format,
					img_r,
					r_format,
					searchWindow,
					v_radius,
					h_radius,
					contiguityVal,
					preNormalize,
					useSymmetricBarycentricCoordinates);

	} else if (lowercaseMethod == "zssd") {

		return callTypedImgRefDisp2d<Correlation::matchingFunctions::ZSSD>(
					img_l,
					l_format,
					img_r,
					r_format,
					searchWindow,
					v_radius,
					h_radius,
					contiguityVal,
					preNormalize,
					useSymmetricBarycentricCoordinates);

	} else if (lowercaseMethod == "meds") {

		return callTypedImgRefDisp2d<Correlation::matchingFunctions::MEDAD>(
					img_l,
					l_format,
					img_r,
					r_format,
					searchWindow,
					v_radius,
					h_radius,
					contiguityVal,
					preNormalize,
					useSymmetricBarycentricCoordinates);

	} else if (lowercaseMethod == "zmeds") {

		return callTypedImgRefDisp2d<Correlation::matchingFunctions::ZMEDAD>(
					img_l,
					l_format,
					img_r,
					r_format,
					searchWindow,
					v_radius,
					h_radius,
					contiguityVal,
					preNormalize,
					useSymmetricBarycentricCoordinates);

	}

	return pybind11::array_t<float>();

}

template<Correlation::matchingFunctions matchingFunc, typename T_L, typename T_R>
pybind11::array_t<float, 3> callTypedContiguityImgRefDisp2d(
		pybind11::array img_l,
		pybind11::array img_r,
		Correlation::searchOffset<2> const& searchWindow,
		Multidim::Array<int,2> const& compressor_mask,
		StereoVision::Contiguity::bidimensionalContiguity contiguity,
		bool preNormalize,
		bool useSymmetricBarycentricCoordinates) {

	switch (contiguity) {
	case StereoVision::Contiguity::Queen:

		return callRefinedBarycentricDisp2d<matchingFunc, float, float, StereoVision::Contiguity::Queen>
				(img_l,
				 img_r,
				 compressor_mask,
				 searchWindow,
				 preNormalize,
				 useSymmetricBarycentricCoordinates);

	case StereoVision::Contiguity::Rook:

		return callRefinedBarycentricDisp2d<matchingFunc, float, float, StereoVision::Contiguity::Rook>
				(img_l,
				 img_r,
				 compressor_mask,
				 searchWindow,
				 preNormalize,
				 useSymmetricBarycentricCoordinates);
	case StereoVision::Contiguity::Bishop:

		return callRefinedBarycentricDisp2d<matchingFunc, float, float, StereoVision::Contiguity::Bishop>
				(img_l,
				 img_r,
				 compressor_mask,
				 searchWindow,
				 preNormalize,
				 useSymmetricBarycentricCoordinates);
	}

	return pybind11::array_t<float>();

}

template <Correlation::matchingFunctions matchingFunc>
pybind11::array_t<float, 3> callTypedImgRefDisp2d(
		pybind11::array img_l,
		ArraysFormat l_format,
		pybind11::array img_r,
		ArraysFormat r_format,
		Correlation::searchOffset<2> const& searchWindow,
		Multidim::Array<int,2> const& compressor_mask,
		StereoVision::Contiguity::bidimensionalContiguity contiguity,
		bool preNormalize,
		bool useSymmetricBarycentricCoordinates) {

	if (l_format == T_FLOAT) {

		if (r_format == T_FLOAT) {

			return callTypedContiguityImgRefDisp2d<matchingFunc, float, float>(
						img_l,
						img_r,
						searchWindow,
						compressor_mask,
						contiguity,
						preNormalize,
						useSymmetricBarycentricCoordinates);

		} else if (r_format == T_UINT8) {

			return callTypedContiguityImgRefDisp2d<matchingFunc, float, uint8_t>(
						img_l,
						img_r,
						searchWindow,
						compressor_mask,
						contiguity,
						preNormalize,
						useSymmetricBarycentricCoordinates);

		}

	} else if (l_format == T_UINT8) {

		if (r_format == T_FLOAT) {

			return callTypedContiguityImgRefDisp2d<matchingFunc, uint8_t, float>(
						img_l,
						img_r,
						searchWindow,
						compressor_mask,
						contiguity,
						preNormalize,
						useSymmetricBarycentricCoordinates);

		} else if (r_format == T_UINT8) {

			return callTypedContiguityImgRefDisp2d<matchingFunc, uint8_t, uint8_t>(
						img_l,
						img_r,
						searchWindow,
						compressor_mask,
						contiguity,
						preNormalize,
						useSymmetricBarycentricCoordinates);

		}

	}

	return pybind11::array_t<float>();

}

pybind11::array_t<float> computeImageRefinedDisparity2dWithCompressor(pybind11::array img_l,
																	   pybind11::array img_r,
																	   std::array<int,4> searchRegion,
																	   pybind11::array_t<int> compressor,
																	   std::string const& method,
																	   std::string const& contiguity,
																	   bool preNormalize,
																	   bool useSymmetricBarycentricCoordinates) {

	for (int searchRadius : searchRegion) {
		constexpr int maxsearchRadius = 1000;
		if (std::abs(searchRadius) > maxsearchRadius) {
			std::stringstream ss;
			ss << "Maximal allowed width is " << maxsearchRadius << " !";
			throw std::invalid_argument(ss.str());
		}
	}

	std::string lowercaseMethod = method;
	std::transform(lowercaseMethod.begin(), lowercaseMethod.end(), lowercaseMethod.begin(),
		[](unsigned char c){ return std::tolower(c); });

	if (lowercaseMethod != "ncc" and
		lowercaseMethod != "sad" and
		lowercaseMethod != "ssd" and
		lowercaseMethod != "meds" and
		lowercaseMethod != "zncc" and
		lowercaseMethod != "zsad" and
		lowercaseMethod != "zssd" and
		lowercaseMethod != "zmeds") {

		std::stringstream ss;
		ss << "Unsupported method " << method << " !";
		throw std::invalid_argument(ss.str());

	}

	std::string lowercaseContiguity = contiguity;
	std::transform(lowercaseContiguity.begin(), lowercaseContiguity.end(), lowercaseContiguity.begin(),
		[](unsigned char c){ return std::tolower(c); });

	if (lowercaseContiguity != "queen" and
		lowercaseContiguity != "rook" and
		lowercaseContiguity != "bishop") {

		std::stringstream ss;
		ss << "Unsupported contiguity pattern " << contiguity << " !";
		throw std::invalid_argument(ss.str());

	}

	StereoVision::Contiguity::bidimensionalContiguity contiguityVal;
	if (lowercaseContiguity == "queen") {
		contiguityVal = StereoVision::Contiguity::Queen;
	}
	if (lowercaseContiguity == "rook") {
		contiguityVal = StereoVision::Contiguity::Rook;
	}
	if (lowercaseContiguity == "bishop") {
		contiguityVal = StereoVision::Contiguity::Bishop;
	}


	Correlation::searchOffset<2> searchWindow(searchRegion[0],searchRegion[1],searchRegion[2],searchRegion[3]);

	std::vector<long> l_shape = img_l.request().shape;
	std::vector<long> r_shape = img_r.request().shape;

	std::vector<long> l_strides = img_l.request().strides;
	std::vector<long> r_strides = img_r.request().strides;

	if (l_shape != r_shape) {
		throw std::invalid_argument("computeImageRefinedDisparity2d needs arrays of the same shape !");
	}

	if (l_shape.size() != 2 and l_shape.size() != 3) {
		throw std::invalid_argument("computeImageRefinedDisparity2d needs arrays with ndims = 2 or 3 !");
	}

	std::vector<long> compressor_shape = compressor.request().shape;

	if (compressor_shape.size() != 2) {
		throw std::invalid_argument("computeImageRefinedDisparity2d needs a compressor mask with ndims = 2 or 3 !");
	}

	Multidim::Array<int, 2>::ShapeBlock mask_shape;
	mask_shape[0] = compressor_shape[0];
	mask_shape[1] = compressor_shape[1];

	std::vector<long> compressor_strides = compressor.request().strides;
	Multidim::Array<int, 2>::ShapeBlock mask_strides;
	mask_strides[0] = compressor_strides[0]/sizeof (int);
	mask_strides[1] = compressor_strides[1]/sizeof (int);

	Multidim::Array<int,2> compressor_mask(compressor.mutable_data(), mask_shape, mask_strides);

	ArraysFormat l_format = formatFromName(img_l.request().format);
	ArraysFormat r_format = formatFromName(img_r.request().format);

	if (l_format == T_INVALID || r_format == T_INVALID) {
		throw std::invalid_argument("computeImageRefinedDisparity2d uses arrays of either float or uint8 !");
	}

	if (lowercaseMethod == "ncc") {
		return callTypedImgRefDisp2d<Correlation::matchingFunctions::NCC>(
					img_l,
					l_format,
					img_r,
					r_format,
					searchWindow,
					compressor_mask,
					contiguityVal,
					preNormalize,
					useSymmetricBarycentricCoordinates);
	} else if (lowercaseMethod == "zncc") {
		return callTypedImgRefDisp2d<Correlation::matchingFunctions::ZNCC>(
					img_l,
					l_format,
					img_r,
					r_format,
					searchWindow,
					compressor_mask,
					contiguityVal,
					preNormalize,
					useSymmetricBarycentricCoordinates);
	} else if (lowercaseMethod == "sad") {

		return callTypedImgRefDisp2d<Correlation::matchingFunctions::SAD>(
					img_l,
					l_format,
					img_r,
					r_format,
					searchWindow,
					compressor_mask,
					contiguityVal,
					preNormalize,
					useSymmetricBarycentricCoordinates);

	} else if (lowercaseMethod == "zsad") {

		return callTypedImgRefDisp2d<Correlation::matchingFunctions::ZSAD>(
					img_l,
					l_format,
					img_r,
					r_format,
					searchWindow,
					compressor_mask,
					contiguityVal,
					preNormalize,
					useSymmetricBarycentricCoordinates);

	} else if (lowercaseMethod == "ssd") {

		return callTypedImgRefDisp2d<Correlation::matchingFunctions::SSD>(
					img_l,
					l_format,
					img_r,
					r_format,
					searchWindow,
					compressor_mask,
					contiguityVal,
					preNormalize,
					useSymmetricBarycentricCoordinates);

	} else if (lowercaseMethod == "zssd") {

		return callTypedImgRefDisp2d<Correlation::matchingFunctions::ZSSD>(
					img_l,
					l_format,
					img_r,
					r_format,
					searchWindow,
					compressor_mask,
					contiguityVal,
					preNormalize,
					useSymmetricBarycentricCoordinates);

	} else if (lowercaseMethod == "meds") {

		return callTypedImgRefDisp2d<Correlation::matchingFunctions::MEDAD>(
					img_l,
					l_format,
					img_r,
					r_format,
					searchWindow,
					compressor_mask,
					contiguityVal,
					preNormalize,
					useSymmetricBarycentricCoordinates);

	} else if (lowercaseMethod == "zmeds") {

		return callTypedImgRefDisp2d<Correlation::matchingFunctions::ZMEDAD>(
					img_l,
					l_format,
					img_r,
					r_format,
					searchWindow,
					compressor_mask,
					contiguityVal,
					preNormalize,
					useSymmetricBarycentricCoordinates);

	}

	return pybind11::array_t<float>();

}

template <StereoVision::Correlation::matchingFunctions matchingFunc,
		  Correlation::dispDirection dispDir>
pybind11::array_t<float> callTypedBarRefDisp(pybind11::array img_l,
											  ArraysFormat l_format,
											  pybind11::array img_r,
											  ArraysFormat r_format,
											  uint32_t width,
											  uint8_t v_radius,
											  uint8_t h_radius,
											  bool preNormalize) {

	if (l_format == T_FLOAT) {

		if (r_format == T_FLOAT) {

			return callRefinedBarycentricSymmetricDisp<matchingFunc, dispDir, float, float>(img_l, img_r, h_radius, v_radius, width, preNormalize);

		} else if (r_format == T_UINT8) {

			return callRefinedBarycentricSymmetricDisp<matchingFunc, dispDir, float, uint8_t>(img_l, img_r, h_radius, v_radius, width, preNormalize);

		}

	} else if (l_format == T_UINT8) {

		if (r_format == T_FLOAT) {

			return callRefinedBarycentricSymmetricDisp<matchingFunc, dispDir, uint8_t, float>(img_l, img_r, h_radius, v_radius, width, preNormalize);

		} else if (r_format == T_UINT8) {

			return callRefinedBarycentricSymmetricDisp<matchingFunc, dispDir, uint8_t, uint8_t>(img_l, img_r, h_radius, v_radius, width, preNormalize);

		}

	}

	return pybind11::array_t<float>();
}

template<Correlation::dispDirection dispDir>
pybind11::array_t<float> callDirectedBarRefDisp(pybind11::array img_l,
												ArraysFormat l_format,
												pybind11::array img_r,
												ArraysFormat r_format,
												std::string const& method,
												uint32_t width,
												uint8_t v_radius,
												uint8_t h_radius,
												bool preNormalize)
{
	if (method == "ncc") {
		return callTypedBarRefDisp<StereoVision::Correlation::matchingFunctions::NCC, dispDir>(
					img_l,
					l_format,
					img_r,
					r_format,
					width,
					v_radius,
					h_radius,
					preNormalize);
	} else if (method == "zncc") {
		return callTypedBarRefDisp<StereoVision::Correlation::matchingFunctions::ZNCC, dispDir>(
					img_l,
					l_format,
					img_r,
					r_format,
					width,
					v_radius,
					h_radius,
					preNormalize);
	} else if (method == "sad") {

		return callTypedBarRefDisp<StereoVision::Correlation::matchingFunctions::SAD, dispDir>(
					img_l,
					l_format,
					img_r,
					r_format,
					width,
					v_radius,
					h_radius,
					preNormalize);

	} else if (method == "zsad") {

		return callTypedBarRefDisp<StereoVision::Correlation::matchingFunctions::ZSAD, dispDir>(
					img_l,
					l_format,
					img_r,
					r_format,
					width,
					v_radius,
					h_radius,
					preNormalize);

	} else if (method == "ssd") {

		return callTypedBarRefDisp<StereoVision::Correlation::matchingFunctions::SSD, dispDir>(
					img_l,
					l_format,
					img_r,
					r_format,
					width,
					v_radius,
					h_radius,
					preNormalize);

	} else if (method == "zssd") {

		return callTypedBarRefDisp<StereoVision::Correlation::matchingFunctions::ZSSD, dispDir>(
					img_l,
					l_format,
					img_r,
					r_format,
					width,
					v_radius,
					h_radius,
					preNormalize);

	} else if (method == "meds") {

		return callTypedBarRefDisp<StereoVision::Correlation::matchingFunctions::MEDAD, dispDir>(
					img_l,
					l_format,
					img_r,
					r_format,
					width,
					v_radius,
					h_radius,
					preNormalize);

	} else if (method == "zmeds") {

		return callTypedBarRefDisp<StereoVision::Correlation::matchingFunctions::ZMEDAD, dispDir>(
					img_l,
					l_format,
					img_r,
					r_format,
					width,
					v_radius,
					h_radius,
					preNormalize);

	}

	return pybind11::array_t<float>();
}

pybind11::array_t<float> computeBarycentricImageRefinedDisparity(pybind11::array img_l,
																 pybind11::array img_r,
																 uint32_t width,
																 uint8_t v_radius,
																 uint8_t h_radius,
																 std::string const& method,
																 bool preNormalize,
																 std::string const& direction) {

	if (width > 1000) {
		std::stringstream ss;
		ss << "Maximal allowed width is " << 1000 << " !";
		throw std::invalid_argument(ss.str());
	}

	auto toLower = [](unsigned char c){ return std::tolower(c); };

	std::string lowercaseMethod = method;
	std::transform(lowercaseMethod.begin(), lowercaseMethod.end(), lowercaseMethod.begin(), toLower);

	std::string lowercaseDirection = direction;
	std::transform(lowercaseDirection.begin(), lowercaseDirection.end(), lowercaseDirection.begin(), toLower);

	if (lowercaseMethod != "ncc" and
		lowercaseMethod != "sad" and
		lowercaseMethod != "ssd" and
		lowercaseMethod != "meds" and
		lowercaseMethod != "zncc" and
		lowercaseMethod != "zsad" and
		lowercaseMethod != "zssd" and
		lowercaseMethod != "zmeds") {

		std::stringstream ss;
		ss << "Unsupported method " << method << " !";
		throw std::invalid_argument(ss.str());

	}

	if (lowercaseDirection != "r2l" and
		lowercaseDirection != "l2r") {

		std::stringstream ss;
		ss << "Unsupported direction " << direction << " !";
		throw std::invalid_argument(ss.str());

	}

	std::vector<long> l_shape = img_l.request().shape;
	std::vector<long> r_shape = img_r.request().shape;

	std::vector<long> l_strides = img_l.request().strides;
	std::vector<long> r_strides = img_r.request().strides;

	if (l_shape != r_shape) {
		throw std::invalid_argument("computeCostVolume needs arrays of the same shape !");
	}

	if (l_shape.size() != 2 and l_shape.size() != 3) {
		throw std::invalid_argument("computeCostVolume needs arrays with ndims = 2 or 3 !");
	}

	ArraysFormat l_format = formatFromName(img_l.request().format);
	ArraysFormat r_format = formatFromName(img_r.request().format);

	if (l_format == T_INVALID || r_format == T_INVALID) {
		throw std::invalid_argument("computeDisparity uses arrays of either float or uint8 !");
	}

	if (lowercaseDirection == "l2r") {
		return callDirectedBarRefDisp<Correlation::dispDirection::LeftToRight>(
					img_l,
					l_format,
					img_r,
					r_format,
					lowercaseMethod,
					width,
					v_radius,
					h_radius,
					preNormalize
					);
	}

	if (lowercaseDirection == "r2l") {
		return callDirectedBarRefDisp<Correlation::dispDirection::RightToLeft>(
					img_l,
					l_format,
					img_r,
					r_format,
					lowercaseMethod,
					width,
					v_radius,
					h_radius,
					preNormalize
					);
	}

	return pybind11::array_t<float>();

}

template <StereoVision::Correlation::matchingFunctions matchingFunc,
		  Correlation::dispDirection dispDir>
pybind11::array_t<float> callTypedBarRefDisp(pybind11::array img_l,
											  ArraysFormat l_format,
											  pybind11::array img_r,
											  ArraysFormat r_format,
											  uint32_t width,
											  Multidim::Array<int,2> const& compressor_mask,
											  bool preNormalize) {

	if (l_format == T_FLOAT) {

		if (r_format == T_FLOAT) {

			return callRefinedBarycentricSymmetricDisp<matchingFunc, dispDir, float, float>(img_l, img_r, compressor_mask, width, preNormalize);

		} else if (r_format == T_UINT8) {

			return callRefinedBarycentricSymmetricDisp<matchingFunc, dispDir, float, uint8_t>(img_l, img_r, compressor_mask, width, preNormalize);

		}

	} else if (l_format == T_UINT8) {

		if (r_format == T_FLOAT) {

			return callRefinedBarycentricSymmetricDisp<matchingFunc, dispDir, uint8_t, float>(img_l, img_r, compressor_mask, width, preNormalize);

		} else if (r_format == T_UINT8) {

			return callRefinedBarycentricSymmetricDisp<matchingFunc, dispDir, uint8_t, uint8_t>(img_l, img_r, compressor_mask, width, preNormalize);

		}

	}

	return pybind11::array_t<float>();
}

template<Correlation::dispDirection dispDir>
pybind11::array_t<float> callDirectedBarRefDisp(pybind11::array img_l,
												ArraysFormat l_format,
												pybind11::array img_r,
												ArraysFormat r_format,
												std::string const& method,
												uint32_t width,
												Multidim::Array<int,2> const& compressor_mask,
												bool preNormalize)
{
	if (method == "ncc") {
		return callTypedBarRefDisp<StereoVision::Correlation::matchingFunctions::NCC, dispDir>(
					img_l,
					l_format,
					img_r,
					r_format,
					width,
					compressor_mask,
					preNormalize);
	} else if (method == "zncc") {
		return callTypedBarRefDisp<StereoVision::Correlation::matchingFunctions::ZNCC, dispDir>(
					img_l,
					l_format,
					img_r,
					r_format,
					width,
					compressor_mask,
					preNormalize);
	} else if (method == "sad") {

		return callTypedBarRefDisp<StereoVision::Correlation::matchingFunctions::SAD, dispDir>(
					img_l,
					l_format,
					img_r,
					r_format,
					width,
					compressor_mask,
					preNormalize);

	} else if (method == "zsad") {

		return callTypedBarRefDisp<StereoVision::Correlation::matchingFunctions::ZSAD, dispDir>(
					img_l,
					l_format,
					img_r,
					r_format,
					width,
					compressor_mask,
					preNormalize);

	} else if (method == "ssd") {

		return callTypedBarRefDisp<StereoVision::Correlation::matchingFunctions::SSD, dispDir>(
					img_l,
					l_format,
					img_r,
					r_format,
					width,
					compressor_mask,
					preNormalize);

	} else if (method == "zssd") {

		return callTypedBarRefDisp<StereoVision::Correlation::matchingFunctions::ZSSD, dispDir>(
					img_l,
					l_format,
					img_r,
					r_format,
					width,
					compressor_mask,
					preNormalize);

	} else if (method == "meds") {

		return callTypedBarRefDisp<StereoVision::Correlation::matchingFunctions::MEDAD, dispDir>(
					img_l,
					l_format,
					img_r,
					r_format,
					width,
					compressor_mask,
					preNormalize);

	} else if (method == "zmeds") {

		return callTypedBarRefDisp<StereoVision::Correlation::matchingFunctions::ZMEDAD, dispDir>(
					img_l,
					l_format,
					img_r,
					r_format,
					width,
					compressor_mask,
					preNormalize);

	}

	return pybind11::array_t<float>();
}

pybind11::array_t<float> computeBarycentricImageRefinedDisparityWithCompressor(pybind11::array img_l,
																				pybind11::array img_r,
																				pybind11::array_t<int> compressor,
																				uint32_t width,
																				std::string const& method,
																				bool preNormalize,
																			   std::string const& direction) {

	if (width > 1000) {
		std::stringstream ss;
		ss << "Maximal allowed width is " << 1000 << " !";
		throw std::invalid_argument(ss.str());
	}

	auto toLower = [](unsigned char c){ return std::tolower(c); };

	std::string lowercaseMethod = method;
	std::transform(lowercaseMethod.begin(), lowercaseMethod.end(), lowercaseMethod.begin(), toLower);

	std::string lowercaseDirection = direction;
	std::transform(lowercaseDirection.begin(), lowercaseDirection.end(), lowercaseDirection.begin(), toLower);

	if (lowercaseMethod != "ncc" and
		lowercaseMethod != "sad" and
		lowercaseMethod != "ssd" and
		lowercaseMethod != "meds" and
		lowercaseMethod != "zncc" and
		lowercaseMethod != "zsad" and
		lowercaseMethod != "zssd" and
		lowercaseMethod != "zmeds") {

		std::stringstream ss;
		ss << "Unsupported method " << method << " !";
		throw std::invalid_argument(ss.str());

	}

	if (lowercaseDirection != "l2r" and
		lowercaseDirection != "r2l") {

		std::stringstream ss;
		ss << "Unsupported direction " << direction << " !";
		throw std::invalid_argument(ss.str());

	}

	std::vector<long> l_shape = img_l.request().shape;
	std::vector<long> r_shape = img_r.request().shape;

	std::vector<long> l_strides = img_l.request().strides;
	std::vector<long> r_strides = img_r.request().strides;

	if (l_shape != r_shape) {
		throw std::invalid_argument("computeCostVolume needs arrays of the same shape !");
	}

	if (l_shape.size() != 2 and l_shape.size() != 3) {
		throw std::invalid_argument("computeCostVolume needs arrays with ndims = 2 or 3 !");
	}

	std::vector<long> compressor_shape = compressor.request().shape;

	if (compressor_shape.size() != 2) {
		throw std::invalid_argument("computeCompressedDisparity needs a compressor mask with ndims = 2 or 3 !");
	}

	Multidim::Array<int, 2>::ShapeBlock mask_shape;
	mask_shape[0] = compressor_shape[0];
	mask_shape[1] = compressor_shape[1];

	std::vector<long> compressor_strides = compressor.request().strides;
	Multidim::Array<int, 2>::ShapeBlock mask_strides;
	mask_strides[0] = compressor_strides[0]/sizeof (int);
	mask_strides[1] = compressor_strides[1]/sizeof (int);

	Multidim::Array<int,2> compressor_mask(compressor.mutable_data(), mask_shape, mask_strides);

	ArraysFormat l_format = formatFromName(img_l.request().format);
	ArraysFormat r_format = formatFromName(img_r.request().format);

	if (l_format == T_INVALID || r_format == T_INVALID) {
		throw std::invalid_argument("computeDisparity uses arrays of either float or uint8 !");
	}

	if (lowercaseDirection == "r2l") {
		return callDirectedBarRefDisp<Correlation::dispDirection::RightToLeft>(
					img_l,
					l_format,
					img_r,
					r_format,
					lowercaseMethod,
					width,
					compressor_mask,
					preNormalize
					);
	}

	if (lowercaseDirection == "l2r") {
		return callDirectedBarRefDisp<Correlation::dispDirection::LeftToRight>(
					img_l,
					l_format,
					img_r,
					r_format,
					lowercaseMethod,
					width,
					compressor_mask,
					preNormalize
					);
	}

	return pybind11::array_t<float>();

}

template <StereoVision::Correlation::matchingFunctions matchingFunc,
		  Correlation::dispDirection dispDir>
pybind11::array_t<float> callTypedCostRefDisp(pybind11::array img_l,
											  ArraysFormat l_format,
											  pybind11::array img_r,
											  ArraysFormat r_format,
											  uint32_t width,
											  uint8_t v_radius,
											  uint8_t h_radius,
											  bool preNormalize) {

	if (l_format == T_FLOAT) {

		if (r_format == T_FLOAT) {

			return callRefinedCostSymmetricDisp<matchingFunc, dispDir, float, float>(img_l, img_r, h_radius, v_radius, width, preNormalize);

		} else if (r_format == T_UINT8) {

			return callRefinedCostSymmetricDisp<matchingFunc, dispDir, float, uint8_t>(img_l, img_r, h_radius, v_radius, width, preNormalize);

		}

	} else if (l_format == T_UINT8) {

		if (r_format == T_FLOAT) {

			return callRefinedCostSymmetricDisp<matchingFunc, dispDir, uint8_t, float>(img_l, img_r, h_radius, v_radius, width, preNormalize);

		} else if (r_format == T_UINT8) {

			return callRefinedCostSymmetricDisp<matchingFunc, dispDir, uint8_t, uint8_t>(img_l, img_r, h_radius, v_radius, width, preNormalize);

		}

	}

	return pybind11::array_t<float>();
}

template<Correlation::dispDirection dispDir>
pybind11::array_t<float> callDirectedCostRefDisp(pybind11::array img_l,
												 ArraysFormat l_format,
												 pybind11::array img_r,
												 ArraysFormat r_format,
												 std::string const& method,
												 uint32_t width,
												 uint8_t v_radius,
												 uint8_t h_radius,
												 bool preNormalize)
{
	if (method == "ncc") {
		return callTypedCostRefDisp<StereoVision::Correlation::matchingFunctions::NCC, dispDir>(
					img_l,
					l_format,
					img_r,
					r_format,
					width,
					v_radius,
					h_radius,
					preNormalize);
	} else if (method == "zncc") {
		return callTypedCostRefDisp<StereoVision::Correlation::matchingFunctions::ZNCC, dispDir>(
					img_l,
					l_format,
					img_r,
					r_format,
					width,
					v_radius,
					h_radius,
					preNormalize);
	} else if (method == "sad") {

		return callTypedCostRefDisp<StereoVision::Correlation::matchingFunctions::SAD, dispDir>(
					img_l,
					l_format,
					img_r,
					r_format,
					width,
					v_radius,
					h_radius,
					preNormalize);

	} else if (method == "zsad") {

		return callTypedCostRefDisp<StereoVision::Correlation::matchingFunctions::ZSAD, dispDir>(
					img_l,
					l_format,
					img_r,
					r_format,
					width,
					v_radius,
					h_radius,
					preNormalize);

	} else if (method == "ssd") {

		return callTypedCostRefDisp<StereoVision::Correlation::matchingFunctions::SSD, dispDir>(
					img_l,
					l_format,
					img_r,
					r_format,
					width,
					v_radius,
					h_radius,
					preNormalize);

	} else if (method == "zssd") {

		return callTypedCostRefDisp<StereoVision::Correlation::matchingFunctions::ZSSD, dispDir>(
					img_l,
					l_format,
					img_r,
					r_format,
					width,
					v_radius,
					h_radius,
					preNormalize);

	} else if (method == "meds") {

		return callTypedCostRefDisp<StereoVision::Correlation::matchingFunctions::MEDAD, dispDir>(
					img_l,
					l_format,
					img_r,
					r_format,
					width,
					v_radius,
					h_radius,
					preNormalize);

	} else if (method == "zmeds") {

		return callTypedCostRefDisp<StereoVision::Correlation::matchingFunctions::ZMEDAD, dispDir>(
					img_l,
					l_format,
					img_r,
					r_format,
					width,
					v_radius,
					h_radius,
					preNormalize);

	}

	return pybind11::array_t<float>();
}

pybind11::array_t<float> computeSymmetricRefinedDisparity(pybind11::array img_l,
														  pybind11::array img_r,
														  uint32_t width,
														  uint8_t v_radius,
														  uint8_t h_radius,
														  std::string const& method,
														  bool preNormalize,
														  std::string const& direction) {

	if (width > 1000) {
		std::stringstream ss;
		ss << "Maximal allowed width is " << 1000 << " !";
		throw std::invalid_argument(ss.str());
	}

	auto toLower = [](unsigned char c){ return std::tolower(c); };

	std::string lowercaseMethod = method;
	std::transform(lowercaseMethod.begin(), lowercaseMethod.end(), lowercaseMethod.begin(), toLower);

	std::string lowercaseDirection = direction;
	std::transform(lowercaseDirection.begin(), lowercaseDirection.end(), lowercaseDirection.begin(), toLower);

	if (lowercaseMethod != "ncc" and
		lowercaseMethod != "sad" and
		lowercaseMethod != "ssd" and
		lowercaseMethod != "meds" and
		lowercaseMethod != "zncc" and
		lowercaseMethod != "zsad" and
		lowercaseMethod != "zssd" and
		lowercaseMethod != "zmeds") {

		std::stringstream ss;
		ss << "Unsupported method " << method << " !";
		throw std::invalid_argument(ss.str());

	}

	if (lowercaseDirection != "r2l" and
		lowercaseDirection != "l2r") {

		std::stringstream ss;
		ss << "Unsupported direction " << direction << " !";
		throw std::invalid_argument(ss.str());

	}

	std::vector<long> l_shape = img_l.request().shape;
	std::vector<long> r_shape = img_r.request().shape;

	std::vector<long> l_strides = img_l.request().strides;
	std::vector<long> r_strides = img_r.request().strides;

	if (l_shape != r_shape) {
		throw std::invalid_argument("computeCostVolume needs arrays of the same shape !");
	}

	if (l_shape.size() != 2 and l_shape.size() != 3) {
		throw std::invalid_argument("computeCostVolume needs arrays with ndims = 2 or 3 !");
	}

	ArraysFormat l_format = formatFromName(img_l.request().format);
	ArraysFormat r_format = formatFromName(img_r.request().format);

	if (l_format == T_INVALID || r_format == T_INVALID) {
		throw std::invalid_argument("computeDisparity uses arrays of either float or uint8 !");
	}

	if (lowercaseDirection == "r2l") {
		return callDirectedCostRefDisp<Correlation::dispDirection::RightToLeft>(
					img_l,
					l_format,
					img_r,
					r_format,
					lowercaseMethod,
					width,
					v_radius,
					h_radius,
					preNormalize
					);
	}

	if (lowercaseDirection == "l2r") {
		return callDirectedCostRefDisp<Correlation::dispDirection::LeftToRight>(
					img_l,
					l_format,
					img_r,
					r_format,
					lowercaseMethod,
					width,
					v_radius,
					h_radius,
					preNormalize
					);
	}

	return pybind11::array_t<float>();

}

template <StereoVision::Correlation::matchingFunctions matchingFunc,
		  Correlation::dispDirection dispDir>
pybind11::array_t<float> callTypedCostRefDisp(pybind11::array img_l,
											  ArraysFormat l_format,
											  pybind11::array img_r,
											  ArraysFormat r_format,
											  uint32_t width,
											  Multidim::Array<int,2> const& compressor_mask,
											  bool preNormalize) {

	if (l_format == T_FLOAT) {

		if (r_format == T_FLOAT) {

			return callRefinedCostSymmetricDisp<matchingFunc, dispDir, float, float>(img_l, img_r, compressor_mask, width, preNormalize);

		} else if (r_format == T_UINT8) {

			return callRefinedCostSymmetricDisp<matchingFunc, dispDir, float, uint8_t>(img_l, img_r, compressor_mask, width, preNormalize);

		}

	} else if (l_format == T_UINT8) {

		if (r_format == T_FLOAT) {

			return callRefinedCostSymmetricDisp<matchingFunc, dispDir, uint8_t, float>(img_l, img_r, compressor_mask, width, preNormalize);

		} else if (r_format == T_UINT8) {

			return callRefinedCostSymmetricDisp<matchingFunc, dispDir, uint8_t, uint8_t>(img_l, img_r, compressor_mask, width, preNormalize);

		}

	}

	return pybind11::array_t<float>();
}

template<Correlation::dispDirection dispDir>
pybind11::array_t<float> callDirectedCostRefDisp(pybind11::array img_l,
												 ArraysFormat l_format,
												 pybind11::array img_r,
												 ArraysFormat r_format,
												 std::string const& method,
												 uint32_t width,
												 Multidim::Array<int,2> const& compressor_mask,
												 bool preNormalize)
{

	if (method == "ncc") {
		return callTypedCostRefDisp<StereoVision::Correlation::matchingFunctions::NCC, dispDir>(
					img_l,
					l_format,
					img_r,
					r_format,
					width,
					compressor_mask,
					preNormalize);
	} else if (method == "zncc") {
		return callTypedCostRefDisp<StereoVision::Correlation::matchingFunctions::ZNCC, dispDir>(
					img_l,
					l_format,
					img_r,
					r_format,
					width,
					compressor_mask,
					preNormalize);
	} else if (method == "sad") {

		return callTypedCostRefDisp<StereoVision::Correlation::matchingFunctions::SAD, dispDir>(
					img_l,
					l_format,
					img_r,
					r_format,
					width,
					compressor_mask,
					preNormalize);

	} else if (method == "zsad") {

		return callTypedCostRefDisp<StereoVision::Correlation::matchingFunctions::ZSAD, dispDir>(
					img_l,
					l_format,
					img_r,
					r_format,
					width,
					compressor_mask,
					preNormalize);

	} else if (method == "ssd") {

		return callTypedCostRefDisp<StereoVision::Correlation::matchingFunctions::SSD, dispDir>(
					img_l,
					l_format,
					img_r,
					r_format,
					width,
					compressor_mask,
					preNormalize);

	} else if (method == "zssd") {

		return callTypedCostRefDisp<StereoVision::Correlation::matchingFunctions::ZSSD, dispDir>(
					img_l,
					l_format,
					img_r,
					r_format,
					width,
					compressor_mask,
					preNormalize);

	} else if (method == "meds") {

		return callTypedCostRefDisp<StereoVision::Correlation::matchingFunctions::MEDAD, dispDir>(
					img_l,
					l_format,
					img_r,
					r_format,
					width,
					compressor_mask,
					preNormalize);

	} else if (method == "zmeds") {

		return callTypedCostRefDisp<StereoVision::Correlation::matchingFunctions::ZMEDAD, dispDir>(
					img_l,
					l_format,
					img_r,
					r_format,
					width,
					compressor_mask,
					preNormalize);

	}

	return pybind11::array_t<float>();

}

pybind11::array_t<float> computeSymmetricRefinedDisparityWithCompressor(pybind11::array img_l,
																		 pybind11::array img_r,
																		 pybind11::array_t<int> compressor,
																		 uint32_t width,
																		 std::string const& method,
																		 bool preNormalize,
																		std::string const& direction) {

	if (width > 1000) {
		std::stringstream ss;
		ss << "Maximal allowed width is " << 1000 << " !";
		throw std::invalid_argument(ss.str());
	}

	auto toLower = [](unsigned char c){ return std::tolower(c); };

	std::string lowercaseMethod = method;
	std::transform(lowercaseMethod.begin(), lowercaseMethod.end(), lowercaseMethod.begin(), toLower);

	std::string lowercaseDirection = direction;
	std::transform(lowercaseDirection.begin(), lowercaseDirection.end(), lowercaseDirection.begin(), toLower);

	if (lowercaseMethod != "ncc" and
		lowercaseMethod != "sad" and
		lowercaseMethod != "ssd" and
		lowercaseMethod != "meds" and
		lowercaseMethod != "zncc" and
		lowercaseMethod != "zsad" and
		lowercaseMethod != "zssd" and
		lowercaseMethod != "zmeds") {

		std::stringstream ss;
		ss << "Unsupported method " << method << " !";
		throw std::invalid_argument(ss.str());

	}

	if (lowercaseDirection != "r2l" and
		lowercaseDirection != "l2r") {

		std::stringstream ss;
		ss << "Unsupported direction " << direction << " !";
		throw std::invalid_argument(ss.str());

	}

	std::vector<long> l_shape = img_l.request().shape;
	std::vector<long> r_shape = img_r.request().shape;

	std::vector<long> l_strides = img_l.request().strides;
	std::vector<long> r_strides = img_r.request().strides;

	if (l_shape != r_shape) {
		throw std::invalid_argument("computeCostVolume needs arrays of the same shape !");
	}

	if (l_shape.size() != 2 and l_shape.size() != 3) {
		throw std::invalid_argument("computeCostVolume needs arrays with ndims = 2 or 3 !");
	}

	std::vector<long> compressor_shape = compressor.request().shape;

	if (compressor_shape.size() != 2) {
		throw std::invalid_argument("computeCompressedDisparity needs a compressor mask with ndims = 2 or 3 !");
	}

	Multidim::Array<int, 2>::ShapeBlock mask_shape;
	mask_shape[0] = compressor_shape[0];
	mask_shape[1] = compressor_shape[1];

	std::vector<long> compressor_strides = compressor.request().strides;
	Multidim::Array<int, 2>::ShapeBlock mask_strides;
	mask_strides[0] = compressor_strides[0]/sizeof (int);
	mask_strides[1] = compressor_strides[1]/sizeof (int);

	Multidim::Array<int,2> compressor_mask(compressor.mutable_data(), mask_shape, mask_strides);

	ArraysFormat l_format = formatFromName(img_l.request().format);
	ArraysFormat r_format = formatFromName(img_r.request().format);

	if (l_format == T_INVALID || r_format == T_INVALID) {
		throw std::invalid_argument("computeDisparity uses arrays of either float or uint8 !");
	}

	if (lowercaseDirection == "r2l") {
		return callDirectedCostRefDisp<Correlation::dispDirection::RightToLeft>(
					img_l,
					l_format,
					img_r,
					r_format,
					lowercaseMethod,
					width,
					compressor_mask,
					preNormalize
					);
	}

	if (lowercaseDirection == "l2r") {
		return callDirectedCostRefDisp<Correlation::dispDirection::LeftToRight>(
					img_l,
					l_format,
					img_r,
					r_format,
					lowercaseMethod,
					width,
					compressor_mask,
					preNormalize
					);
	}

	return pybind11::array_t<float>();


}

template <class T_L, class T_R>
inline float getProportionCoveredImpl(pybind11::array & disp_l,
									  pybind11::array & disp_r) {

	auto shape_l = disp_l.request().shape;
	auto strides_l = disp_l.request().strides;

	auto shape_r = disp_r.request().shape;
	auto strides_r = disp_r.request().strides;

	return StereoVision::Statistics::computeCoveringProportion
			<Correlation::dispDirection::RightToLeft>
			(
				Multidim::Array<T_L,2>(static_cast<T_L*>(disp_l.mutable_data()),
									   {static_cast<int>(shape_l[0]), static_cast<int>(shape_l[1])},
									   {static_cast<int>(strides_l[0]/sizeof (T_L)), static_cast<int>(strides_l[1]/sizeof (T_L))}),
				Multidim::Array<T_R,2>(static_cast<T_R*>(disp_r.mutable_data()),
									   {static_cast<int>(shape_r[0]), static_cast<int>(shape_r[1])},
									   {static_cast<int>(strides_r[0]/sizeof (T_R)), static_cast<int>(strides_r[1]/sizeof (T_R))})
			);

}

template <class T_L>
inline float getProportionCoveredImplIntrm(pybind11::array & disp_l,
										   pybind11::array & disp_r,
										   DispFormat r_format) {

	switch (r_format) {
	case T_DISP_FLOAT:
		return getProportionCoveredImpl<T_L, float>(disp_l, disp_r);
	case T_DISP_INT8:
		return getProportionCoveredImpl<T_L, int8_t>(disp_l, disp_r);
	case T_DISP_INT32:
		return getProportionCoveredImpl<T_L, int32_t>(disp_l, disp_r);
	default:
		return -1;
	}

}


float getProportionCovered(pybind11::array disp_l,
						   pybind11::array disp_r) {

	DispFormat l_format = dispFormatFromName(disp_l.request().format);
	DispFormat r_format = dispFormatFromName(disp_r.request().format);

	if (l_format == T_DISP_INVALID || r_format == T_DISP_INVALID) {
		throw std::invalid_argument("getProportionCovered can use disparities of either float or int8 or int32 !");
	}

	std::vector<long> l_shape = disp_l.request().shape;
	std::vector<long> r_shape = disp_r.request().shape;

	if (l_shape != r_shape) {
		throw std::invalid_argument("getProportionCovered needs arrays of the same shape !");
	}

	if (l_shape.size() != 2) {
		throw std::invalid_argument("getProportionCovered needs arrays with ndims = 2 !");
	}

	switch (l_format) {
	case T_DISP_FLOAT:
		return getProportionCoveredImplIntrm<float>(disp_l, disp_r, r_format);
	case T_DISP_INT8:
		return getProportionCoveredImplIntrm<int8_t>(disp_l, disp_r, r_format);
	case T_DISP_INT32:
		return getProportionCoveredImplIntrm<int32_t>(disp_l, disp_r, r_format);
	default:
		return -1;
	}

}

template <class T_L, class T_R>
inline Multidim::Array<float, 2> getCoverMaskImpl(pybind11::array & disp_l,
												  pybind11::array & disp_r) {

	auto shape_l = disp_l.request().shape;
	auto strides_l = disp_l.request().strides;

	auto shape_r = disp_r.request().shape;
	auto strides_r = disp_r.request().strides;

	return StereoVision::Statistics::computeCovering
			<Correlation::dispDirection::RightToLeft>
			(
				Multidim::Array<T_L,2>(static_cast<T_L*>(disp_l.mutable_data()),
									   {static_cast<int>(shape_l[0]), static_cast<int>(shape_l[1])},
									   {static_cast<int>(strides_l[0]/sizeof (T_L)), static_cast<int>(strides_l[1]/sizeof (T_L))}),
				Multidim::Array<T_R,2>(static_cast<T_R*>(disp_r.mutable_data()),
									   {static_cast<int>(shape_r[0]), static_cast<int>(shape_r[1])},
									   {static_cast<int>(strides_r[0]/sizeof (T_R)), static_cast<int>(strides_r[1]/sizeof (T_R))})
			);

}

template <class T_L>
inline Multidim::Array<float, 2> getCoverMaskImplIntrm(pybind11::array & disp_l,
													   pybind11::array & disp_r,
													   DispFormat r_format) {

	switch (r_format) {
	case T_DISP_FLOAT:
		return getCoverMaskImpl<T_L, float>(disp_l, disp_r);
	case T_DISP_INT8:
		return getCoverMaskImpl<T_L, int8_t>(disp_l, disp_r);
	case T_DISP_INT32:
		return getCoverMaskImpl<T_L, int32_t>(disp_l, disp_r);
	default:
		return Multidim::Array<float, 2>();
	}

}

pybind11::array_t<float> getCoverMask(pybind11::array disp_l,
									  pybind11::array disp_r) {
	DispFormat l_format = dispFormatFromName(disp_l.request().format);
	DispFormat r_format = dispFormatFromName(disp_r.request().format);

	if (l_format == T_DISP_INVALID || r_format == T_DISP_INVALID) {
		throw std::invalid_argument("getProportionCovered can use disparities of either float or int8 or int32 !");
	}

	std::vector<long> l_shape = disp_l.request().shape;
	std::vector<long> r_shape = disp_r.request().shape;

	if (l_shape != r_shape) {
		throw std::invalid_argument("getProportionCovered needs arrays of the same shape !");
	}

	if (l_shape.size() != 2) {
		throw std::invalid_argument("getProportionCovered needs arrays with ndims = 2 !");
	}

	Multidim::Array<float, 2> coverMask;

	switch (l_format) {
	case T_DISP_FLOAT:
		coverMask = getCoverMaskImplIntrm<float>(disp_l, disp_r, r_format);
		break;
	case T_DISP_INT8:
		coverMask = getCoverMaskImplIntrm<int8_t>(disp_l, disp_r, r_format);
		break;
	case T_DISP_INT32:
		coverMask = getCoverMaskImplIntrm<int32_t>(disp_l, disp_r, r_format);
		break;
	default:
		return pybind11::array_t<float>();
	}

	return Multidim2NumpyArray(coverMask);
}
