#ifndef TRANSFORMS_H
#define TRANSFORMS_H

#include <pybind11/pytypes.h>
#include <pybind11/numpy.h>

enum CompPaddingType{
        MIN = 0,
        MEAN = 1,
        MAX = 2
};

pybind11::array_t<uint64_t> censusTransform(pybind11::array img, uint8_t h_radius, uint8_t v_radius);

#endif // TRANSFORMS_H
