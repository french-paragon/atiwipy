#ifndef UNFOLD_H
#define UNFOLD_H

#include <pybind11/pybind11.h>
#include <pybind11/numpy.h>

pybind11::array unfoldImage(pybind11::array image,
							int h_radius,
							int v_radius);

#endif // UNFOLD_H
