#include "./unfold.h"

#include "utils/type_utils.h"
#include "utils/arrays_utils.h"

#include "submodules/LibStevi/correlation/unfold.h"

#include <MultidimArrays/MultidimArrays.h>

template<typename T>
pybind11::array unfoldImageImpl(pybind11::array img,
								int h_radius,
								int v_radius) {


	std::vector<long> shape = img.request().shape;

	std::vector<long> strides = img.request().strides;

	if (shape.size() == 3) {

		Multidim::Array<T,3> im(static_cast<T*>(img.mutable_data(0)),
							   {static_cast<int>(shape[0]),
								static_cast<int>(shape[1]),
								static_cast<int>(shape[2])},
							   {static_cast<int>(strides[0]/sizeof (T)),
								static_cast<int>(strides[1]/sizeof (T)),
								static_cast<int>(strides[2]/sizeof (T))});

		return Multidim2UntypedNumpyArray(StereoVision::Correlation::unfold<T,T>(h_radius, v_radius, im));

	}

	if (shape.size() == 2) {

		Multidim::Array<T,2> im(static_cast<T*>(img.mutable_data(0)),
							   {static_cast<int>(shape[0]),
								static_cast<int>(shape[1])},
							   {static_cast<int>(strides[0]/sizeof (T)),
								static_cast<int>(strides[1]/sizeof (T))});

		return Multidim2UntypedNumpyArray(StereoVision::Correlation::unfold<T,T>(h_radius, v_radius, im));

	}

	return pybind11::array();
}

pybind11::array unfoldImage(pybind11::array image,
							int h_radius,
							int v_radius) {

	std::vector<long> shape = image.request().shape;

	if (shape.size() != 2 and shape.size() != 3) {
		throw std::invalid_argument("unfoldImage needs arrays with ndims = 2 or 3 !");
	}

	ArraysFormat format = formatFromName(image.request().format);

	if (format == T_INVALID) {
		throw std::invalid_argument("computeDisparity uses arrays of either float or uint8 !");
	}

	if (format == T_FLOAT) {
		return unfoldImageImpl<float>(image, h_radius, v_radius);
	}

	if (format == T_UINT8) {
		return unfoldImageImpl<uint8_t>(image, h_radius, v_radius);
	}

	return pybind11::array();
}
