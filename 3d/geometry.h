#ifndef ATIWIPY_GEOMETRY_H
#define ATIWIPY_GEOMETRY_H

#include <pybind11/pytypes.h>
#include <pybind11/numpy.h>

pybind11::array_t<float> estimateShapePreservingTransformBetweenPoints(pybind11::array_t<float> ptsSource,
																	   pybind11::array_t<float> ptsTarget,
																	   int n_steps,
																	   float incrLimit,
																	   float damping,
																	   float dampingScale);

#endif // ATIWIPY_GEOMETRY_H
