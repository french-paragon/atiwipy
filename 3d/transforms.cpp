#include "transforms.h"
#include "utils/type_utils.h"

#include <limits>

#include <pybind11/pybind11.h>
#include <pybind11/numpy.h>

namespace py = pybind11;

template<class T_I, int N_DIMS = 2>
py::array_t<uint64_t> census_transform_impl(py::array img,
										   uint8_t h_radius,
										   uint8_t v_radius,
										   CompPaddingType padding = MEAN) {

	T_I padding_v;

	switch (padding) {
	case MIN:
		padding_v = std::numeric_limits<T_I>::min();
		break;
	case MEAN:
		padding_v = std::numeric_limits<T_I>::max()/2 + std::numeric_limits<T_I>::min()/2;
		break;
	case MAX:
		padding_v = std::numeric_limits<T_I>::max();
		break;
	}

	std::vector<long> im_shape = img.request().shape;

	auto im_data = img.unchecked<T_I>();

	std::vector<long> out_shape = {im_shape[0], im_shape[1]};
	std::vector<long> out_strides = {im_shape[1]*static_cast<long>(sizeof(uint64_t)), 1*static_cast<long>(sizeof(uint64_t))};

	py::array_t<uint64_t> output = py::array_t<uint64_t>(out_shape, out_strides);

	auto out_data = output.mutable_unchecked();

	#pragma omp parallel for
	for (int i = 0; i < im_shape[0]; i++) {

		for (int j = 0; j < im_shape[1]; j++) {

			uint8_t id = 0;
			uint64_t bitMask = 0;

			for (int k = -v_radius; k <= v_radius; k++) {

				for (int l = -h_radius; l <= h_radius; l++) {

					if (i+k < 0 or i+k >= im_shape[0]) {
						if (N_DIMS == 3) {
							for (long c = 0; c < im_shape[2]; c++) {
								bitMask |= (padding_v < im_data(i,j,c)) ? (1<<id) : 0;
								id++;
							}
						} else {
							bitMask |= (padding_v < im_data(i,j)) ? (1<<id) : 0;
							id++;
						}
						continue;
					}

					if (j+l < 0 or j+l >= im_shape[1]) {
						if (N_DIMS == 3) {
							for (long c = 0; c < im_shape[2]; c++) {
								bitMask |= (padding_v < im_data(i,j,c)) ? (1<<id) : 0;
								id++;
							}
						} else {
							bitMask |= (padding_v < im_data(i,j)) ? (1<<id) : 0;
							id++;
						}
						continue;
					}

					if (N_DIMS == 3) {
						for (long c = 0; c < im_shape[2]; c++) {
							bitMask |= (im_data(i+k,j+l,c) < im_data(i,j,c)) ? (1<<id) : 0;
							id++;
						}
					} else {
						bitMask |= (im_data(i+k,j+l) < im_data(i,j)) ? (1<<id) : 0;
						id++;
					}

				}

			}

			out_data(i,j) = bitMask;

		}

	}

	return output;
}


py::array_t<uint64_t> censusTransform(pybind11::array img, uint8_t h_radius, uint8_t v_radius) {

	ArraysFormat im_format = formatFromName(img.request().format);

	if (im_format == T_INVALID) {
		throw std::invalid_argument("censusTransform uses arrays of either float or uint8 !");
	}

	if (img.ndim() == 2 and img.ndim() == 3) {
		throw std::invalid_argument("censusTransform needs array with ndims = 2 or 3 !");
	}

	size_t winSize = (2*h_radius+1)*(2*v_radius+1);
	if (img.ndim() == 3) {
		winSize *= img.shape()[2];
	}

	if (winSize > 64) {
		throw std::invalid_argument("censusTransform requires the radius to be small enough such that the moving windows has size 64 or less !");
	}

	if (im_format == T_FLOAT) {
		if (img.ndim() == 3) {
			return census_transform_impl<float, 3>(img, h_radius, v_radius);
		} else {
			return census_transform_impl<float, 2>(img, h_radius, v_radius);
		}
	}

	if (im_format == T_UINT8) {
		if (img.ndim() == 3) {
			return census_transform_impl<uint8_t, 3>(img, h_radius, v_radius);
		} else {
			return census_transform_impl<uint8_t, 2>(img, h_radius, v_radius);
		}
	}

	return py::array_t<uint64_t>();

}
