#include "semiglobalmatching.h"
#include "parallax.h"

#include "LibStevi/correlation/sgm.h"

#include <pybind11/pybind11.h>
#include <pybind11/numpy.h>

#include <MultidimArrays/MultidimArrays.h>

Multidim::Array<float, 3> semisGlobalMatchingCostVolume(pybind11::array cv_base,
														std::string const& cv_type,
														int nDirections,
														pybind11::array_t<int> margins,
														float P1,
														float P2) {
	std::string lowercaseType = cv_type;
	std::transform(lowercaseType.begin(), lowercaseType.end(), lowercaseType.begin(),
		[](unsigned char c){ return std::tolower(c); });

	if (lowercaseType != "score" and
		lowercaseType != "cost") {

		std::stringstream ss;
		ss << "Unsupported cost volume type " << cv_type << " !";
		throw std::invalid_argument(ss.str());

	}

	if (nDirections != 4 and nDirections != 8 and nDirections != 16) {
		std::stringstream ss;
		ss << "Invalid number of sgm directions " << nDirections << ", valid numbers of directions are 4, 8 or 16 !";
		throw std::invalid_argument(ss.str());
	}

	auto shape = cv_base.request().shape;

	if (shape.size() != 3) {
		std::stringstream ss;
		ss << "Expected a cost volume of dimension 3, got " << shape.size() << " !";
		throw std::invalid_argument(ss.str());
	}

	auto strides = cv_base.request().strides;

	Multidim::Array<float,3> cv(static_cast<float*>(cv_base.mutable_data(0)),
								{static_cast<int>(shape[0]), static_cast<int>(shape[1]), static_cast<int>(shape[2])},
								{static_cast<int>(strides[0]/sizeof (float)),
								 static_cast<int>(strides[1]/sizeof (float)),
								 static_cast<int>(strides[2]/sizeof (float))});

	StereoVision::Margins mrgs;

	auto mShape = margins.request().shape;
	if (mShape.size() != 1) {
		std::stringstream ss;
		ss << "Expected margins array of dimensions 1 , got " << mShape.size() << " !";
		throw std::invalid_argument(ss.str());
	}

	if (mShape[0] != 0 and mShape[0] != 1 and mShape[0] != 2 and mShape[0] != 4) {
		std::stringstream ss;
		ss << "Expected 0, 1, 2 or 4 margins elements , got " << mShape[0] << " !";
		throw std::invalid_argument(ss.str());
	}

	if (mShape[0] == 1) {
		mrgs = StereoVision::Margins(margins.at(0));
	}

	if (mShape[0] == 2) {
		mrgs = StereoVision::Margins(margins.at(0), margins.at(1));
	}

	if (mShape[0] == 4) {
		mrgs = StereoVision::Margins(margins.at(0), margins.at(1), margins.at(2), margins.at(3));
	}

	if (lowercaseType == "score") {
		switch (nDirections) {
		case 4:
			return StereoVision::Correlation::sgmCostVolume<4, StereoVision::Correlation::dispExtractionStartegy::Score>(cv, P1, P2, mrgs, 100*P2);
		case 8:
			return StereoVision::Correlation::sgmCostVolume<8, StereoVision::Correlation::dispExtractionStartegy::Score>(cv, P1, P2, mrgs, 100*P2);
		case 16:
			return StereoVision::Correlation::sgmCostVolume<16, StereoVision::Correlation::dispExtractionStartegy::Score>(cv, P1, P2, mrgs, 100*P2);
		default:
			break;
		}
	} else if (lowercaseType == "cost") {
		switch (nDirections) {
		case 4:
			return StereoVision::Correlation::sgmCostVolume<4, StereoVision::Correlation::dispExtractionStartegy::Cost>(cv, P1, P2, mrgs, 100*P2);
		case 8:
			return StereoVision::Correlation::sgmCostVolume<8, StereoVision::Correlation::dispExtractionStartegy::Cost>(cv, P1, P2, mrgs, 100*P2);
		case 16:
			return StereoVision::Correlation::sgmCostVolume<16, StereoVision::Correlation::dispExtractionStartegy::Cost>(cv, P1, P2, mrgs, 100*P2);
		default:
			break;
		}
	}

	return Multidim::Array<float, 3>();
}
