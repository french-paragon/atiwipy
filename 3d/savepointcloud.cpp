#include "savepointcloud.h"

#include <pcl/io/pcd_io.h>
#include <pcl/io/ply_io.h>
#include <pcl/point_types.h>
#include <pcl/conversions.h>

#include <type_traits>

#include "utils/type_utils.h"

namespace py = pybind11;

template<class T_C, int N_DIMS = 2>
int saveXYZRGBpointcloudTyped(std::string p_file, py::array_t<float> geom, py::array color, bool correctGamma) {

	pcl::PointCloud<pcl::PointXYZRGB> cloud;

	size_t h = geom.shape(0);
	size_t w = geom.shape(1);

	size_t stride_geom_h = geom.strides(0)/sizeof(T_C);
	size_t stride_geom_w = geom.strides(1)/sizeof(T_C);
	size_t stride_geom_d = geom.strides(2)/sizeof(T_C);

	size_t stride_color_h = color.strides(0)/sizeof(T_C);
	size_t stride_color_w = color.strides(1)/sizeof(T_C);
	size_t stride_color_d = color.strides(2)/sizeof(T_C);

	const float* g_data = geom.data();
	const T_C* c_data = static_cast<const T_C*>(color.data());

	// Fill in the cloud data
	cloud.width = static_cast<uint32_t>(w);
	cloud.height = static_cast<uint32_t>(h);
	cloud.is_dense = false;
	cloud.points.resize (cloud.width * cloud.height);

	for (std::size_t i = 0; i < h; ++i) {
		for (std::size_t j = 0; j < w; ++j) {

			cloud.points[i*w + j].x = static_cast<float>(g_data[i*stride_geom_h + j*stride_geom_w + 0*stride_geom_d]);
			cloud.points[i*w + j].y = static_cast<float>(g_data[i*stride_geom_h + j*stride_geom_w + 1*stride_geom_d]);
			cloud.points[i*w + j].z = static_cast<float>(g_data[i*stride_geom_h + j*stride_geom_w + 2*stride_geom_d]);

			if (std::is_same<T_C, float>::value) {

				float r = c_data[i*stride_color_h + j*stride_color_w + 0*stride_color_d];
				float g = c_data[i*stride_color_h + j*stride_color_w + 1*stride_color_d];
				float b = c_data[i*stride_color_h + j*stride_color_w + 2*stride_color_d];

				if (correctGamma) {
					r = std::pow<float>(r, float(1/2.2));
					g = std::pow<float>(g, float(1/2.2));
					b = std::pow<float>(b, float(1/2.2));
				}

				r = (r < 0) ? 0 : r;
				g = (g < 0) ? 0 : g;
				b = (b < 0) ? 0 : b;

				cloud.points[i*w + j].r = static_cast<uint8_t>(std::roundf(r*255));
				cloud.points[i*w + j].g = static_cast<uint8_t>(std::roundf(g*255));
				cloud.points[i*w + j].b = static_cast<uint8_t>(std::roundf(b*255));

			} else if (std::is_same<T_C, uint8_t>::value) {

				cloud.points[i*w + j].r = c_data[i*stride_color_h + j*stride_color_w + 0*stride_color_d];
				cloud.points[i*w + j].g = c_data[i*stride_color_h + j*stride_color_w + 1*stride_color_d];
				cloud.points[i*w + j].b = c_data[i*stride_color_h + j*stride_color_w + 2*stride_color_d];

			}
		}
	}

	std::string file = p_file;
	std::string ext_pcd = ".pcd";
	std::string ext_ply = ".ply";

	std::string file_ext = "";

	if (p_file.size() > 4) {
		if (std::equal(ext_pcd.rbegin(), ext_pcd.rend(), file.rbegin())) {
			file_ext = "pcd";
		} else if (std::equal(ext_ply.rbegin(), ext_ply.rend(), file.rbegin())) {
			file_ext = "ply";
		}
	}

	if (file_ext == "") {
		file += ".pcd";
		file_ext = "pcd";
	}

	int code = -1;

	if (file_ext == "pcd") {

		code = pcl::io::savePCDFileBinary(file, cloud);

	} else if (file_ext == "ply") {

		pcl::PLYWriter writer;
		pcl::PCLPointCloud2 cloud2;
		pcl::toPCLPointCloud2(cloud, cloud2);
		code = writer.write(file, cloud2, Eigen::Vector4f::Zero (), Eigen::Quaternionf::Identity (), true, false);

	}

	return code;

}

int saveXYZRGBpointcloud(std::string file, py::array_t<float> geom, py::array color, bool correctGamma) {

	ArraysFormat c_format = formatFromName(color.request().format);

	if (c_format == T_INVALID) {
		throw std::invalid_argument("saveXYZRGBpointcloud take color as array of either float or uint8 !");
	}

	if (c_format == T_FLOAT) {
		return saveXYZRGBpointcloudTyped<float>(file, geom, color, correctGamma);
	} else if (c_format == T_UINT8) {
		return saveXYZRGBpointcloudTyped<uint8_t>(file, geom, color, correctGamma);
	}

	return -1;
}
