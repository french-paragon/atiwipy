#ifndef OCIOCOLORTRANSFORMER_H
#define OCIOCOLORTRANSFORMER_H

#include <pybind11/pybind11.h>
#include <pybind11/numpy.h>

struct OcioColorTransformerData;

class OcioColorTransformer
{
public:
        OcioColorTransformer(std::string const& configFile, std::string const& inColorSpace, std::string const& outView, std::string const& displayView);

        ~OcioColorTransformer();

        void applyTransform(pybind11::array_t<float> input);

private:
        OcioColorTransformerData* _dats;
};

#endif // OCIOCOLORTRANSFORMER_H
