#ifndef FOREGROUNDBACKGROUNDSEGMENTATION_H
#define FOREGROUNDBACKGROUNDSEGMENTATION_H

#include <pybind11/pytypes.h>
#include <pybind11/numpy.h>

#include <MultidimArrays/MultidimArrays.h>

pybind11::array_t<bool> partialGlobalSegmentation(pybind11::array cost,
												  pybind11::array_t<bool> current_mask,
												  pybind11::array guide,
												  int optimizablePixelsRadius,
												  float switch_max_cost,
												  float switch_min_cost);

#endif // FOREGROUNDBACKGROUNDSEGMENTATION_H
