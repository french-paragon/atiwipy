#include "foregroundbackgroundsegmentation.h"

#include "LibStevi/imageProcessing/foregroundSegmentation.h"
#include "LibStevi/imageProcessing/morphologicalOperators.h"

#include "utils/type_utils.h"
#include "utils/arrays_utils.h"

template<typename T_Cost, typename T_Guide>
pybind11::array_t<bool> partialGlobalSegmentationImpl(pybind11::array cost,
													  pybind11::array_t<bool> current_mask,
													  int optimizablePixelsRadius,
													  float switch_max_cost,
													  float switch_min_cost,
													  pybind11::array guide) {

	constexpr int nGuideDim = 3;

	using MaskInfo = StereoVision::ImageProcessing::FgBgSegmentation::MaskInfo;

	std::vector<long> cost_shape = cost.request().shape;
	std::vector<long> cost_strides = cost.request().strides;

	std::vector<long> mask_shape = current_mask.request().shape;
	std::vector<long> mask_strides = current_mask.request().strides;

	std::vector<long> guide_shape = cost.request().shape;
	std::vector<long> guide_strides = cost.request().strides;

	Multidim::Array<T_Cost,3> costArray(static_cast<T_Cost*>(cost.mutable_data(0)),
										{static_cast<int>(cost_shape[0]),
										 static_cast<int>(cost_shape[1]),
										 static_cast<int>(cost_shape[2])},
										{static_cast<int>(cost_strides[0]/sizeof (T_Cost)),
										 static_cast<int>(cost_strides[1]/sizeof (T_Cost)),
										 static_cast<int>(cost_strides[2]/sizeof (T_Cost))});

	Multidim::Array<bool,2> currentMaskArray(current_mask.mutable_data(0),
										 {static_cast<int>(mask_shape[0]),
										  static_cast<int>(mask_shape[1])},
										 {static_cast<int>(mask_strides[0]/sizeof (bool)),
										  static_cast<int>(mask_strides[1]/sizeof (bool))});

	std::array<int,nGuideDim> gShape;
	std::array<int,nGuideDim> gStrides;

	for (int i = 0; i < nGuideDim; i++) {
		gShape[i] = guide_shape[i];
		gStrides[i] = guide_strides[i]/sizeof (T_Guide);
	}

	Multidim::Array<T_Guide,nGuideDim> guideArray(static_cast<T_Guide*>(guide.mutable_data(0)),
										 gShape,
										 gStrides);



	Multidim::Array<bool, 2> extended_mask = StereoVision::ImageProcessing::dilation<bool, bool>(optimizablePixelsRadius, optimizablePixelsRadius, currentMaskArray);
	Multidim::Array<bool, 2> eroded_mask = StereoVision::ImageProcessing::erosion<bool, bool>(optimizablePixelsRadius, optimizablePixelsRadius, currentMaskArray);

	Multidim::Array<bool, 2> optimizable_pixels(currentMaskArray.shape());

	for (int i = 0; i < currentMaskArray.shape()[0]; i++) {
		for (int j = 0; j < currentMaskArray.shape()[1]; j++) {

			optimizable_pixels.atUnchecked(i,j) = extended_mask.valueUnchecked(i,j) == true and eroded_mask.valueUnchecked(i,j) == false;

		}
	}

	StereoVision::ImageProcessing::GuidedMaskCostPolicy maskCostPolicy(static_cast<T_Cost>(switch_max_cost),
																	   guideArray,
																	   static_cast<T_Cost>(switch_min_cost));

	Multidim::Array<MaskInfo, 2> mask =
			StereoVision::ImageProcessing::getPartialGlobalRefinedMask(costArray, maskCostPolicy, optimizable_pixels, currentMaskArray.cast<MaskInfo>());

	return Multidim2NumpyArray<bool>(mask.cast<bool>());
}

pybind11::array_t<bool> partialGlobalSegmentation(pybind11::array cost,
												  pybind11::array_t<bool> current_mask,
												  pybind11::array guide,
												  int optimizablePixelsRadius,
												  float switch_max_cost,
												  float switch_min_cost) {



	std::vector<long> cost_shape = cost.request().shape;
	std::vector<long> mask_shape = current_mask.request().shape;
	std::vector<long> guide_shape = guide.request().shape;

	if (cost_shape.size() != 3) {
		throw std::invalid_argument("invalid cost shape !");
	}

	if (mask_shape.size() != 2) {
		throw std::invalid_argument("invalid current mask shape !");
	}

	if (cost_shape[2] != 2) {
		throw std::invalid_argument("invalid cost shape !");
	}


	if (guide_shape.size() != 3) {
		throw std::invalid_argument("invalid guide shape !");
	}

	for (int idx = 0; idx < 2; idx++) {
		if (cost_shape[idx] != mask_shape[idx] or
				cost_shape[idx] != guide_shape[idx]) {
			throw std::invalid_argument("Incompatible inputs shapes !");
		}
	}

	ArraysFormat cost_format = formatFromName(cost.request().format);
	ArraysFormat guide_format = formatFromName(guide.request().format);

	if (cost_format == T_INVALID || guide_format == T_INVALID) {
		throw std::invalid_argument("computeDisparity uses arrays of either float or uint8 !");
	}

	switch (cost_format) {
	case T_FLOAT:
		switch (cost_format) {
		case T_FLOAT:
			return partialGlobalSegmentationImpl<float, float>(cost, current_mask, optimizablePixelsRadius, switch_max_cost, switch_min_cost, guide);
		case T_UINT8:
			return partialGlobalSegmentationImpl<uint8_t, float>(cost, current_mask, optimizablePixelsRadius, switch_max_cost, switch_min_cost, guide);
		default:
			break;
		}
		break;
	case T_UINT8:
		switch (cost_format) {
		case T_FLOAT:
			return partialGlobalSegmentationImpl<float, uint8_t>(cost, current_mask, optimizablePixelsRadius, switch_max_cost, switch_min_cost, guide);
		case T_UINT8:
			return partialGlobalSegmentationImpl<uint8_t, uint8_t>(cost, current_mask, optimizablePixelsRadius, switch_max_cost, switch_min_cost, guide);
		default:
			break;
		}
		break;
	default:
		break;
	}

	return pybind11::array_t<bool>();
}
